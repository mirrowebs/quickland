/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/backend/modules/properties/js/properties.js":
/*!**********************************************************************!*\
  !*** ./resources/assets/backend/modules/properties/js/properties.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('.delete_property_image').on('click', function () {
    var image = $(this).attr('id');

    if (image != '') {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: '/admin/ajax/deletepropertyimage',
        type: 'POST',
        data: {
          'image': image
        },
        success: function success(response) {
          $('.imagediv' + image).remove();
        }
      });
    }
  });
  $('#property_type').on('change', function () {
    var property = $('#property_type').val();

    if (property == 'Residential') {
      $('.propertysubtype').show();
    } else {
      $('.propertysubtype').hide();
    }

    $('#property_sub_type').children('option').remove();

    if (property != '') {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: '/admin/ajax/getPropertySubTypes',
        type: 'POST',
        data: {
          'property': property
        },
        success: function success(response) {
          var resultdata = $.parseJSON(response);
          var length = Object.keys(resultdata.propertytypes).length;

          if (length > 0) {
            $('#property_sub_type').append(new Option('Select', '', false, false));
            $.each(resultdata.propertytypes, function (k, v) {
              if ($('.property_sub_type').val() == k) {
                $('#property_sub_type').append(new Option(v, k, true, true));
              } else {
                $('#property_sub_type').append(new Option(v, k, false, false));
              }
            });
          } else {
            $('#property_sub_type').append(new Option('There is no sub types', '', false, false));
          }
        }
      });
    } else {
      $('#property_sub_type').append(new Option('Select Type first', '', false, false));
    }
  }).change();
  $('#properties').validate({
    ignore: [],
    errorClass: 'text-danger',
    // You can change the animation class for a different entrance animation - check animations page
    errorElement: 'div',
    errorPlacement: function errorPlacement(error, e) {
      e.parents('.form-group').append(error);
    },
    highlight: function highlight(e) {
      $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
      $(e).closest('.text-danger').remove();
    },
    success: function success(e) {
      // You can use the following if you would like to highlight with green color the input after successful validation!
      e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');

      e.closest('.text-danger').remove();
    },
    rules: {
      'property[property_sell]': {
        required: true
      },
      'property[property_transaction_type]': {
        required: true
      },
      'property[property_location]': {
        required: true
      },
      'property[property_area_unit]': {
        required: true
      },
      'property[property_built_up_area]': {
        required: true
      },
      'property[property_type]': {
        required: true
      }
    },
    messages: {
      'property[property_sell]': {
        required: 'Please select sell type'
      },
      'property[property_transaction_type]': {
        required: 'Please select transaction type'
      },
      'property[property_location]': {
        required: 'Please enter location'
      },
      'property[property_area_unit]': {
        required: 'Please select area unit'
      },
      'property[property_built_up_area]': {
        required: 'Please enter built up area'
      },
      'property[property_type]': {
        required: 'Please select type'
      }
    }
  });
});

/***/ }),

/***/ 4:
/*!****************************************************************************!*\
  !*** multi ./resources/assets/backend/modules/properties/js/properties.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /working/gonda/web/quickland/resources/assets/backend/modules/properties/js/properties.js */"./resources/assets/backend/modules/properties/js/properties.js");


/***/ })

/******/ });