/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 56);
/******/ })
/************************************************************************/
/******/ ({

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(57);


/***/ }),

/***/ 57:
/***/ (function(module, exports) {

jQuery(document).ready(function () {
    $('.delete_productExtra_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deleteproductextraimage',
                type: 'POST',
                data: { 'image': image },
                success: function success(response) {
                    $('.imagediv' + image).remove();
                    $('#productExtraimage' + image).addClass('productExtraimage');
                    imageRules();
                }
            });
        }
    });
    $('.addProductExtraInfo').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function errorPlacement(error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function highlight(e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function success(e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                pe_title: {
                    required: true
                },
                pe_position: {
                    required: true,
                    number: true
                },
                pe_description: {
                    required: true
                },
                pe_status: {
                    required: true
                }
            },
            messages: {
                pe_title: {
                    required: 'Please enter title'
                },
                pe_position: {
                    required: 'Please enter description',
                    number: 'position must be a number'
                },
                pe_description: {
                    required: 'Please enter description'
                },
                pe_status: {
                    required: 'Please select status'
                }
            }
        });
    });

    imageRules();

    $('.delete_product_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deleteproductimage',
                type: 'POST',
                data: { 'image': image },
                success: function success(response) {
                    $('.imagediv' + image).remove();
                }
            });
        }
    });
    jQuery('#productImages').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function errorPlacement(error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function highlight(e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function success(e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            'productimages[]': {
                required: true
            }
        },
        messages: {
            'productimages[]': {
                required: 'Select any one image'
            }
        }
    });

    jQuery('#products').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function errorPlacement(error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function highlight(e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function success(e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            product_name: {
                required: true
            },
            product_type: {
                required: true
            },
            product_availability: {
                required: true
            },
            product_color: {
                required: true
            },
            product_description: {
                required: true
            },
            product_real_price: {
                required: true,
                number: true
            },
            product_price: {
                required: true,
                number: true
            },
            product_brand: {
                required: true
            },
            product_country: {
                required: true
            },
            product_category: {
                required: true
            },
            product_shipping_price: {
                required: true,
                number: true
            },
            product_status: {
                required: true
            }
        },
        messages: {
            product_name: {
                required: 'Please enter name'
            },
            product_type: {
                required: 'Please enter type'
            },
            product_availability: {
                required: 'Please enter availability'
            },
            product_color: {
                required: 'Please enter color'
            },
            product_description: {
                required: 'Please enter description'
            },
            product_real_price: {
                required: 'Please enter price'
            },
            product_price: {
                required: 'Please enter price'
            },
            product_brand: {
                required: 'Select Brand name'
            },
            product_country: {
                required: 'Please select country'
            },
            product_category: {
                required: 'Please select category'
            },
            product_shipping_price: {
                required: 'Please enter shipping price'
            },
            product_status: {
                required: 'Please select status'
            }
        }
    });
});

function imageRules() {
    $(".productExtraimage").each(function () {
        $(this).rules("add", {
            required: true,
            messages: {
                required: "Upload image to save"
            }
        });
    });
}

/***/ })

/******/ });