<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_options', function (Blueprint $table) {
            $table->increments('po_id');
            $table->integer('po_property_id');
            $table->string('po_construction_status')->nullable();
            $table->string('po_construction_month')->nullable();
            $table->integer('po_construction_year')->nullable();
            $table->string('po_bedrooms')->nullable();
            $table->string('po_bathrooms')->nullable();
            $table->string('po_balconies')->nullable();
            $table->string('po_facing')->nullable();
            $table->string('po_furnishing')->nullable();
            $table->text('po_other_rooms')->nullable();
            $table->string('po_age_of_property')->nullable();
            $table->string('po_total_floors')->nullable();
            $table->integer('po_floor_no')->nullable();
            $table->string('po_parking')->nullable();
            $table->integer('po_plot_area')->nullable();
            $table->string('po_plot_area_type')->nullable();
            $table->integer('po_built_up_area')->nullable();
            $table->string('po_built_up_area_type')->nullable();
            $table->integer('po_carpet_area')->nullable();
            $table->string('po_carpet_area_type')->nullable();
            $table->string('po_longitude')->nullable();
            $table->string('po_latitude')->nullable();
            $table->string('po_location_name')->nullable();
            $table->string('po_landmark')->nullable();
            $table->string('po_city')->nullable();
            $table->string('po_locality')->nullable();
            $table->bigInteger('po_expected_price')->nullable();
            $table->bigInteger('po_all_inclusive_price')->nullable();
            $table->bigInteger('po_price_per')->nullable();
            $table->bigInteger('po_maintenance_amount')->nullable();
            $table->string('po_maintenance_amount_type')->nullable();
            $table->bigInteger('po_booking_amount')->nullable();
            $table->text('po_description')->nullable();
            $table->text('po_landscape')->nullable();
            $table->text('po_indoor_games')->nullable();
            $table->text('po_fitness')->nullable();
            $table->text('po_outdoor_games')->nullable();
            $table->text('po_network_and_connectivity')->nullable();
            $table->text('po_green_living')->nullable();
            $table->text('po_recreation')->nullable();
            $table->text('po_convenience')->nullable();
            $table->text('po_health_facilities')->nullable();
            $table->text('po_safety_and_security')->nullable();
            $table->text('po_infrastructure')->nullable();
            $table->text('po_entertainment')->nullable();
            $table->text('po_retail')->nullable();
            $table->text('po_transport')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_options');
    }
}
