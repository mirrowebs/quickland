<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiries', function (Blueprint $table) {
            $table->increments('enquiry_id');
            $table->string('enquiry_name');
            $table->string('enquiry_email');
            $table->string('enquiry_phone')->nullable();
            $table->string('enquiry_subject')->nullable();
            $table->text('enquiry_message')->nullable();
            $table->integer('enquiry_user_id')->default(0)->comment('0=site enquiry 1-Agent enquiry');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiries');
    }
}
