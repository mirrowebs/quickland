<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('property_id');
            $table->string('property_userid')->comment('from users table');
            $table->string('property_for')->comment('Sell or Rent');
            $table->string('property_by')->comment('Owner or Dealer');
            $table->string('property_type')->comment('residential or commercial');
            $table->string('property_sub_type')->comment('appartment or commercial office space like that')->nullable();
            $table->string('property_status')->default('inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
