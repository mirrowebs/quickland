<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::match(array('GET', 'POST'), '/', 'Frontend\HomeController@index')->name('home');

Route::match(array('GET', 'POST'), '/properties/{id?}', 'Frontend\HomeController@propertyList')->name('propertyList');

Route::match(array('GET', 'POST'), '/agents/{id?}', 'Frontend\HomeController@agentsList')->name('agents');


Route::get('/login', 'Auth\LoginController@showLoginForm')->name('userlogin');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('userlogout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::get('/ajax/getPropertyType', 'Frontend\AjaxController@getPropertyType')->name('ajaxGetPropertyType');
Route::get('/ajax/getPropertyType/options', 'Frontend\AjaxController@getPropertyTypeOptions')->name('ajaxGetPropertyTypeOptions');
Route::get('/ajax/getPropertyType/options/selected', 'Frontend\AjaxController@getPropertyTypeOptionSelected')->name('ajaxGetPropertyTypeOptionSelected');

Route::match(array('GET', 'POST'), 'enquiries', 'Frontend\HomeController@enquiries')->name('enquiries');

Route::prefix('user')->group(function () {

    Route::match(array('GET', 'POST'), '/listing/add/{step?}', 'Frontend\UserController@ListingAdd')->name('ListingAdd');

// User Profile
    Route::match(array('GET', 'POST'), '/profile', 'Frontend\UserController@index')->name('userprofile');
    Route::match(array('GET', 'POST'), '/properties', 'Frontend\UserController@properties')->name('userproperties');
    Route::match(array('GET', 'POST'), '/property/details/{type}/{propertyid}', 'Frontend\UserController@updateProperties')->name('userpropertyDetails');
    Route::post('ajax/deletepropertyimage', 'Frontend\UserController@deletepropertyimage')->name('deletepropertyimage');
});

Route::prefix('admin')->group(function () {

    /* login module routes */
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    /* Dashboard routes */
    Route::get('/dashboard', 'Backend\AdminController@index')->name('dashboard');

    /* Change Password */
    Route::match(array('GET', 'POST'), '/change/password', 'Backend\AdminController@changePassword')->name('changePassword');

    /* update Profile */
    Route::match(array('GET', 'POST'), '/update/profile', 'Backend\AdminController@profile')->name('profile');

    /* Countries module routes */
    Route::match(array('GET', 'POST'), '/countries', 'Backend\CountriesController@index')->name('countries');
    Route::match(array('GET', 'POST'), '/countries/manage/{id?}', 'Backend\CountriesController@addNewCountries')->name('addNewCountries');

    /* Agents module routes */
    Route::match(array('GET', 'POST'), '/agents', 'Backend\AdminController@agentsList')->name('agentsList');

    /* Agents Enquiries module routes */
    Route::match(array('GET', 'POST'), '/agents/enquiries/{id}', 'Backend\AdminController@agentEnquiriesList')->name('agentEnquiriesList');

    /* Properties module routes */
    Route::match(array('GET', 'POST'), '/properties', 'Backend\PropertiesController@index')->name('properties');
    Route::match(array('GET', 'POST'), '/properties/manage/{id?}', 'Backend\PropertiesController@addNewProperties')->name('addNewProperties');
    Route::post('ajax/getPropertySubTypes', 'Backend\PropertiesController@getPropertySubTypes')->name('getPropertySubTypes');
    Route::match(array('GET', 'POST'), '/property/images/{id?}', 'Backend\PropertiesController@addPropertyImages')->name('addPropertyImages');
    Route::post('ajax/deletepropertyimage', 'Backend\PropertiesController@deletepropertyimage')->name('deletepropertyimage');

});

