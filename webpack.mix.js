const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // .js('resources/js/app.js', 'public/js')
    .js('resources/assets/frontend/js/frontend_all_in_one_js.js', 'public/frontend/js')
    .sass('resources/assets/frontend/sass/frontend_all_in_one_style.scss', 'public/frontend/css');
    // .sass('resources/sass/app.scss', 'public/css');

// images
mix.copyDirectory('resources/assets/backend/images', 'public/backend/images')
    .copyDirectory('resources/assets/frontend/images/', 'public/frontend/images');


// styles
mix.sass('resources/assets/backend/sass/theme.scss', 'public/backend/css')
    .js('resources/assets/backend/js/main.js', 'public/backend/js');



// Modules
mix.js('resources/assets/backend/js/dashboard.js', 'public/backend/js')
    .js('resources/assets/backend/modules/countries/js/countries.js', 'public/backend/modules/countries/js/')
    .js('resources/assets/backend/modules/properties/js/properties.js', 'public/backend/modules/properties/js/');
