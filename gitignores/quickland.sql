-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2019 at 03:37 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quickland`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_13_074227_create_countries_table', 1),
(14, '2018_12_13_092658_create_properties_table', 2),
(15, '2018_12_13_101313_create_property_options_table', 2),
(16, '2018_12_13_103603_create_property_images_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `property_id` int(10) UNSIGNED NOT NULL,
  `property_userid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'from users table',
  `property_for` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Sell or Rent',
  `property_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Owner or Dealer',
  `property_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'residential or commercial',
  `property_sub_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'appartment or commercial office space like that',
  `property_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`property_id`, `property_userid`, `property_for`, `property_by`, `property_type`, `property_sub_type`, `property_status`, `created_at`, `updated_at`) VALUES
(1, '1', 'sell', 'owner', 'residential', 'independentHouseOrVilla', 'inactive', '2019-03-25 08:25:20', '2019-03-25 08:25:20'),
(2, '1', 'sell', 'owner', 'residential', 'independentHouseOrVilla', 'inactive', '2019-03-25 08:31:24', '2019-03-25 08:31:24'),
(3, '1', 'sell', 'owner', 'residential', 'independentHouseOrVilla', 'inactive', '2019-03-25 08:35:04', '2019-03-25 08:35:04');

-- --------------------------------------------------------

--
-- Table structure for table `property_images`
--

CREATE TABLE `property_images` (
  `pi_id` int(10) UNSIGNED NOT NULL,
  `pi_property_id` int(11) NOT NULL,
  `pi_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property_images`
--

INSERT INTO `property_images` (`pi_id`, `pi_property_id`, `pi_image`, `created_at`, `updated_at`) VALUES
(1, 3, '1553522811tilesindex03.jpg', '2019-03-25 08:36:52', '2019-03-25 08:36:52'),
(2, 3, '1553522812tilesindex04 - Copy.jpg', '2019-03-25 08:36:52', '2019-03-25 08:36:52'),
(3, 3, '1553522812tilesindex04.jpg', '2019-03-25 08:36:52', '2019-03-25 08:36:52'),
(4, 3, '1553522813tilesindex05.jpg', '2019-03-25 08:36:53', '2019-03-25 08:36:53'),
(5, 3, '1553522813tilesindex06.jpg', '2019-03-25 08:36:53', '2019-03-25 08:36:53');

-- --------------------------------------------------------

--
-- Table structure for table `property_options`
--

CREATE TABLE `property_options` (
  `po_id` int(10) UNSIGNED NOT NULL,
  `po_property_id` int(11) NOT NULL,
  `po_construction_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_construction_month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_construction_year` int(11) DEFAULT NULL,
  `po_bedrooms` int(11) DEFAULT NULL,
  `po_bathrooms` int(11) DEFAULT NULL,
  `po_balconies` int(11) DEFAULT NULL,
  `po_facing` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_furnishing` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_other_rooms` text COLLATE utf8mb4_unicode_ci,
  `po_age_of_property` int(11) DEFAULT NULL,
  `po_total_floors` int(11) DEFAULT NULL,
  `po_floor_no` int(11) DEFAULT NULL,
  `po_parking` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_plot_area` int(11) DEFAULT NULL,
  `po_plot_area_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_built_up_area` int(11) DEFAULT NULL,
  `po_built_up_area_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_carpet_area` int(11) DEFAULT NULL,
  `po_carpet_area_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_location_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_landmark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_locality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_expected_price` double(8,2) DEFAULT NULL,
  `po_all_inclusive_price` double(8,2) DEFAULT NULL,
  `po_price_per_sqft` double(8,2) DEFAULT NULL,
  `po_maintenance_amount` double(8,2) DEFAULT NULL,
  `po_maintenance_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_booking_amount` double(8,2) DEFAULT NULL,
  `po_description` text COLLATE utf8mb4_unicode_ci,
  `po_landscape` text COLLATE utf8mb4_unicode_ci,
  `po_indoor_games` text COLLATE utf8mb4_unicode_ci,
  `po_fitness` text COLLATE utf8mb4_unicode_ci,
  `po_outdoor_games` text COLLATE utf8mb4_unicode_ci,
  `po_network_and_connectivity` text COLLATE utf8mb4_unicode_ci,
  `po_green_living` text COLLATE utf8mb4_unicode_ci,
  `po_recreation` text COLLATE utf8mb4_unicode_ci,
  `po_convenience` text COLLATE utf8mb4_unicode_ci,
  `po_health_facilities` text COLLATE utf8mb4_unicode_ci,
  `po_safety_and_security` text COLLATE utf8mb4_unicode_ci,
  `po_infrastructure` text COLLATE utf8mb4_unicode_ci,
  `po_entertainment` text COLLATE utf8mb4_unicode_ci,
  `po_retail` text COLLATE utf8mb4_unicode_ci,
  `po_transport` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property_options`
--

INSERT INTO `property_options` (`po_id`, `po_property_id`, `po_construction_status`, `po_construction_month`, `po_construction_year`, `po_bedrooms`, `po_bathrooms`, `po_balconies`, `po_facing`, `po_furnishing`, `po_other_rooms`, `po_age_of_property`, `po_total_floors`, `po_floor_no`, `po_parking`, `po_plot_area`, `po_plot_area_type`, `po_built_up_area`, `po_built_up_area_type`, `po_carpet_area`, `po_carpet_area_type`, `po_longitude`, `po_latitude`, `po_location_name`, `po_landmark`, `po_city`, `po_locality`, `po_expected_price`, `po_all_inclusive_price`, `po_price_per_sqft`, `po_maintenance_amount`, `po_maintenance_type`, `po_booking_amount`, `po_description`, `po_landscape`, `po_indoor_games`, `po_fitness`, `po_outdoor_games`, `po_network_and_connectivity`, `po_green_living`, `po_recreation`, `po_convenience`, `po_health_facilities`, `po_safety_and_security`, `po_infrastructure`, `po_entertainment`, `po_retail`, `po_transport`, `created_at`, `updated_at`) VALUES
(1, 1, 'ready-to-move', 'January', 2019, 0, 0, 1, 'North', 'semi furnishing', 'Pooja Room', NULL, NULL, NULL, NULL, 222, 'sq.ft', 222, 'sq.ft', 222, 'sq.ft', '222', '222', '222', '222', '222', 'AP', 222.00, 222.00, 222.00, 2222.00, 'monthy', 22222.00, 'qweqwewqeqwe', 'Landscaped Garden', 'Tennis,Badminton', 'Swimming Pool,Gymnasium', 'Tennis Court', 'Intercom,', 'Rain Water Harvesting', 'Club House', 'Library,Maintenance Staff', 'Health Facilities,Jacuzzi Steam', 'Gated Community,Security', 'Pucca Road', 'Projector', 'Grocery Store', 'Shuttle Busses', '2019-03-25 08:27:03', '2019-03-25 08:27:03'),
(2, 3, 'ready-to-move', 'January', 2019, 0, 0, 1, 'North', 'semi furnishing', 'Pooja Room', NULL, NULL, NULL, NULL, NULL, 'sq.ft', NULL, 'sq.ft', NULL, 'sq.ft', NULL, NULL, NULL, NULL, NULL, 'Select', NULL, NULL, NULL, NULL, 'monthy', NULL, 'deretrtret', 'Landscaped Garden', 'Tennis,Badminton', 'Swimming Pool,Gymnasium', 'Play Area,Tennis Court', NULL, NULL, 'Club House', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-25 08:36:51', '2019-03-25 08:36:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` bigint(20) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL COMMENT '404=admin,1=user',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile_prefix`, `mobile`, `email`, `email_verified_at`, `password`, `image`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sample', NULL, 9809809809, 'susu@gmail.com', NULL, '$2y$10$08W9YZIoTse./hH7.zZn5uBx2f5nXLas5EnNl2bJVIVdd2d8Cc1g.', NULL, 1, 'active', 'MpejKZYpjIJYZdgP40FS15PqsC5rxvi7dtjQLjnbNKeRcPAuPCKA1oFig49Z', '2019-03-21 06:02:47', '2019-03-21 06:02:47'),
(12, 'assadasd', NULL, 1231231232, 'sadasdsad', NULL, '$2y$10$VORz0db0KP9O9qLWovpYLu5zWTRZc7uds3ddswePfJFN3mNuy5Zz6', NULL, 1, 'active', NULL, '2019-03-25 08:25:20', '2019-03-25 08:25:20'),
(13, 'fdfsdfsdf', NULL, 54545, 'fdsfsdfsdf', NULL, '$2y$10$J8Jc.krtHfwtAyyNQTkn7.82jd9ZmWrcn81sw27s43Bz1yp.cSC8W', NULL, 1, 'active', NULL, '2019-03-25 08:31:24', '2019-03-25 08:31:24'),
(14, 'rretert', NULL, 2342342342, 'ertertert', NULL, '$2y$10$/y3vb1TdM9bCkK96n/ZUmOj3muP5lBanT3epuJykgL2zBgE6IHkHK', NULL, 1, 'active', NULL, '2019-03-25 08:35:03', '2019-03-25 08:35:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`property_id`);

--
-- Indexes for table `property_images`
--
ALTER TABLE `property_images`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `property_options`
--
ALTER TABLE `property_options`
  ADD PRIMARY KEY (`po_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `property_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `property_images`
--
ALTER TABLE `property_images`
  MODIFY `pi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `property_options`
--
ALTER TABLE `property_options`
  MODIFY `po_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
