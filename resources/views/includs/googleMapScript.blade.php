<script type='text/javascript'>


    function showPosition(position) {

        var lati = position.coords.latitude;
        var longitude = position.coords.longitude;

        var lat = lati;
        var lng = longitude;
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    document.getElementById("autocomplete").value = results[1].formatted_address;
                    document.getElementById("location_name").value = results[1].formatted_address;


                }
            }
        });
    }


    window.onload = function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }


    //<![CDATA[

    //map.js

    //Set up some of our variables.
    var map; //Will contain map object.
    var marker = true; ////Has the user plotted their location marker?

    //Function called to initialize / create the map.
    //This is called when the page has loaded.
    function initMap() {

            <?php
            $po_longitude = '78.44967289456349';
            $po_latitude = '17.2751370312338';

            if (isset($properties->propertyOptions) && count($properties->propertyOptions) > 0) {
                $po_longitude = $properties->propertyOptions[0]->po_longitude;
//                    $po_longitude = $properties->propertyOptions[0]->po_longitude;
                $po_latitude = $properties->propertyOptions[0]->po_latitude;

            }
            ?>


        var input = document.getElementById('autocomplete');
        var options = {
            types: ['(regions)'],
            componentRestrictions: {country: "IN"}
        };
        // var options = {}
        var autocomplete = new google.maps.places.Autocomplete(input, options);


        var po_latitude = '{{$po_latitude}}';
        var po_longitude = '{{$po_longitude}}';

        console.log(po_latitude + '<>' + po_longitude);

        //The center location of our map.
        var centerOfMap = new google.maps.LatLng(po_latitude, po_longitude);
        var options = {
            center: centerOfMap, //Set center.
            zoom: 15 //The zoom value.
        };
        map = new google.maps.Map(document.getElementById('map'), options);
        var myLatlng = {lat: 17.2751370312338, lng: 78.44967289456349};

        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable: true //make it draggable
        });

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();

            console.log(lat)
            console.log(lng)
            markerLocation();

            document.getElementById("latitude").value = lat;
            document.getElementById("longitude").value = lng;


            marker.setPosition(place.geometry.location);
            map.setCenter(place.geometry.location);
            map.setZoom(17);


        });


        google.maps.event.addListener(marker, 'dragend', function (event) {
            markerLocation();
        });


        //Listen for any clicks on the map.
        google.maps.event.addListener(map, 'click', function (event) {
            //Get the location that the user clicked.
            var clickedLocation = event.latLng;

            console.log('event' + clickedLocation);
            //If the marker hasn't been added.
            if (marker === false) {
                //Create the marker.
                marker = new google.maps.Marker({
                    position: clickedLocation,
                    map: map,
                    draggable: true //make it draggable
                });
                //Listen for drag events!
                google.maps.event.addListener(marker, 'dragend', function (event) {
                    markerLocation();
                });
            } else {
                //Marker has already been added, so just change its location.
                marker.setPosition(clickedLocation);
            }
            //Get the marker's location.
            markerLocation();
        });
    }

    function geocodeLatLng(map, lat, lng) {

        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;


        var latlng = {lat: lat, lng: lng};

        // console.log(latlng);

        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {


                    document.getElementById('location_name').value = results[0].formatted_address; //

                    console.log(results[0]);

                    // map.setZoom(11);
                    // var marker = new google.maps.Marker({
                    //     position: latlng,
                    //     map: map
                    // });
                    // infowindow.setContent(results[0].formatted_address);
                    // infowindow.open(map, marker);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }


    function markerLocation() {
        var currentLocation = marker.getPosition();


        document.getElementById('latitude').value = currentLocation.lat(); //latitude
        document.getElementById('longitude').value = currentLocation.lng(); //longitude

        geocodeLatLng(map, currentLocation.lat(), currentLocation.lng());


        var latlng = new google.maps.LatLng(currentLocation.lat(), currentLocation.lng());
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    document.getElementById("autocomplete").value = results[1].formatted_address;
                    document.getElementById("location_name").value = results[1].formatted_address;
                }
            }
        });

    }

    google.maps.event.addDomListener(window, 'load', initMap);


</script>



