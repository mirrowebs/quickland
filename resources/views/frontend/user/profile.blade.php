@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')

    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container">
                        <h1>User Profile Edit
                        </h1>
                        <div class="page-title-actions">
                            <div class="switcher">
                                <strong>Currency</strong>
                                <ul>
                                    <li class="active"><a href="#">USD</a></li>
                                    <li><a href="#">EUR</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                            <div class="switcher">
                                <strong>Language</strong>
                                <ul>
                                    <li class="active"><a href="#">EN</a></li>
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">DE</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                        </div>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-->
                </div>
                <!-- /.page-title -->
                <div class="container">
                    <nav class="breadcrumb">
                        <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                        <span class="breadcrumb-item active">User Information</span>
                    </nav>
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <form method="POST" id="profileInfo" action="{{ route('userprofile') }}"
                          accept-charset="UTF-8" class="py-4" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="col-lg-2">
                            <figure class="propic">
                                @if(($user) && ($user->image))
                                    <div class="imagediv{{ $user->id }}">
                                        <img src="/uploads/users/thumbs/{{$user->image}}"/>
                                        <a href="#" id="{{$user->id}}"

                                           class="delete_user_image btn btn-danger "
                                           onclick="return confirm(&quot;Confirm delete?&quot;)">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                            Delete
                                        </a>

                                    </div>
                            @endif
                            <!-- file  input-->
                                <div class="input-file-container">
                                    <table class="inputfile">
                                        <tr>
                                            <td>
                                                <input class="input-file" id="my-file"
                                                       type="file" name="image">
                                                <label tabindex="0" for="my-file"
                                                       class="input-file-trigger"><i
                                                            class="fas fa-camera"></i> Upload
                                                    Image</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <p class="file-return"></p>
                                <!--/ file input -->
                            </figure>
                        </div>


                        <?php

//                            dump($user);

                        ?>

                        <div class="box">
                            <div class="box-inner">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label>Name</label>
                                        <input type="text" placeholder="Name"
                                               class="form-control" name="name"
                                               value="{{ !empty(old('name')) ? old('name') : ((($user) && ($user->name)) ? $user->name : '') }}">
                                        @if ($errors->has('name'))
                                            <span class="text-danger help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group col-sm-6">
                                        <label>Your Gender</label>
                                        <label class="pr-3 pt-0">
                                            <input type="radio" name="gender"
                                                   value="Male" {{ (!empty(old('gender')) && old('gender')=="Male")  ? 'selected' : ((($user) && ($user->gender == "Male")) ? 'checked' : '') }}><span
                                                    class="px-2">Male</span></label>
                                        <label class="pr-3 pt-0">
                                            <input type="radio" name="gender"
                                                   value="Female" {{ (!empty(old('gender')) && old('gender')=="Female")  ? 'selected' : ((($user) && ($user->gender == "Female")) ? 'checked' : '') }}><span
                                                    class="px-2">Female</span></label>
                                        @if ($errors->has('gender'))
                                            <span class="text-danger help-block">{{ $errors->first('gender') }}</span>
                                        @endif
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group col-sm-6">
                                        <label>Date of Birth</label>
                                        <input type="text" placeholder="Date Of Birth"
                                               class="form-control datepicker" name="dob"
                                               value="{{ !empty(old('dob')) ? old('dob') : ((($user) && ($user->dob)) ? \Carbon\Carbon::parse($user->dob)->format('d-m-Y') : '') }}">
                                        @if ($errors->has('dob'))
                                            <span class="text-danger help-block">{{ $errors->first('dob') }}</span>
                                        @endif
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group col-sm-6">
                                        <label>Phone Number</label>
                                        <div class="row">

                                            <div class="col-2">
                                                <input type="text" placeholder="Mobile Prefic"
                                                       class="form-control" name="mobile_prefix"
                                                       value="{{ !empty(old('mobile_prefix')) ? old('mobile_prefix') : ((
                                                   ($user) && ($user->mobile_prefix)) ? $user->mobile_prefix : '') }}" />
                                            </div>
                                            <div class="col-10">
                                                <input type="text" placeholder="Mobile No"
                                                       class="form-control" name="mobile"
                                                       value="{{ !empty(old('mobile')) ? old('mobile') : ((($user) &&
                                                   ($user->mobile)) ? $user->mobile : '') }}" />
                                                @if ($errors->has('mobile'))
                                                    <span class="text-danger help-block">{{ $errors->first('mobile') }}</span>
                                                @endif
                                            </div>

                                        </div>




                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label>Email Address</label>
                                        <input type="text" placeholder="Email"
                                               class="form-control" name="email" readonly
                                               value="{{ !empty(old('email')) ? old('email') : ((($user) && ($user->email)) ? $user->email : '') }}">
                                        @if ($errors->has('email'))
                                            <span class="text-danger help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>


                                    <div class="form-group col-sm-6">
                                        <label>About Yourself</label>
                                        <textarea type="text" placeholder="Description"
                                               class="form-control" name="description"
                                                  >{{ !empty(old('description')) ? old('description') : ((($user) && ($user->description)) ? $user->description : '') }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="text-danger help-block">{{ $errors->first('description') }}</span>
                                        @endif
                                    </div>


                                    <!-- /.form-group -->
                                </div>
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                            <!-- /.box-inner -->
                        </div>
                        <!-- /.box -->
                    </form>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

@endsection




@section('footerScripts')

    <script type="text/javascript">
        $(function(){
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>

@endsection