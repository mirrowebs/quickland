@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')

    <div class="main-wrapper">
        <div class="main">

            <div class="main-inner">
                <div class="posting-header-title">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                </div>
                <!-- /.page-title -->
                <div class="container">
                    <br/>
                    <nav class="breadcrumb">
                        <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                        <a class="breadcrumb-item" href="{{ route('userproperties') }}">Properties</a>
                        <span class="breadcrumb-item active">Basic Details</span>
                    </nav>
                    <div class="row">
                        <div class="col-md-2 col-lg-2">
                            <div class="sidebar">
                                <div class="widget">
                                    <div class="box">
                                        <div class="box-inner">
                                            <div class="box-title">
                                                <h3>Overview</h3>
                                            </div>


                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'basic','propertyid'=>$properties->property_id]) }}">Basic
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'propertyDetails','propertyid'=>$properties->property_id]) }}">Property
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'location','propertyid'=>$properties->property_id]) }}">Location</a>


                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'pricing','propertyid'=>$properties->property_id]) }}">Pricing
                                                        Details</a>

                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'features','propertyid'=>$properties->property_id]) }}">Features</a>


                                                </li>
                                            </ul>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 col-lg-10">

                            <?php
//                            dump($properties)
                            ?>

                            <form method="POST" id="ListingAddStep_1" action="{{ route('userpropertyDetails',['type'=>'basic','propertyid'=>$properties->property_id]) }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                <div class="row">


                                    <div class="col-md-8 youAre">
                                        @if(count(listingOptions('youAre'))>0)
                                            <div class="box">
                                                <div class="box-inner">

                                                    <div class="box-title">
                                                        <h2>Are You</h2>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group ">

                                                                @foreach(listingOptions('youAre') AS $youAre_key => $youAre_value)
                                                                    <div class="form-check">
                                                                        <label class="form-check-label">
                                                                            <div class="radio-wrapper">
                                                                                <input type="radio"  <?php echo ($properties->property_by==$youAre_key)?'checked':'' ?>
                                                                                       class="form-check-input"
                                                                                       name="basic[youAre]"
                                                                                       value="{{$youAre_key}}"/>
                                                                                <span class="indicator"></span>
                                                                            </div>
                                                                            {{$youAre_value}}
                                                                        </label>
                                                                    </div>
                                                                @endforeach
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    {{--<div class="col-md-12 userInfo_box" style="">
                                        @if(count(listingOptions('userInfo'))>0)
                                            <div class="box">
                                                <div class="box-inner">

                                                    <div class="box-title">
                                                        <h2>About Yourself</h2>
                                                    </div>

                                                    @foreach(listingOptions('userInfo') AS $userInfo_key => $userInfo)

                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <div class="form-check">
                                                                        <label class="form-check-label">
                                                                            {{$userInfo}}
                                                                        </label>
                                                                        <input class="form-control" value="{{
                                                                        (Auth::check())?    Auth::user()->$userInfo_key:''  }}"
                                                                               name="basic[{{$userInfo_key}}]"/>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>


                                                        --}}{{--{{ dump($list_key_1)  }}--}}{{--
                                                        --}}{{--{{ dump($list_1)  }}--}}{{--
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    </div>--}}


                                    <div class="col-md-12 propertyFor">
                                        @if(count(listingOptions('propertyFor'))>0)
                                            <div class="box">
                                                <div class="box-inner">
                                                    <div class="box-title">
                                                        <h2>Property For</h2>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group ">

                                                                @foreach(listingOptions('propertyFor') AS $propertyFor_key => $propertyFor_value)
                                                                    <div class="form-check">
                                                                        <label class="form-check-label">
                                                                            <div class="radio-wrapper">
                                                                                <input type="radio"
                                                                                       {{($properties->property_for==$propertyFor_key)?'checked':''}}
                                                                                       class="form-check-input"
                                                                                       name="basic[propertyFor]"
                                                                                       value="{{ $propertyFor_key }}"/>
                                                                                <span class="indicator"></span>
                                                                            </div>
                                                                            {{$propertyFor_key}}
                                                                        </label>
                                                                    </div>

                                                                @endforeach
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>


                                </div>


                                <div class="col-md-12 propertyTypeBox">


                                </div>
                                <div class="col-md-12 propertyTypeOptionsBox">

                                </div>

                                <div class="row">
                                    <div class="col-md-12 propertyTypeOptionsSelectedBox">

                                    </div>
                                </div>


                                <div class="next-prev">
                                    <!-- <div class="prev">
                                      <a href="#" class="btn btn-secondary">Return Back</a>
                                    </div> -->
                                    <!-- /.prev -->
                                    <div class="next">
                                        {{--<a href="{{route('ListingAdd', ['step'=>2])}}"--}}
                                        {{--class="btn btn-primary">CONTINUE</a>--}}

                                        <input type="submit" name="submit" value="Update Now" class="btn btn-primary">

                                    </div>
                                    <!-- /.next -->
                                </div>
                                <!-- /.next-prev -->
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection




@section('footerScripts')

    @include('frontend.listing.script');



    <script type="text/javascript">
        $(function () {



            $('#ListingAddStep_1').validate({
                ignore: [],
                errorElement: 'span',
                errorClass: 'label text-danger',
                errorPlacement: function (error, element) {
                    // console.log(element.closest('.form-group').attr('class'))

                    if (element.closest('.form-group').length) {
                        error.insertAfter(element.closest('.form-group'));
                    } else {
                        error.insertAfter(element);
                    }
                },


                rules: {
                    "basic[youAre]": {
                        required: true
                    },
                    "basic[propertyFor]": {
                        required: true
                    },
                    "basic[propertyType]": {
                        required: true
                    },
                    "basic[propertyTypeOtp]": {
                        required: true
                    }
                },
                messages: {
                    "basic[youAre]": {
                        required: "This field is required"
                    },
                    "basic[propertyFor]": {
                        required: "This field is required"
                    },
                    "basic[propertyType]": {
                        required: "This field is required"
                    },
                    "basic[propertyTypeOtp]": {
                        required: "This field is required"
                    }
                },
                submitHandler: function (form) { // for demo
                    // alert('valid form submitted'); // for demo


                    // console.log('validate sss');


                    return true; // for demo

                    // $(form).submit();

                }


            });




            if ($('.propertyFor .form-check-input').is(":checked")) {

                // console.log($('.propertyFor .form-check-input').is(":checked").val())

                var propertyFor = $(".propertyFor .form-check-input:checked").map(
                    function () {
                        return this.value;
                    }).get().join(",");

                console.log(propertyFor);

                curentValues = [];
                curentValues.push({
                    property_for: '{{$properties->property_for}}',
                    property_by: '{{$properties->property_by}}',
                    property_type: '{{$properties->property_type}}',
                    property_sub_type: '{{$properties->property_sub_type}}'
                });


                if (curentValues.length > 0) {

                    getPropertyFor(propertyFor, curentValues[0])

                }


                // var values = $('.propertyFor .form-check-input').map(function(i,el){
                //     if($(el).is(':checked')){
                //         return $(el).val()
                //     }
                // }).get();
                //
                // console.log(values.first())

            }


            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>

@endsection