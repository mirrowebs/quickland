@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')


    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container">
                        <h1>My Listings
                        </h1>
                        <div class="page-title-actions">
                            <div class="switcher">
                                <strong>Currency</strong>
                                <ul>
                                    <li class="active"><a href="#">USD</a></li>
                                    <li><a href="#">EUR</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                            <div class="switcher">
                                <strong>Language</strong>
                                <ul>
                                    <li class="active"><a href="#">EN</a></li>
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">DE</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                        </div>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-->
                </div>
                <!-- /.page-title -->
                <div class="container">
                    <nav class="breadcrumb">
                        <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                        <span class="breadcrumb-item active">Properties</span>
                    </nav>
                    <!-- /.table-header -->
                    <div class="table-wrapper">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="min-width center">#</th>
                                <th class="min-width center">Property For</th>
                                <th class="min-width center">Property By</th>
                                <th class="min-width center">Property Type</th>
                                <th class="min-width center">Property Sub Type</th>
                                <th class="min-width center">Status</th>
                                <th class="min-width"></th>
                            </tr>
                            </thead>

                            <tbody>
                            @if(count($properties)>0)
                                @foreach($properties as $item)
                                    <tr>
                                        <td class="min-width center"> {{ $loop->iteration }}</td>
                                        <td class="min-width center"> {{ $item->property_for }}</td>
                                        <td class="min-width center"> {{ $item->property_by }}</td>
                                        <td class="min-width center"> {{ $item->property_type }}</td>
                                        <td class="min-width center"> {{ $item->property_sub_type }}</td>
                                        <td class="min-width no-wrap center">
                                            <span class="tag">{{ $item->property_status }}</span>
                                        </td>

                                        <td class="min-width">



                                            <div class="dropdown">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    Manage
                                                </button>
                                                <div class="dropdown-menu">



                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'basic','propertyid'=>$item->property_id]) }}">Basic
                                                        Details</a>
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'propertyDetails','propertyid'=>$item->property_id]) }}">Property
                                                        Details</a>
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'location','propertyid'=>$item->property_id]) }}">Location</a>
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'pricing','propertyid'=>$item->property_id]) }}">Pricing
                                                        Details</a>
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'features','propertyid'=>$item->property_id]) }}">Features</a>



                                                    <a class="dropdown-item" href="#">

                                                        <form method="POST" id="userproperties"
                                                              action="{{ route('userproperties') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="property_id"
                                                                   value="{{ $item->property_id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Property"
                                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i
                                                                        class="fa fa-trash"></i> Delete
                                                            </button>

                                                        </form>

                                                    </a>
                                                </div>
                                            </div>



                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="11">No records found</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-wrapper -->
                    <div class="row">
                        <div class="col-md-10">
                            <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                 aria-live="polite">Showing {{ $properties->firstItem() }}
                                to {{ $properties->lastItem() }} of {{ $properties->total() }} entries
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                {!! $properties->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

@endsection




@section('footerScripts')

    <script type="text/javascript">
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>

@endsection