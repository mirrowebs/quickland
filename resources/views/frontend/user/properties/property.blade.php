@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')


    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                <div class="posting-header-title">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                </div>

                <div class="container">


                    <div class="row">

                        <div class="col-md-2 col-lg-2">
                            <div class="sidebar">
                                <div class="widget">
                                    <div class="box">
                                        <div class="box-inner">
                                            <div class="box-title">
                                                <h3>Overview</h3>
                                            </div>


                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'basic','propertyid'=>$properties->property_id]) }}">Basic
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'propertyDetails','propertyid'=>$properties->property_id]) }}">Property
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'location','propertyid'=>$properties->property_id]) }}">Location</a>


                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'pricing','propertyid'=>$properties->property_id]) }}">Pricing
                                                        Details</a>

                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'features','propertyid'=>$properties->property_id]) }}">Features</a>


                                                </li>
                                            </ul>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 col-lg-10">

                            <?php
                            $data = new App\Http\Controllers\Options\PropertyFiled();

//                            dump(\Illuminate\Support\Facades\Session::get('formData'));

                            ?>


                            <form method="post" action="{{ route('userpropertyDetails',['type'=>'propertyDetails','propertyid'=>$properties->property_id]) }}">
                                {{ csrf_field() }}
                                {{--                                {!! $data->getPropertyTypeOptionSelected($options_step2) !!}--}}




                                {!!  $data->propertyDetails($options_form_hidden, $properties->propertyOptions) !!}

                                {!!  $data->hiddenFiledsRender($options_form_hidden) !!}


                                <input type="hidden" name="hidden[propertyFor]"
                                       value="{{$options_form_hidden['propertyFor']}}"/>
                                <input type="hidden" name="hidden[youAreOtp]"
                                       value="{{$options_form_hidden['youAreOtp']}}"/>
                                <input type="hidden" name="hidden[optionsSelected]"
                                       value="{{$options_form_hidden['optionsSelected']}}"/>

                                <div class="next-prev">
                                    <div class="prev">

                                    </div>
                                    <!-- /.prev-->
                                    <div class="next">
                                        <input type="submit" name="submit" value="Update" class="btn btn-primary">

                                    </div>
                                    <!-- /.next -->
                                </div>

                            </form>


                        </div>

                    </div>
                    <!-- /.row  -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>


@endsection




@section('footerScripts')

    <script type="text/javascript">
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>

@endsection