@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 500px;
            width: 500px;
        }

        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto', 'sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        #floating-panel {
            position: absolute;
            top: 5px;
            left: 50%;
            margin-left: -180px;
            width: 350px;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
        }

        #latlng {
            width: 225px;
        }
    </style>
@endsection




@section('content')


    <?php
    //    dump($properties->propertyOptions)
    ?>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                <div class="posting-header-title">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                </div>
                <!-- /.page-title -->
                <div class="container">

                    <br/>
                    <nav class="breadcrumb">
                        <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                        <a class="breadcrumb-item" href="{{ route('userproperties') }}">Properties</a>
                        <span class="breadcrumb-item active">Location Details</span>
                    </nav>


                    <div class="row">


                        <div class="col-md-2 col-lg-2">
                            <div class="sidebar">
                                <div class="widget">
                                    <div class="box">
                                        <div class="box-inner">
                                            <div class="box-title">
                                                <h3>Overview</h3>
                                            </div>


                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'basic','propertyid'=>$properties->property_id]) }}">Basic
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'propertyDetails','propertyid'=>$properties->property_id]) }}">Property
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'location','propertyid'=>$properties->property_id]) }}">Location</a>


                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'pricing','propertyid'=>$properties->property_id]) }}">Pricing
                                                        Details</a>

                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'features','propertyid'=>$properties->property_id]) }}">Features</a>


                                                </li>
                                            </ul>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 col-lg-9">

                            <div class="col-md-12">


                                <div id="map" style="width: 800px; height: 500px"></div>

                            </div>


                            <div class="col-md-12">

                                <input class="form-control" id="autocomplete" type="text"/>
                            </div>


                            <?php
                            $data = new App\Http\Controllers\Options\PropertyFiled();

                            //                            dump(\Illuminate\Support\Facades\Session::get('formData'));

                            ?>





                            <form method="post"
                                  action="{{ route('userpropertyDetails',['type'=>'location','propertyid'=>$properties->property_id]) }}">
                                {{ csrf_field() }}

                                {!!  $data->locationOpt($options_form_hidden, $properties->propertyOptions) !!}
                                {!!  $data->hiddenFiledsRender($options_form_hidden) !!}



                                <?php
                                dump($properties->propertyOptions);
                                ?>

                                <div class="next-prev">
                                    <div class="prev">
                                        {{--<input class="btn btn-secondary" onclick="window.history.go(-1); return false;" type="button" value="Return Back" />--}}
                                    </div>
                                    <!-- /.prev-->
                                    <div class="next">
                                        <input type="submit" name="submit" value="Update" class="btn btn-primary">
                                    </div>
                                    <!-- /.next -->
                                </div>

                            </form>

                            <!-- /.next-prev -->
                        </div>

                    </div>
                    <!-- /.row  -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>

@endsection




@section('footerScripts')

    {{--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAjU0EJWnWPMv7oQ-jjS7dYxSPW5CJgpdgO_s4yyMovOaVh_KvvhSfpvagV18eOyDWu7VytS6Bi1CWxw"--}}
    {{--type="text/javascript"></script>--}}


    {{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>--}}

    {{--<script async defer  src="https://maps.googleapis.com/maps/api/js?&signed_in=true&&libraries=places"></script>--}}
    {{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOWcwE_TknuLwOZNVcl5WpSFOk-4DObVE&callback=initMap">  </script>--}}


    @include('includs.googleMapScript')






@endsection