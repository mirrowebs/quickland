@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="posting-header-title">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                </div>
                <!-- /.page-title -->
                <div class="container">

                    <br/>
                    <nav class="breadcrumb">
                        <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                        <a class="breadcrumb-item" href="{{ route('userproperties') }}">Properties</a>
                        <span class="breadcrumb-item active">Features</span>
                    </nav>

                    <div class="row">
                        <div class="col-md-2 col-lg-2">
                            <div class="sidebar">
                                <div class="widget">
                                    <div class="box">
                                        <div class="box-inner">
                                            <div class="box-title">
                                                <h3>Overview</h3>
                                            </div>


                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'basic','propertyid'=>$properties->property_id]) }}">Basic
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'propertyDetails','propertyid'=>$properties->property_id]) }}">Property
                                                        Details</a>


                                                </li>
                                                <li class="list-group-item">

                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'location','propertyid'=>$properties->property_id]) }}">Location</a>


                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'pricing','propertyid'=>$properties->property_id]) }}">Pricing
                                                        Details</a>

                                                </li>
                                                <li class="list-group-item">
                                                    <a class="dropdown-item"
                                                       href="{{ route('userpropertyDetails',['type'=>'features','propertyid'=>$properties->property_id]) }}">Features</a>


                                                </li>
                                            </ul>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 col-lg-9">

                            <?php
                            $data = new App\Http\Controllers\Options\PropertyFiled();

                            ?>


                            <form method="post"
                                  action="{{ route('userpropertyDetails',['type'=>'features','propertyid'=>$properties->property_id]) }}"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="box">
                                    <div class="box-inner">

                                        <div class="box-title">
                                            <h2> Gallery </h2>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                @if(count($property_images)>0)
                                                    @foreach($property_images as $item)
                                                        <div class="col-md-2 imagediv{{ $item->pi_id }}">
                                                            <img class="img-responsive"
                                                                 src="/uploads/properties/thumbs/{{ $item->pi_image }}"/>
                                                            <a href="#" id="{{$item->pi_id}}"
                                                               class="delete_property_image btn btn-danger btn-xs"
                                                               onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                                        class="fa fa-trash-o" aria-hidden="true"></i>Delete
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="col-md-2">
                                                        No records found
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">

                                                    <input name="propertyimages[]" multiple type="file"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-inner">

                                        <div class="box-title">
                                            <h2> Description </h2>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">

                                                    <textarea class="form-control editor"
                                                              name="features[description]"><?=  $properties->propertyOptions[0]->po_description ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                {!!  $data->Features($options_form_hidden, $properties->propertyOptions) !!}


                                <div class="next-prev">
                                    <div class="prev">

                                    </div>
                                    <!-- /.prev-->
                                    <div class="next">
                                        <input type="submit" name="submit" value="update" class="btn btn-primary"/>
                                    </div>
                                    <!-- /.next -->
                                </div>

                            </form>

                            <!-- /.next-prev -->
                        </div>

                    </div>
                    <!-- /.row  -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>


@endsection




@section('footerScripts')

    <script type="text/javascript">
        $(function () {

            $('.delete_property_image').on('click', function () {
                var image = $(this).attr('id');
                if (image != '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '/user/ajax/deletepropertyimage',
                        type: 'POST',
                        data: {'image': image},
                        success: function (response) {
                            $('.imagediv' + image).remove();
                        }
                    });
                }
            });

            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>

@endsection