{{--<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Lolokplease - Admin">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('backend._partials.stylesheets')
    @yield('headerStyles')
</head>
<body>
@include('backend._partials.leftsidemenu')
<div id="right-panel" class="right-panel">
@include('backend._partials.header')
@yield('content')
<!-- add content here -->
</div>
<!-- end page container -->
@include('backend._partials.footer')
@include('backend._partials.scripts')
@yield('footerScripts')
</body>--}}


        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">


    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Lolokplease - Admin">

    @include('frontend._partials.stylesheets')

    @yield('headerStyles')

</head>
<body class="">
<div class="page-wrapper">
@include('frontend._partials.headder')


@yield('content')


@include('frontend._partials.footer')

<!-- /.footer-wrapper -->
</div>


@include('frontend._partials.scripts')

@yield('footerScripts')


</body>
</html>