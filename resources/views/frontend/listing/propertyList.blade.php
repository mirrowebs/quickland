@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')

@endsection




@section('content')

    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container-fluid">
                        <h1>Listings Grid
                        </h1>
                        <div class="page-title-actions">
                            <div class="switcher">
                                <strong>Currency</strong>
                                <ul>
                                    <li class="active"><a href="#">USD</a></li>
                                    <li><a href="#">EUR</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                            <div class="switcher">
                                <strong>Language</strong>
                                <ul>
                                    <li class="active"><a href="#">EN</a></li>
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">DE</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                        </div>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.page-title -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-md-3 sidebar-wrapper-col">
                            <div class="sidebar">
                                <div class="filter">
                                    <h2>Search criteria</h2>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input type="text" class="form-control" placeholder="Search by keyword ...">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>City</label>
                                        <select class="form-control">
                                            <option>Location</option>
                                            <option>Chicago</option>
                                            <option>San Francisco</option>
                                            <option>Seattle</option>
                                            <option>New York</option>
                                            <option>Washington</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Price Slider</label>
                                        <div id="slider" class="price-slider"></div>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Amenities</label>
                                        <select class="form-control">
                                            <option>Air conditioning</option>
                                            <option>Balcony</option>
                                            <option>Bedding</option>
                                            <option>Cable TV</option>
                                            <option>Cleaning after exit</option>
                                            <option>Cofee pot</option>
                                            <option>Computer</option>
                                            <option>Cot</option>
                                            <option>Dishwasher</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="row">
                                        <div class="form-group col-xl-6">
                                            <label>Price From</label>
                                            <input type="text" class="form-control" placeholder="Min. price">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group col-xl-6">
                                            <label>Price To</label>
                                            <input type="text" class="form-control" placeholder="Max. price">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.row -->
                                    <h2>Category</h2>
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="rent"> Property
                                            <span class="label">78</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="sale"> Restaurant
                                            <span class="label">54</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="sale"> Travel <span
                                                    class="label">50</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="lease"> Food &amp;
                                            Drink <span class="label">32</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="lease"> Events <span
                                                    class="label">53</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <h2>Attributes</h2>
                                    <div class="checkbox">
                                        <label><input type="checkbox"> Reduced <span class="label">93</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="checkbox"> Featured <span class="label">54</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="checkbox"> Verified <span class="label">22</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="form-group-btn form-group-btn-placeholder-gap">
                                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.filter -->
                            </div>
                            <!-- /.sidebar -->
                        </div>
                        <!-- /.col-* -->
                        <div class="col-sm-8 col-md-9">
                            <div class="content">
                                <div class="row">


                                    @if(count($properties)>0)
                                        @foreach($properties as $property)

                                            <?php

                                            //                                            dd($property);

                                            $imagesData = \App\Models\PropertyImages::getPropertyImages($property->property_id);




                                            if (isset($imagesData[0]['thumb'])) {
                                                $imageUrl = $imagesData[0]['thumb'];
                                            } else {
                                                $imageUrl = '';
                                            }



                                            ?>

                                            <div class="col-md-6 col-lg-3">
                                                <div class="listing-box">
                                                    <div class="listing-box-inner">
                                                        <a href="{{ route('propertyList',['id'=>$property->property_id]) }}"
                                                           class="listing-box-image">
                                                        <span class="listing-box-image-content"
                                                              style="background: url('{{ $imageUrl }}')"></span>
                                                            <!-- /.listing-box-image-content -->
                                                            <span class="listing-box-category tag">{{ $property->property_type }}</span>
                                                            <span class="listing-box-rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            </span>
                                                        </a><!-- /.listing-box-image -->
                                                        <div class="listing-box-content">
                                                            <h2>
                                                                <a href="{{ route('propertyList',['id'=>$property->property_id]) }}">{{ $property->property_sub_type }}</a>
                                                            </h2>
                                                            <h3>{{ $property->property_by }}</h3>
                                                            <div class="actions">
                                                                <div class="actions-button">
                                                                    <span></span>
                                                                    <span></span>
                                                                    <span></span>
                                                                </div>

                                                            </div>
                                                            <!-- /.actions -->
                                                        </div>

                                                    </div>
                                                    <!-- /.listing-box-inner -->
                                                </div>
                                                <!-- /.listing-box -->
                                            </div>
                                        @endforeach

                                    @else

                                        No properties

                                    @endif
                                </div>
                                @if(count($properties)>0)

                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="dataTables_info p-t-10" id="editable-datatable_info"
                                                 role="status"
                                                 aria-live="polite">Showing {{ $properties->firstItem() }}
                                                to {{ $properties->lastItem() }} of {{ $properties->total() }} entries
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                 id="editable-datatable_paginate">
                                                {!! $properties->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <!-- /.content -->
                        </div>
                        <!-- /.col-* -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

@endsection




@section('footerScripts')

@endsection