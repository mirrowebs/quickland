@extends('frontend.layout')
@section('title', $title)



@section('headerStyles')

@endsection





@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="posting-header-title">
                    <div class="container">
                        <h1>If you are existing user please</h1>
                        <a href="#">Login</a>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-->
                </div>


                <div class="container">

                    @include('frontend.listing.add.progress_menu')

                    <div class="row">

                        <div class="col-md-2 col-lg-2">
                            <div class="sidebar">
                                <div class="widget">
                                    <div class="box">
                                        <div class="box-inner">
                                            <div class="box-title">
                                                <h3>Overview</h3>
                                            </div>


                                            <p>Display Selection Options</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 col-lg-10">

                            <?php
                            $data = new App\Http\Controllers\Options\PropertyFiled();

                            dump(\Illuminate\Support\Facades\Session::get('formData'));

                            ?>


                            <form method="post" action="{{ route('ListingAdd',['step'=>3]) }}">
                                {{ csrf_field() }}
                                {{--                                {!! $data->getPropertyTypeOptionSelected($options_step2) !!}--}}

                                {!!  $data->propertyDetails($options_form_hidden) !!}

                                {!!  $data->hiddenFiledsRender($options_form_hidden) !!}


                                <input type="hidden" name="hidden[propertyFor]" value="{{$options_form_hidden['propertyFor']}}"/>
                                <input type="hidden" name="hidden[youAreOtp]" value="{{$options_form_hidden['youAreOtp']}}"/>
                                <input type="hidden" name="hidden[optionsSelected]" value="{{$options_form_hidden['optionsSelected']}}"/>

                                <div class="next-prev">
                                    <div class="prev">
                                        <input class="btn btn-secondary" onclick="window.history.go(-1); return false;" type="button" value="Return Back" />
                                    </div>
                                    <!-- /.prev-->
                                    <div class="next">
                                        <input type="submit" name="submit" value="CONTINUE" class="btn btn-primary">

                                        {{--<a href="{{route('ListingAdd', ['step'=>3])}}"--}}
                                           {{--class="btn btn-primary">CONTINUE</a>--}}
                                    </div>
                                    <!-- /.next -->
                                </div>

                            </form>


                        </div>

                    </div>
                    <!-- /.row  -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>

@endsection


@section('footerScripts')
    @include('frontend.listing.script')

    <script>
        $(function () {

        });

    </script>

@endsection
