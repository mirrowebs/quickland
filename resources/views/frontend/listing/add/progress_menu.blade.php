<div class="progress-steps">
    <div class="row">
        <div class="col-md-2">
            <a class="progress-step {{($sub_active_menu=='add_step_1')?'done':''}}" href="#">
                <i class="fa fa-check"></i>
                <span class="title ">Basic Details</span>
            </a>
        </div>
        <!-- /.col -->
        <div class="col-md-2">
            <a class="progress-step  {{($sub_active_menu=='add_step_2')?'done':''}}" href="#">
                <i class="fa fa-check"></i>
                <span class="title">Property Details</span>
            </a>
        </div>
        <!-- /.col -->
        <div class="col-md-2">
            <a class="progress-step {{($sub_active_menu=='add_step_3')?'done':''}}" href="#">
                <i class="fa fa-check"></i>
                <span class="title">Location</span>
            </a>
        </div>
        <!-- /.col -->
        <div class="col-md-2">
            <a class="progress-step {{($sub_active_menu=='add_step_4')?'done':''}}" href="#">
                <i class="fa fa-check"></i>
                <span class="title">Pricing</span>
            </a>
        </div>
        <!-- /.col -->
        <div class="col-md-2">
            <a class="progress-step {{($sub_active_menu=='add_step_5')?'done':''}}" href="#">
                <i class="fa fa-check"></i>
                <span class="title">Features</span>
            </a>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>