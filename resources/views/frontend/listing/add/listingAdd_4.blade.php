@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="posting-header-title">
                </div>
                <!-- /.page-title -->
                <div class="container">

                    @include('frontend.listing.add.progress_menu')


                    <div class="row">
                        <div class="col-md-8 col-lg-9">

                            <?php
                            $data = new App\Http\Controllers\Options\PropertyFiled();

                            dump(\Illuminate\Support\Facades\Session::get('formData'));

                            ?>


                            <form method="post" action="{{ route('ListingAdd',['step'=>5]) }}">

                                {{ csrf_field() }}


                                {!!  $data->hiddenFiledsRender($options_form_hidden) !!}

                                {!!  $data->Pricing($options_form_hidden) !!}


                                <div class="next-prev">
                                    <div class="prev">
                                        <input class="btn btn-secondary" onclick="window.history.go(-1); return false;" type="button" value="Return Back" />
                                    </div>
                                    <!-- /.prev-->
                                    <div class="next">
                                        <input type="submit" name="submit" value="CONTINUE" class="btn btn-primary"/>
                                    </div>
                                    <!-- /.next -->
                                </div>

                            </form>

                            <!-- /.next-prev -->
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="sidebar">
                                <div class="widget">
                                    <div class="box">
                                        <div class="box-inner">
                                            <div class="box-title">
                                                <h3>Overview</h3>
                                            </div>
                                            <!-- /.box-title -->
                                            <p>Display Selection Options</p>
                                        </div>
                                        <!-- /.box-inner -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.widget -->
                            </div>
                            <!-- /.sidebar -->
                        </div>
                    </div>
                    <!-- /.row  -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

@endsection




@section('footerScripts')



@endsection