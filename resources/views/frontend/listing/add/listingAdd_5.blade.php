@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="posting-header-title">
                </div>
                <!-- /.page-title -->
                <div class="container">

                    @include('frontend.listing.add.progress_menu')


                    <div class="row">
                        <div class="col-md-8 col-lg-9">

                            <?php
                            $data = new App\Http\Controllers\Options\PropertyFiled();

                            dump(\Illuminate\Support\Facades\Session::get('formData'));

                            ?>


                            <form method="post" action="{{ route('ListingAdd',['step'=>6]) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="box">
                                    <div class="box-inner">

                                        <div class="box-title">
                                            <h2> Gallery </h2>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">

                                                    <input name="propertyimages[]" multiple type="file"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box-inner">

                                        <div class="box-title">
                                            <h2> Description </h2>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">

                                                    <textarea class="form-control editor"
                                                              name="features[description]"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            {!!  $data->Features($options_form_hidden) !!}

                            {{--<!-- /.box -->--}}
                            {{--<div class="box location-posting">--}}
                            {{--<div class="box-inner">--}}
                            {{--<div class="box-title">--}}
                            {{--<h2>Upload Property Pictures</h2>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-title -->--}}
                            {{--<div class="row">--}}
                            {{--<div class="col">--}}
                            {{--<div class="form-group">--}}
                            {{--<label for="exampleInputFile">Upload Pictures</label>--}}
                            {{--<input type="file" class="form-control-file" id="exampleInputFile"--}}
                            {{--aria-describedby="fileHelp">--}}
                            {{--<small id="fileHelp" class="form-text text-muted">This is some--}}
                            {{--placeholder block-level help text for the above input. It's a--}}
                            {{--bit lighter and easily wraps to a new line.--}}
                            {{--</small>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.col -->--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-inner -->--}}
                            {{--</div>--}}
                            <!-- /.box -->

                                <div class="next-prev">
                                    <div class="prev">

                                        <input class="btn btn-secondary" onclick="window.history.go(-1); return false;" type="button" value="Return Back" />


                                        {{--<a href="{{route('ListingAdd', ['step'=>2])}}" class="btn btn-secondary">Return--}}
                                            {{--Back</a>--}}
                                    </div>
                                    <!-- /.prev-->
                                    <div class="next">
                                        <input type="submit" name="submit" value="CONTINUE" class="btn btn-primary"/>
                                    </div>
                                    <!-- /.next -->
                                </div>

                            </form>

                            <!-- /.next-prev -->
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="sidebar">
                                <div class="widget">
                                    <div class="box">
                                        <div class="box-inner">
                                            <div class="box-title">
                                                <h3>Overview</h3>
                                            </div>
                                            <!-- /.box-title -->
                                            <p>Display Selection Options</p>
                                        </div>
                                        <!-- /.box-inner -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.widget -->
                            </div>
                            <!-- /.sidebar -->
                        </div>
                    </div>
                    <!-- /.row  -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

@endsection




@section('footerScripts')
@endsection