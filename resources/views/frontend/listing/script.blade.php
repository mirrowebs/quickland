<script type="text/javascript">


    function validate_userInfo_box() {
        $('.userInfo_box input').each(function () {
            $(this).rules('add', {
                required: true,

                messages: {
                    required: "This field is required"
                }
                // other rules
            });
        });
    }


    function getPropertyTypeOptionslist(propertyFor, youAreOtp, curentValues=[]) {
        var propertyTypeOptionsdata = { //Fetch form data
            'propertyFor': propertyFor,      //Store name fields value
            'youAreOtp': youAreOtp,     //Store name fields value
            'property_sub_type': curentValues.property_sub_type      //Store name fields value
        };

        $.ajax({ //Process the form using $.ajax()
            type: 'get', //Method type
            url: '{{route('ajaxGetPropertyTypeOptions')}}', //Your form processing file URL
            data: propertyTypeOptionsdata, //Forms name
            success: function (propertyTypeopt) {
                console.log(propertyTypeopt);
                $('.propertyTypeOptionsBox').html(propertyTypeopt).show();
                $('.propertyTypeOptionsSelectedBox').hide();

                /*  $(".propertyTypeOptionSelected input:radio").click(function () {

                      var optionsSelected = $(this).val();

                      var propertyTypeOptionSelectedData = { //Fetch form data
                          'propertyFor': propertyFor,      //Store name fields value
                          'youAreOtp': youAreOtp,      //Store name fields value
                          'optionsSelected': optionsSelected      //Store name fields value
                      };

                      $.ajax({ //Process the form using $.ajax()
                          type: 'get', //Method type
{{--                          url: '{{route('ajaxGetPropertyTypeOptionSelected')}}', //Your form processing file URL--}}
                                        data: propertyTypeOptionSelectedData, //Forms name
                                        success: function (propertyTypeoptSelect) {
                                            console.log(propertyTypeoptSelect);
                                            $('.propertyTypeOptionsSelectedBox').html(propertyTypeoptSelect).show();
                                        }
                                    });

                                });*/


            }
        });

    }
    function getPropertyFor(propertyFor, curentValues=[]) {

      // if(curentValues.length > 0){
      //     curentValues = curentValues[0];
      // }

      console.log(curentValues);

        var propertyForData = { //Fetch form data
            'propertyFor': propertyFor, //Store name fields value
            'propertyFor_Value': curentValues.property_type //Store name fields value
        };

        $.ajax({ //Process the form using $.ajax()
            type: 'get', //Method type
            url: "{{route('ajaxGetPropertyType')}}", //Your form processing file URL
            data: propertyForData, //Forms name
            success: function (propertyType) {
                console.log(propertyType);
                $('.propertyTypeBox').html(propertyType).show();
                $('.propertyTypeOptionsBox').hide();
                $('.propertyTypeOptionsSelectedBox').hide();


                if ($('.propertyTypeOptionslist input:radio').is(":checked")) {
                    var youAreOtp = $(".propertyTypeOptionslist input:radio:checked").map(
                        function () {
                            return this.value;
                        }).get().join(",");

                    console.log(youAreOtp);

                    getPropertyTypeOptionslist(propertyFor, youAreOtp, curentValues);

                }

                $(".propertyTypeOptionslist input:radio").click(function () {

                    var youAreOtp = $(this).val();

                    getPropertyTypeOptionslist(propertyFor, youAreOtp, curentValues);

                });


            }
        });
    }


    $(function () {

    });
</script>