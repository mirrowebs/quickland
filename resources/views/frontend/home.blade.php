@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')
<div class="hero-image-wrapper">
    <div class="hero-image-src" style="background-image: url('/frontend/images/homepage-banner.jpg');">
    </div>
    <!-- /.hero-image-src -->
    <div class="hero-image">
        <div class="hero-image-inner">
            <div class="hero-image-content">
                <div class="container">
                    <div class="center">
                        <h1 class="hero-image-content-title-simple float-none">PROPERTY GROUP FOR TELUGU STATES</h1>
                        <form method="post" action="?">
                            <div id="main-search-tabs">
                                <input type="radio" name="tabs" id="toggle-tab1" checked="checked"/>
                                <label for="toggle-tab1">BUY</label>
                                <input type="radio" name="tabs" id="toggle-tab2"/>
                                <label for="toggle-tab2">RENTAL</label>
                                <input type="radio" name="tabs" id="toggle-tab3"/>
                                <label for="toggle-tab3">COMMERCIAL</label>
                                <input type="radio" name="tabs" id="toggle-tab4"/>
                                <label for="toggle-tab4">PROJECTS</label>
                                <!-- BUY SEARCH SECTION-->
                                <div id="tab1" class="search_tab">
                                    <form method="post" action="?">
                                        <div class="row">
                                            <div class="col-sm-12 ql-search-order">
                                                <div class="ql-search-field">
                                                    <div class="input-group">
                                                            <span class="input-group-addon no-border"><i
                                                                        class="fa fa-location-arrow"></i></span>
                                                        <input type="text"
                                                               class="form-control no-border form-control-xl"
                                                               placeholder="Type your address ...">
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="ql-search-field">
                                                    <div class="form-group">
                                                        <select class="form-control form-control-xl form-control-no-border">
                                                            <option>Chicago</option>
                                                            <option>San Francisco</option>
                                                            <option>Seattle</option>
                                                            <option>New York</option>
                                                            <option>Washington</option>
                                                        </select>
                                                    </div>
                                                    <!-- /.form-group -->
                                                </div>
                                                <div class="ql-search-field search-property">
                                                    <button type="submit" class="btn alert-info">
                                                        <i class="fa fa-search"></i> SEARCH
                                                    </button>
                                                    <button type="submit" class="btn alert-success">
                                                        <i class="fa fa-location-arrow"></i> MAP SEARCH
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- /.col-* -->
                                        </div>
                                        <!-- /.row -->
                                    </form>
                                </div>
                                <!-- END BUY SEARCH SECTION-->
                                <!-- BUY SEARCH SECTION-->
                                <div id="tab2" class="search_tab">
                                    <form method="post" action="?">
                                        <div class="row">
                                            <div class="col-sm-12 ql-search-order">
                                                <div class="ql-search-field">
                                                    <div class="input-group">
                                                            <span class="input-group-addon no-border"><i
                                                                        class="fa fa-location-arrow"></i></span>
                                                        <input type="text"
                                                               class="form-control no-border form-control-xl"
                                                               placeholder="Type your address ...">
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="ql-search-field">
                                                    <div class="form-group">
                                                        <select class="form-control form-control-xl form-control-no-border">
                                                            <option>Chicago</option>
                                                            <option>San Francisco</option>
                                                            <option>Seattle</option>
                                                            <option>New York</option>
                                                            <option>Washington</option>
                                                        </select>
                                                    </div>
                                                    <!-- /.form-group -->
                                                </div>
                                                <div class="ql-search-field search-property">
                                                    <button type="submit" class="btn alert-info">
                                                        <i class="fa fa-search"></i> SEARCH
                                                    </button>
                                                    <button type="submit" class="btn alert-success">
                                                        <i class="fa fa-location-arrow"></i> MAP SEARCH
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- /.col-* -->
                                        </div>
                                        <!-- /.row -->
                                    </form>
                                </div>
                                <!-- END BUY SEARCH SECTION-->

                                <div id="tab3" class="search_tab">
                                    Suspendisse volutpat risus faucibus, luctus sem id, egestas risus.
                                </div>

                                <div id="tab4" class="search_tab">
                                    Sed tempus ante vitae orci fringilla, sit amet posuere mi imperdiet.
                                </div>

                            </div>
                            <!-- /.row -->
                        </form>
                    </div>
                </div>
                <!-- /.container -->
            </div>
            <!-- /.hero-image-content -->
        </div>
        <!-- /.hero-image-inner -->
        <div class="hero-image-categories">
            <a href="#" class="hero-image-category">
                <i class="fa fa-commenting-o"></i>
                <strong>Clubs</strong>
            </a><!-- /.hero-image-category -->
            <a href="#" class="hero-image-category">
                <i class="fa fa-snowflake-o"></i>
                <strong>Sky Resorts</strong>
            </a><!-- /.hero-image-category -->
            <a href="#" class="hero-image-category">
                <i class="fa fa-star-o"></i>
                <strong>Bestsellers</strong>
            </a><!-- /.hero-image-category -->
            <a href="#" class="hero-image-category">
                <i class="fa fa-hourglass-o"></i>
                <strong>Coffee</strong>
            </a><!-- /.hero-image-category -->
            <a href="#" class="hero-image-category">
                <i class="fa fa-map-o"></i>
                <strong>Spots</strong>
            </a><!-- /.hero-image-category -->
            <a href="#" class="hero-image-category">
                <i class="fa fa-picture-o"></i>
                <strong>Galleries</strong>
            </a><!-- /.hero-image-category -->
            <a href="#" class="hero-image-category">
                <i class="fa fa-handshake-o"></i>
                <strong>Businesses</strong>
            </a><!-- /.hero-image-category -->
        </div>
        <!-- /.hero-image-categories -->
    </div>
    <!-- /.hero-image -->
</div>
<!-- /.hero-image-wrapper -->
<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">

                </div>
                <!-- /.content -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.main-inner -->
    </div>
    <!-- /.main -->
</div>
<!-- /.main-wrapper -->

@endsection




@section('footerScripts')
@endsection