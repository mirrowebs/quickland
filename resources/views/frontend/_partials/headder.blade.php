<div class="header-wrapper header-transparent header-light">
    <div class="header">
        <div class="container">
            <div class="header-inner">
                <div class="navigation-toggle toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- /.header-toggle -->
                <div class="header-logo">
                    <a href="{{route('home')}}">
                        <img src="/frontend/images/quicklands_logo.png" class="svg" alt="Home">
                    </a>
                </div>
                <!-- /.header-logo -->
                <div class="header-nav">
                    <div class="primary-nav-wrapper">
                        <ul class="nav">
                            <li class="nav-item has-sub-menu">
                                <a href="#" class="nav-link active">SELECT STATES</a>
                                <ul class="sub-menu">
                                    <li><a href="index-directory.html">TELANGA PROPERTY</a></li>
                                    <li><a href="index-classified.html">ANDHRA PRADESH PROPERTY</a></li>
                                </ul>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('propertyList')}}"
                                   class="nav-link modal-submit @if ($active_menu == 'propertyList') active @endif">
                                    <span>Properties</span>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('agents')}}"
                                   class="nav-link modal-submit @if ($active_menu == 'agentsList') active @endif">
                                    <span>Agents</span>
                                </a>
                            </li>


                        </ul>
                    </div>
                    <!-- /.primary-nav-wrapper -->
                </div>
                <!-- /.header-nav -->
            {{--<div class="header-toggle toggle">--}}
            {{--<span></span>--}}
            {{--<span></span>--}}
            {{--<span></span>--}}
            {{--</div>--}}
            <!-- /.header-toggle -->
                <div class="header-actions primary-nav-wrapper float-right">
                    <ul class="nav nav-pills ">

                        <li class="nav-item">
                            <a href="{{route('ListingAdd')}}" class="nav-link modal-submit">
                                <i class="fa fa-plus"></i> <span>Sell/Rent Property</span>
                            </a>
                        </li>
                        @auth


                            <li class="nav-item has-sub-menu">
                                <a href="#" class="nav-link @if ($active_menu == 'myaccount') active @endif">Account</a>
                                <ul class="sub-menu">
                                    <li><a href="{{ route('userprofile') }}"
                                           class="@if ($sub_active_menu == 'profile') active @endif">My Account</a></li>
                                    <li><a href="{{ route('userproperties') }}"
                                           class="@if ($sub_active_menu == 'properties') active @endif">My
                                            Properties</a></li>
                                    <li><a href="{{ route('userlogout') }}"
                                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                        <form id="logout-form" action="{{ route('userlogout') }}" method="POST"
                                              style="display: none;"> {{ csrf_field() }} </form>
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li class="nav-item"><a class="nav-link @if ($active_menu == 'agentsList') active @endif"
                                                    href="{{ route('register') }}">Register</a>
                            </li>
                            <li class="nav-item"><a class="nav-link @if ($active_menu == 'signup') active @endif"
                                                    href="{{ route('userlogin') }}">Login</a>
                            </li>
                        @endauth


                    </ul>
                </div>
                <!-- /.header-actions -->
            </div>
            <!-- /.header-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header -->
</div>
<!-- /.header-wrapper -->