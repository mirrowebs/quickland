<div class="footer-wrapper ql-footer">
    <div class="footer">
        <div class="footer-inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="widget">
                            <img src="/frontend/images/quick_lands.png" class="svg" alt="Home">
                            <p>
                                Explorer is an advanced directory HTML template. It contains all HTML components
                                required for creating beautiful and functional websites.
                            </p>
                            <ul class="social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.col-* -->
                    <div class="col-lg-4">
                        <div class="widget">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link" href="index-directory.html">Directory
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-classified.html">Classified
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-job-board.html">Job Board
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-real-estate.html">Real Estate
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-automotive.html">Automotive
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-travel.html">Travel &amp; Hotel
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-restaurant.html">Restaurant
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="admin-dashboard.html">Admin
                                        Dashboard</a></li>
                            </ul>
                        </div>
                        <!-- /.widget -->
                    </div>
                    <!-- /.col-* -->
                    <div class="col-lg-4">
                        <div class="widget">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link" href="index-directory.html">Directory
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-classified.html">Classified
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-job-board.html">Job Board
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-real-estate.html">Real Estate
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-automotive.html">Automotive
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-travel.html">Travel &amp; Hotel
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="index-restaurant.html">Restaurant
                                        Version</a></li>
                                <li class="nav-item"><a class="nav-link" href="admin-dashboard.html">Admin
                                        Dashboard</a></li>
                            </ul>
                        </div>
                        <!-- /.widget -->
                    </div>
                    <!-- /.col-* -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.footer-inner -->
    </div>
    <!-- /.footer -->
</div>
<!-- end page container -->