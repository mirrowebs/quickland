@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')

@endsection




@section('content')

    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                <div class="container">
                    <nav class="breadcrumb">

                    </nav>
                    <div class="row">
                        <div class="col-md-8 col-lg-9">
                            <div class="sort-options">
                            </div>

                        @if(count($agents)>0)
                            @foreach($agents as $agent)

                                @php
                                    if (!empty($agent->image)){
                                     $ptname=asset("uploads/users/thumbs/".$agent->image);
                                    }else{
                                    $ptname=asset("frontend/images/tmp/listing-4.jpg");
                                    }
                                @endphp

                                <!-- /.sort-options -->
                                    <div class="listing-row">
                                        <div class="listing-row-inner">
                                            <a class="listing-row-image" href="listings-detail.html">
                                            <span class="listing-row-image-content"
                                                  style="background-image: url({{ $ptname }})"></span>
                                            </a>
                                            <div class="listing-row-content">
                                                <div class="listing-row-content-header">
                                                    <h3><a href="listings-detail.html">{{ $agent->name }}</a></h3>
                                                    <h4>{{ $agent->email }}</h4>

                                                </div>
                                                <div class="listing-row-content-body">
                                                    {{ $agent->description }}
                                                    <div class="listing-row-content-read-more">
                                                        <a href="{{route('agents',['id'=>$agent->id])}}">Read more</a>
                                                    </div>
                                                    <!-- /.listing-row-content-read-more -->
                                                </div>
                                                <!-- /.listing-row-content-body -->
                                            </div>
                                            <!-- /.listing-row-content -->
                                        </div>
                                        <!-- /.listing-row-inner -->
                                    </div>

                                @endforeach
                            @endif

                            @if(count($agents)>0)

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="dataTables_info p-t-10" id="editable-datatable_info"
                                             role="status"
                                             aria-live="polite">Showing {{ $agents->firstItem() }}
                                            to {{ $agents->lastItem() }} of {{ $agents->total() }} entries
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <div class="dataTables_paginate paging_simple_numbers"
                                             id="editable-datatable_paginate">
                                            {!! $agents->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->
@endsection




@section('footerScripts')

@endsection