@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')

@endsection




@section('content')
    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container">
                        <h1>Agent Detail Page
                        </h1>
                        <div class="page-title-actions">
                            <div class="switcher">
                                <strong>Currency</strong>
                                <ul>
                                    <li class="active"><a href="#">USD</a></li>
                                    <li><a href="#">EUR</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                            <div class="switcher">
                                <strong>Language</strong>
                                <ul>
                                    <li class="active"><a href="#">EN</a></li>
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">DE</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                        </div>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-->
                </div>
                <!-- /.page-title -->
                <div class="container">

                    @if(($agent) && ($agent->image))
                        <?php
                        $ptname = asset("uploads/users/thumbs/" . $agent->imag);
                        ?>
                    @else
                        <?php
                        $ptname = asset("frontend/images/tmp/listing-4.jpg");
                        ?>
                    @endif


                    <div class="row">
                        <div class="listing-detail col-md-8 col-lg-9">
                            <div class="listing-user">
                                <a href="#" class="avatar"
                                   style="background-image: url({{ $ptname }});"></a>
                                <div class="listing-user-title">
                                    <h2>{{ $agent->name }}</h2>
                                    <h3>{{ $agent->mobile }}</h3>
                                </div>
                                <!-- /.listing-user-title -->
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-foursquare"></i></a></li>
                                </ul>
                            </div>
                            <!-- /.listing-user -->
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="overview">
                                        <h3 class="page-title-small">Information</h3>
                                        <ul>
                                            <li><strong>Name</strong><span>{{ $agent->name }}</span></li>
                                            <li><strong>Phone</strong><span>{{ $agent->mobile }}</span></li>
                                            <li><strong>E-mail</strong><span><a href="#">{{ $agent->email }}</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.overview -->
                                </div>
                                <!-- col-* -->
                                <div class="col-lg-7">
                                    <h3 class="page-title-small">About Agent</h3>
                                    <p class="mb30">
                                        {{ $agent->description }}
                                    </p>
                                </div>
                                <!-- /.col-* -->
                            </div>
                            <!-- /.row -->
                            <div class="box">
                                <div class="box-inner">
                                    <div class="box-title">
                                        <h2>Contact Agent</h2>
                                        <p>
                                            In dictum fringilla sem vitae condimentum. Maecenas venenatis odio et eros
                                            venenatis, vel bibendum mauris elementum. Maecenas id lobortis tortor. Nam
                                            condimentum pulvinar odio eget tincidunt. Curabitur in mattis enim.
                                        </p>
                                    </div>
                                    <div class="enquiriesStatus">
                                    </div>
                                    <!-- /.box-title -->
                                    <form class="" action="{{ route('enquiries') }}" id="enquiries">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="enquiry_user_id" value="{{ $agent->id }}">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input type="text" name="enquiry_name" class="form-control">
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="text" name="enquiry_email" class="form-control">
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                    <label>Subject</label>
                                                    <input type="text" name="enquiry_subject" class="form-control">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col-* -->
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label>Message</label>
                                                    <textarea name="enquiry_message" class="form-control" rows="10"
                                                              placeholder="Please keep your message as simple as possible."></textarea>
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col-* -->
                                            <div class="col-sm-12">
                                                <div class="form-group-btn">
                                                    <button type="submit" class="btn btn-primary pull-right">Send
                                                        Message
                                                    </button>
                                                </div>
                                                <!-- /.form-group-btn -->
                                            </div>
                                            <!-- /.col-* -->
                                        </div>
                                        <!-- /.row -->
                                    </form>
                                </div>
                                <!-- /.box-inner -->
                            </div>

                        @if(count($properties)>0)

                            <!-- /.box -->
                                <h3 class="page-title-small">Agent Assigned Listings</h3>
                                <div class="row">
                                    @foreach($properties as $property)
                                        @php
                                            if (!empty(getPropertyImages($property->property_id))){
                                             $ptname=asset("uploads/properties/thumbs/".getPropertyImages($property->property_id)->pi_image);
                                            }else{
                                            $ptname=asset("frontend/images/tmp/listing-4.jpg");
                                            }
                                        @endphp
                                        <div class="col-md-6 col-lg-3">
                                            <div class="listing-box">
                                                <div class="listing-box-inner">
                                                    <a href="listings-detail.html" class="listing-box-image">
                                                        <span class="listing-box-image-content"
                                                              style="background-image: url({{ $ptname }})"></span>
                                                        <!-- /.listing-box-image-content -->
                                                        <span class="listing-box-category tag">{{ $property->property_type }}</span>
                                                        <span class="listing-box-rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            </span>
                                                    </a><!-- /.listing-box-image -->
                                                    <div class="listing-box-content">
                                                        <h2>
                                                            <a href="listings-detail.html">{{ $property->property_sub_type }}</a>
                                                        </h2>
                                                        <h3>{{ $property->property_by }}</h3>
                                                        <div class="actions">
                                                            <div class="actions-button">
                                                                <span></span>
                                                                <span></span>
                                                                <span></span>
                                                            </div>
                                                            <!-- /.actions-button -->
                                                            <ul class="actions-list">
                                                                <li><a href="#">Add to compare</a></li>
                                                                <li><a href="#">Add to favorites</a></li>
                                                                <li><a href="#">Report listing</a></li>
                                                            </ul>
                                                            <!-- /.actions-list -->
                                                        </div>
                                                        <!-- /.actions -->
                                                    </div>

                                                </div>
                                                <!-- /.listing-box-inner -->
                                            </div>
                                            <!-- /.listing-box -->
                                        </div>
                                    @endforeach
                                </div>
                                <!-- /.row -->
                                @if(count($properties)>0)

                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="dataTables_info p-t-10" id="editable-datatable_info"
                                                 role="status"
                                                 aria-live="polite">Showing {{ $properties->firstItem() }}
                                                to {{ $properties->lastItem() }} of {{ $properties->total() }} entries
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                 id="editable-datatable_paginate">
                                                {!! $properties->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                        <!-- /.col-* -->
                        <div class="col-md-4 col-lg-3">
                            <div class="sidebar">
                                <div class="widget">
                                    <h3 class="widgettitle">Search Site</h3>
                                    <div class="search">
                                        <form method="get" action="?">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                <input type="text" placeholder="Search the site" class="form-control">
                                            </div>
                                            <!-- /.form-group -->
                                        </form>
                                    </div>
                                    <!-- /.search -->
                                </div>
                                <!-- /.widget -->
                                <div class="widget">
                                    <h2 class="widgettitle">Public Documents</h2>
                                    <div class="documents">
                                        <a class="document" href="#">
                                            <i class="fa fa-file-image-o"></i> <span>Image Gallery</span>
                                        </a>
                                        <a class="document" href="#">
                                            <i class="fa fa-file-excel-o"></i> <span>Detailed Pricing</span>
                                        </a>
                                        <a class="document" href="#">
                                            <i class="fa fa-file-pdf-o"></i> <span>Download Floor Plans</span>
                                        </a>
                                    </div>
                                    <!-- /.documents -->
                                </div>
                                <div class="widget">
                                    <h3 class="widgettitle">Recent Listings</h3>
                                    <div>
                                        <div class="listing-row-medium">
                                            <div class="listing-row-medium-inner">
                                                <a href="listings-detail.html" class="listing-row-medium-image"
                                                   style="background-image: url(assets/img/tmp/listing-12.jpg);">
                                                </a>
                                                <div class="listing-row-medium-content">
                                                    <a class="listing-row-medium-price" href="listings-detail.html">$533.00</a>
                                                    <a class="listing-row-medium-category tag"
                                                       href="listings-detail.html">Marketing</a>
                                                    <h4 class="listing-row-medium-title"><a href="listings-detail.html">T-Shirt
                                                            Specialized Shop</a></h4>
                                                    <div class="listing-row-medium-address">
                                                        2460 Ashwood Drive Farragut
                                                    </div>
                                                    <!-- /.listing-row-medium-address -->
                                                </div>
                                                <!-- /.listing-row-medium-content -->
                                            </div>
                                            <!-- /.listing-row-medium-inner -->
                                        </div>
                                        <!-- /.listings-row-medium -->
                                        <div class="listing-row-medium">
                                            <div class="listing-row-medium-inner">
                                                <a href="listings-detail.html" class="listing-row-medium-image"
                                                   style="background-image: url(assets/img/tmp/listing-1.jpg);">
                                                    <span class="listing-row-medium-featured">Featured</span>
                                                    <!-- /.listing-row-medium-featured -->
                                                </a>
                                                <div class="listing-row-medium-content">
                                                    <a class="listing-row-medium-category tag"
                                                       href="listings-detail.html">Education</a>
                                                    <h4 class="listing-row-medium-title"><a href="listings-detail.html">Ideal
                                                            Garden Maintenance</a></h4>
                                                    <div class="listing-row-medium-address">
                                                        2813 Preston Street Wichita
                                                    </div>
                                                    <!-- /.listing-row-medium-address -->
                                                </div>
                                                <!-- /.listing-row-medium-content -->
                                            </div>
                                            <!-- /.listing-row-medium-inner -->
                                        </div>
                                        <!-- /.listings-row-medium -->
                                        <div class="listing-row-medium">
                                            <div class="listing-row-medium-inner">
                                                <a href="listings-detail.html" class="listing-row-medium-image"
                                                   style="background-image: url(assets/img/tmp/listing-3.jpg);">
                                                </a>
                                                <div class="listing-row-medium-content">
                                                    <a class="listing-row-medium-price" href="listings-detail.html">$76.90</a>
                                                    <a class="listing-row-medium-category tag"
                                                       href="listings-detail.html">Museum</a>
                                                    <h4 class="listing-row-medium-title"><a href="listings-detail.html">Erb
                                                            Lumber</a></h4>
                                                    <div class="listing-row-medium-address">
                                                        2155 Philli Lane Mcalester
                                                    </div>
                                                    <!-- /.listing-row-medium-address -->
                                                </div>
                                                <!-- /.listing-row-medium-content -->
                                            </div>
                                            <!-- /.listing-row-medium-inner -->
                                        </div>
                                        <!-- /.listings-row-medium -->
                                        <div class="listing-row-medium">
                                            <div class="listing-row-medium-inner">
                                                <a href="listings-detail.html" class="listing-row-medium-image"
                                                   style="background-image: url(assets/img/tmp/listing-14.jpg);">
                                                </a>
                                                <div class="listing-row-medium-content">
                                                    <a class="listing-row-medium-price" href="listings-detail.html">$19.49</a>
                                                    <a class="listing-row-medium-category tag"
                                                       href="listings-detail.html">Mechanics</a>
                                                    <h4 class="listing-row-medium-title"><a href="listings-detail.html">Skateshop</a>
                                                    </h4>
                                                    <div class="listing-row-medium-address">
                                                        3298 Lewis Street Downers Grove
                                                    </div>
                                                    <!-- /.listing-row-medium-address -->
                                                </div>
                                                <!-- /.listing-row-medium-content -->
                                            </div>
                                            <!-- /.listing-row-medium-inner -->
                                        </div>
                                        <!-- /.listings-row-medium -->
                                    </div>
                                </div>
                                <!-- /.widget -->
                                <div class="widget">
                                    <h2 class="widgettitle">Contact Information</h2>
                                    <table class="contact">
                                        <tbody>
                                        <tr>
                                            <th>Address:</th>
                                            <td>2300 Main Ave.<br>
                                                Lost Angeles, CA 23123<br>
                                                United States<br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Phone:</th>
                                            <td>+0-123-456-789</td>
                                        </tr>
                                        <tr>
                                            <th>E-mail:</th>
                                            <td><a href="mailto:info@example.com">info@example.com</a></td>
                                        </tr>
                                        <tr>
                                            <th>Skype:</th>
                                            <td>your.company</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.widget -->
                            </div>
                            <!-- /.sidebar -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->
@endsection




@section('footerScripts')
    <script type="text/javascript">
        $(function () {
            $('#enquiries').validate({
                ignore: [],
                errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                rules: {
                    enquiry_name: {
                        required: true
                    },
                    enquiry_subject: {
                        required: true
                    },
                    enquiry_email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    enquiry_name: {
                        required: 'Please enter name'
                    },
                    enquiry_subject: {
                        required: 'Please enter subject'
                    },
                    enquiry_email: {
                        required: 'Please enter email'
                    }
                },
                submitHandler: function (form) {
                    $('.enquiriesStatus').html('');
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: "{{ route('enquiries') }}",
                        data: $(form).serialize(),
                        success: function (response) {
                            if (response.status == 1) {
                                $('#enquiries')[0].reset();
                                $('.enquiriesStatus').html('<div class="alert alert-success">\n' +
                                    '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                    '    <strong>Success!</strong> Your message has been sent successfully.\n' +
                                    '</div>');
                            } else {
                                $('.enquiriesStatus').html('<div class="alert alert-danger">\n' +
                                    '    <a href="#" class="close" data-dismiss="alert">&times;</a>\n' +
                                    '    <strong>' + response.errors + '</strong>\n' +
                                    '</div>');
                            }
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });
        });
    </script>
@endsection