@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')

    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container">
                        <div class="page-title-actions">
                            <div class="switcher">
                                <strong>Currency</strong>
                                <ul>
                                    <li class="active"><a href="#">USD</a></li>
                                    <li><a href="#">EUR</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                            <div class="switcher">
                                <strong>Language</strong>
                                <ul>
                                    <li class="active"><a href="#">EN</a></li>
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">DE</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                        </div>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-->
                </div>
                <!-- /.page-title -->
                <div class="container">
                    <nav class="breadcrumb">
                        <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>

                    </nav>
                    <div class="row mb80">
                        <div class="col-sm-4 offset-sm-4">
                            <h3 class="page-title-small">Login</h3>
                            <form method="POST" action="{{ route('userlogin') }}" aria-label="{{ __('Login') }}"
                                  class="formsign pt-4"
                                  id="login">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Enter User Name</label>
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           type="text"
                                           placeholder="Enter Your User name" name="email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback"
                                              role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Enter Password</label>
                                    <input class="form-control" type="password" placeholder="Enter Password"
                                           name="password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback"
                                              role="alert"> <strong>{{ $errors->first('password') }}</strong>   </span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary pull-right">Login</button>
                            </form>
                        </div>
                        <!-- /.col-sm-4 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->


@endsection




@section('footerScripts')
@endsection