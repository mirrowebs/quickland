@extends('frontend.layout')
@section('title', $title)


@section('headerStyles')
@endsection




@section('content')
    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container">
                        <h1>Register
                        </h1>
                        <div class="page-title-actions">
                            <div class="switcher">
                                <strong>Currency</strong>
                                <ul>
                                    <li class="active"><a href="#">USD</a></li>
                                    <li><a href="#">EUR</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                            <div class="switcher">
                                <strong>Language</strong>
                                <ul>
                                    <li class="active"><a href="#">EN</a></li>
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">DE</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                        </div>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-->
                </div>
                <!-- /.page-title -->
                <div class="container">
                    <nav class="breadcrumb">
                        <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                        <span class="breadcrumb-item active">Registration</span>
                    </nav>
                    <form method="POST" action="{{ route('register') }}" class="formsign pt-4" id="register"
                          autocomplete="off">
                        {{ csrf_field() }}
                        <input type="hidden" name="role" value="1">
                        <input type="hidden" name="status" value="active">
                        <div class="row mb80">
                            <div class="col-sm-4 offset-sm-4">



                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           type="text"
                                           placeholder="Enter your Name" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                         <strong>{!! $errors->first('name') !!}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           type="text"
                                           placeholder="Enter your Email" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                         <strong>{!! $errors->first('email') !!}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Mobile Number</label>
                                    <input class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}"
                                           type="text" placeholder="Enter your Mobile Number" name="mobile">
                                    @if ($errors->has('mobile'))
                                        <span class="invalid-feedback" role="alert">
                                         <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Create Password</label>
                                    <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           id="password" type="password" placeholder="Create Password" name="password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="form-control" id="password-confirm" type="password"
                                           placeholder="Confirm Password" name="password_confirmation">
                                </div>

                                <div class="center">
                                    <div class="checkbox mb30">
                                        <label>
                                            <input type="checkbox" name="termsAndConditions"> By signing up, you agree with the <a
                                                    href="terms-conditions.html">terms and conditions</a>.
                                        </label>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group-btn">
                                        <button type="submit" class="btn btn-primary pull-right">Create Account</button>
                                    </div>
                                    <!-- /.form-group-btn -->
                                </div>
                                <!-- /.center -->
                            </div>
                            <!-- /.col-* -->
                        </div>
                        <!-- /.row -->
                    </form>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->
@endsection




@section('footerScripts')
@endsection