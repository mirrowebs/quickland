@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'countries'=>'Countries',
               ''=>'Add New'
               ),'Add New Country'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="countries" action="{{ route('addNewCountries') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="country_id" value="{{ $country_id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Country Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_name" type="text"
                                               placeholder="Country Name"
                                               name="country_name"
                                               value="{{ !empty(old('country_name')) ? old('country_name') : ((($country) && ($country->country_name)) ? $country->country_name : '') }}">
                                        @if ($errors->has('country_name'))
                                            <span class="text-danger help-block">{{ $errors->first('country_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Country Code</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_code" type="text"
                                               placeholder="Country Code"
                                               name="country_code"
                                               value="{{ !empty(old('country_code')) ? old('country_code') : ((($country) && ($country->country_code)) ? $country->country_code : '') }}">
                                        @if ($errors->has('country_code'))
                                            <span class="text-danger help-block">{{ $errors->first('country_code') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="country_status" id="country_status" class="form-control">
                                            <?php
                                            $status = allOptions('statues'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('country_status')) && old('country_status')==$ks)  ? 'selected' : ((($country) && ($country->country_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country_status'))
                                            <span class="text-danger help-block">{{ $errors->first('country_status') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/countries/js/countries.js')}}"></script>

@endsection