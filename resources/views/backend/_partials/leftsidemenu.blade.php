<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            {{--<a class="navbar-brand" href="{{ route('dashboard') }}"><img src="/backend/images/logo.png" alt="Logo"></a>--}}
            {{--<a class="navbar-brand hidden" href="{{ route('dashboard') }}"><img src="/backend/images/logo.png"--}}
            {{--alt="Logo"></a>--}}
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="@if ($active_menu == 'dashboard')active @endif">
                    <a href="{{ route('dashboard') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>


                <li class="menu-item-has-children dropdown @if ($active_menu == 'countries')active @endif">
                    <a href="{{ route('countries') }}" class=""> <i class="menu-icon fa fa-table"></i>Countries</a>

                </li>

                <li class="menu-item-has-children dropdown @if ($active_menu == 'properties')active @endif">
                    <a href="{{ route('properties') }}" class=""> <i class="menu-icon fa fa-table"></i>Properties</a>

                </li>
                <li class="menu-item-has-children dropdown @if ($active_menu == 'agentsList')active @endif">
                    <a href="{{ route('agentsList') }}" class=""> <i class="menu-icon fa fa-table"></i>Agents</a>

                </li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->
