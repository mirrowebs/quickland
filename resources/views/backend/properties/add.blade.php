@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'properties'=>'Properties',
               ''=>'Add New'
               ),'PropertyFiled'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">


            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="properties" action="{{ route('addNewProperties') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="property_id" value="{{ $property_id }}">


                                <div class="card">
                                    <div class="card-header">
                                        Property Information
                                    </div>
                                    <div class="card-body">
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">I want to Sell</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="property[property_sell]" id="property_sell"
                                                        class="form-control">
                                                    <?php
                                                    $selloptions = allOptions('sell'); ?>
                                                    <option value="">Select</option>
                                                    @foreach($selloptions as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('property_sell')) && old('property_sell')==$ks)  ? 'selected' : ((($property) && ($property->property_sell == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('property[property_sell]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_sell]') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Property Type</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="property[property_type]" id="property_type"
                                                        class="form-control">
                                                    <?php
                                                    $propertytype = allOptions('property_type'); ?>
                                                    <option value="">Select</option>
                                                    @foreach($propertytype as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('property_type')) && old('property_type')==$ks)  ? 'selected' : ((($property) && ($property->property_type == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('property[property_type]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_type]') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        @if(isset($property->property_sub_type))
                                            <input type="hidden" class="property_sub_type"
                                                   value="{{$property->property_sub_type}}">
                                            @php
                                                $optionselected='';
                                                $optionselected=$property->property_sub_type;
                                            @endphp
                                        @else
                                            @php
                                                $optionselected='';
                                                $optionselected=old('property_sub_type');
                                            @endphp
                                        @endif


                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Property Sub
                                                    Type</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="property[property_sub_type]" id="property_sub_type"
                                                        class="form-control">
                                                    @foreach (json_decode('{" ": "Select Type first"}', true) as $optionKey => $optionValue)
                                                        <option value="{{ $optionKey }}" {{ (isset($optionselected) && $optionselected == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('property[property_sub_type]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_sub_type]') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Transaction
                                                    Type</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="property[property_transaction_type]"
                                                        id="property_transaction_type"
                                                        class="form-control">
                                                    <?php
                                                    $transactiontype = allOptions('transaction_type'); ?>
                                                    <option value="">Select</option>
                                                    @foreach($transactiontype as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('property_transaction_type')) && old('property_transaction_type')==$ks)  ? 'selected' : ((($property) && ($property->property_transaction_type == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('property[property_transaction_type]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_transaction_type]') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Location</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="property_location" type="text"
                                                       placeholder="Location"
                                                       name="property[property_location]"
                                                       value="{{ !empty(old('property_location')) ? old('property_location') : ((($property) && ($property->property_location)) ? $property->property_location : '') }}">
                                                @if ($errors->has('property[property_location]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_location]') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Built Up Area</label>
                                            </div>
                                            <div class="col-12 col-md-9">

                                                <input class="form-control" id="property_built_up_area" type="text"
                                                       placeholder="Built Up Area"
                                                       name="property[property_built_up_area]"
                                                       value="{{ !empty(old('property_built_up_area')) ? old('property_built_up_area') : ((($property) && ($property->property_built_up_area)) ? $property->property_built_up_area : '') }}">
                                                @if ($errors->has('property[property_built_up_area]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_built_up_area]') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Area Unit</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="property[property_area_unit]" id="property_area_unit"
                                                        class="form-control">
                                                    <?php
                                                    $areaunit = allOptions('area_unit'); ?>
                                                    <option value="">Select</option>
                                                    @foreach($areaunit as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('property_area_unit')) && old('property_area_unit')==$ks)  ? 'selected' : ((($property) && ($property->property_area_unit == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('property[property_area_unit]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_area_unit]') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Description</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                         <textarea name="property[property_description]" id="property_description"
                                                   rows="9"
                                                   placeholder="Description..."
                                                   class="form-control ckeditor">{{ !empty(old('property_description')) ? old('property_description') : ((($property) && ($property->property_description)) ? $property->property_description : '') }}</textarea>
                                                @if ($errors->has('property[property_description]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_description]') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="hf-email" class=" form-control-label">Status</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="property[property_status]" id="property_status"
                                                        class="form-control">
                                                    <?php
                                                    $status = allOptions('statues'); ?>
                                                    @foreach($status as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('property_status')) && old('property_status')==$ks)  ? 'selected' : ((($property) && ($property->property_status == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('property[property_status]'))
                                                    <span class="text-danger help-block">{{ $errors->first('property[property_status]') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                </div>


                                <div class="propertysubtype">
                                    <div class="card">
                                        <div class="card-header">
                                            Residential
                                        </div>
                                        <div class="card-body">
                                            @include('backend._partials.residential_part')
                                        </div>
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/properties/js/properties.js')}}"></script>

@endsection