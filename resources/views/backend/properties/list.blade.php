@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Properties'
              ),'Properties'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Properties</strong>
                            <a href="{{route('addNewProperties')}}" class="btn btn-primary btn-xs" style="float:right;">Add
                                Property
                            </a>


                            {{--<!-- Button trigger modal -->--}}
                            {{--<button type="button" class="btn btn-primary" data-toggle="modal"--}}
                            {{--data-target="#exampleModal">--}}
                            {{--Launch demo modal--}}
                            {{--</button>--}}

                            {{--<!-- Modal -->--}}
                            {{--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"--}}
                            {{--aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
                            {{--<div class="modal-dialog" role="document">--}}
                            {{--<div class="modal-content">--}}
                            {{--<div class="modal-header">--}}
                            {{--<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>--}}
                            {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                            {{--<span aria-hidden="true">&times;</span>--}}
                            {{--</button>--}}
                            {{--</div>--}}
                            {{--<div class="modal-body">--}}
                            {{--...--}}
                            {{--</div>--}}
                            {{--<div class="modal-footer">--}}
                            {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close--}}
                            {{--</button>--}}
                            {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}


                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Sell Type</th>
                                    <th scope="col">Property Type</th>
                                    <th scope="col">Property Sub Type</th>
                                    <th scope="col">Transaction Type</th>
                                    <th scope="col">Location</th>
                                    <th scope="col">Built Up Area</th>
                                    <th scope="col">Area Unit</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($properties)>0)
                                    @foreach($properties as $item)
                                        <tr>
                                            <td scope="row">
                                            @if (count($item->propertyOptions)==1)
                                                <a href="javascript:void(0)" class="" data-toggle="modal"
                                                data-target="#exampleModal{{ $item->property_id }}">
                                                {{ $loop->iteration }}
                                                </a>
                                            @else
                                                {{ $loop->iteration }}
                                            @endif
                                            <!-- Modal -->
                                                <div class="modal fade" id="exampleModal{{ $item->property_id }}"
                                                     tabindex="-1"
                                                     role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"
                                                                    id="exampleModalLabel{{ $item->property_id }}">
                                                                    #{{ $item->property_id }}</h5>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">

                                                                <table class="table table-striped" id="tblGrid">

                                                                    <tr>
                                                                        <td> Bedrooms :</td>

                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_bedrooms : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Bathrooms :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_bathrooms : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Balcony :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_balcony : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Other Rooms :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_other_rooms : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Total Rooms :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_total_floors : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Property On Floor :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_property_on_floor : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Reserved Parking :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_reserved_on_parking : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Availability :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_availability : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Possession by :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_possession_by : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Ownership :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_owner_ship : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Expected price :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_expected_price : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Inclusive price :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? ($item->propertyOptions[0]->po_inclusive_price) ? 'Yes' : 'No' : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Price negotiable :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? ($item->propertyOptions[0]->po_price_negotiable) ? 'Yes' : 'No' : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Price per sqyards :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_price_per_sqyards  : ''}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Advance Booking :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_advance_booking_amount : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Maintenance :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_maintenance : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Amenties :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_amenties : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Power backup :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_power_backup : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Water source :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_water_source : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Property facing :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_property_facing : '' }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Furniture :</td>
                                                                        <td class="text-right"> {{ (count($item->propertyOptions)==1) ? $item->propertyOptions[0]->po_furniture : '' }}</td>
                                                                    </tr>

                                                                </table>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{ $item->property_sell }}</td>
                                            <td>{{ $item->property_type }}</td>
                                            <td>{{ $item->property_sub_type }}</td>
                                            <td>{{ $item->property_transaction_type }}</td>
                                            <td>{{ $item->property_location }}</td>
                                            <td>{{ $item->property_built_up_area }}</td>
                                            <td>{{ $item->property_area_unit }}</td>
                                            <td>{{ $item->property_description }}</td>
                                            <td>{{ allOptions('statues',$item->property_status) }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-outline-primary dropdown-toggle" href="#"
                                                       role="button"
                                                       data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item"
                                                           href="{{ route('addNewProperties',['id'=>$item->property_id]) }}"><i
                                                                    class="fa fa-pencil"></i> Edit</a>
                                                        <a class="dropdown-item"
                                                           href="{{ route('addPropertyImages',['id'=>$item->property_id]) }}"><i
                                                                    class="fa fa-plus"></i> Property Images Images</a>

                                                        @if (count($item->propertyOptions)==1)
                                                            <a class="dropdown-item" class="" data-toggle="modal"
                                                               data-target="#exampleModal{{ $item->property_id }}"
                                                               href="javascript:void(0)"><i
                                                                        class="fa fa-eye"></i> Property Details</a>
                                                        @endif

                                                        <form method="POST" id="properties"
                                                              action="{{ route('properties') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="property_id"
                                                                   value="{{ $item->property_id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Country"
                                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i
                                                                        class="fa fa-trash"></i> Delete
                                                            </button>

                                                        </form>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $properties->firstItem() }}
                        to {{ $properties->lastItem() }} of {{ $properties->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $properties->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection