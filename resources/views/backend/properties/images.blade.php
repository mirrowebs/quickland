@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')


    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'properties'=>'Properties',
               ''=>'Add Images to property'
               ),'Add Images to property'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">


            <div class="row">
                <div class="col-md-12">
                    @if(count($property_images)>0)
                        @foreach($property_images as $item)
                            <div class="col-md-2 imagediv{{ $item->pi_id }}">
                                <img class="img-responsive" src="/uploads/properties/thumbs/{{ $item->pi_image }}"/>
                                <a href="javascript:void(0)" id="{{$item->pi_id}}"
                                   class="delete_property_image btn btn-danger btn-xs"
                                   onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                            </div>
                        @endforeach
                    @else
                        <div class="col-md-2">
                            No images found
                        </div>
                    @endif
                </div>
            </div>
            <br/><br/>


            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="propertyimages"
                                  action="{{ route('addPropertyImages',['id'=>$property_id]) }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Images</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="propertyimages" type="file"
                                               name="propertyimages[]" multiple>
                                        @if ($errors->has('propertyimages[]'))
                                            <span class="text-danger">{{ $errors->first('propertyimages[]') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script src="{{url('backend/modules/properties/js/properties.js')}}"></script>

@endsection