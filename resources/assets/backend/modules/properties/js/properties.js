$(document).ready(function () {
    $('.delete_property_image').on('click', function () {
        var image = $(this).attr('id');
        if (image != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/deletepropertyimage',
                type: 'POST',
                data: {'image': image},
                success: function (response) {
                    $('.imagediv' + image).remove();
                }
            });
        }
    });
    $('#property_type').on('change', function () {
        var property = $('#property_type').val();
        if (property == 'Residential') {
            $('.propertysubtype').show();
        } else {
            $('.propertysubtype').hide();
        }

        $('#property_sub_type').children('option').remove();
        if (property != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/admin/ajax/getPropertySubTypes',
                type: 'POST',
                data: {'property': property},
                success: function (response) {
                    var resultdata = $.parseJSON(response);
                    var length = Object.keys(resultdata.propertytypes).length;
                    if (length > 0) {
                        $('#property_sub_type').append(new Option('Select', '', false, false));
                        $.each(resultdata.propertytypes, function (k, v) {
                            if ($('.property_sub_type').val() == k) {
                                $('#property_sub_type').append(new Option(v, k, true, true));
                            } else {
                                $('#property_sub_type').append(new Option(v, k, false, false));
                            }

                        });
                    } else {
                        $('#property_sub_type').append(new Option('There is no sub types', '', false, false));
                    }
                }
            });
        } else {
            $('#property_sub_type').append(new Option('Select Type first', '', false, false));
        }
    }).change();

    $('#properties').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            'property[property_sell]': {
                required: true
            },
            'property[property_transaction_type]': {
                required: true
            },
            'property[property_location]': {
                required: true
            },
            'property[property_area_unit]': {
                required: true
            },
            'property[property_built_up_area]': {
                required: true
            },
            'property[property_type]': {
                required: true
            }
        },
        messages: {
            'property[property_sell]': {
                required: 'Please select sell type'
            },
            'property[property_transaction_type]': {
                required: 'Please select transaction type'
            },
            'property[property_location]': {
                required: 'Please enter location'
            },
            'property[property_area_unit]': {
                required: 'Please select area unit'
            },
            'property[property_built_up_area]': {
                required: 'Please enter built up area'
            },
            'property[property_type]': {
                required: 'Please select type'
            }
        },
    });
});