<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class PropertyImages extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'property_images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pi_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pi_property_id', 'pi_image'];

    static public function upDir($type = '')
    {

        $pathDir = '/uploads/properties';

        if ($type == 'url') {
            $uploadPath = url($pathDir).'/';
            $thumbPath = url($pathDir . '/thumbs/').'/';
        } else {

            $uploadPath = public_path($pathDir);

            if (!file_exists($uploadPath)) {
                mkdir($uploadPath, 0777, true);
            }

            $thumbPath = public_path($pathDir . 'thumbs/');

            if (!file_exists($thumbPath)) {
                mkdir($thumbPath, 0777, true);
            }


        }


        return ['imgs' => $uploadPath, 'thumb' => $thumbPath];
    }


    public static function fileUploads($file)
    {

        $imagesFolder = self::upDir();

        $uploadPath = $imagesFolder['imgs'];
        $thumbPath = $imagesFolder['thumb'];


        $thumb_width = 400;
        $thumb_height = 400;
        $thumb_quality = 60;


        $extension = $file->getClientOriginalExtension();
        $fileName = time() . '_' . uniqid() . '.' . $extension;


        $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize($thumb_width, $thumb_height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $thumb_img->save($thumbPath . '/' . $fileName, $thumb_quality);

        $file->move($uploadPath, $fileName);

        return $fileName;

    }


    public static function fileUpload($fileinput, $propertyid)
    {


        foreach ($fileinput as $file) {


            $fileName = self::fileUploads($file);

            $imgdata = array();
            $imgdata['pi_image'] = $fileName;
            $imgdata['pi_property_id'] = $propertyid;
            PropertyImages::create($imgdata);

//            $requestdata = array();
//            $requestdata['pi_image_name'] = $fileName;
//            $requestdata['pi_status'] = 'active';
//            $requestdata['pi_product_id'] = $product_id;
//            ProductImages::create($requestdata);
        }

    }


    public static function removeImage($imgId)
    {


        $images = self::where('pi_id', $imgId)->first();

//        dd($images);

        if (isset($images) && !empty($images)) {
            self::destroy($imgId);

            $imagesFolder = self::upDir();

            $uploadPath = $imagesFolder['imgs'];
            $thumbPath = $imagesFolder['thumb'];


            File::delete($uploadPath . $images->pi_image_name);
            File::delete($thumbPath . $images->pi_image_name);
        }
        return true;
    }


    static public function getPropertyImages($propertyid)
    {
        $resuts = array();

        $images = self::where('pi_property_id', $propertyid)->get();

        if (count($images) > 0) {
            foreach ($images AS $key=>$item) {

                $imagesFolder = self::upDir('url');

                $uploadPath = $imagesFolder['imgs'];
                $thumbPath = $imagesFolder['thumb'];


                $resuts[$key]['image'] = $uploadPath . $item->pi_image;
                $resuts[$key]['thumb'] = $thumbPath . $item->pi_image;


//                $item->push('pi_image_url', 'asda');
//                    =  $uploadPath . $item->pi_image;
//                $item->put('pi_image_url', $uploadPath . $item->pi_image);
//                $images[$key]->put('pi_image_thumb_url', $thumbPath . $item->pi_image);


//                $images->pi_image_url = $uploadPath . $item->pi_image;
//                $images->pi_image_thumb_url = $thumbPath . $item->pi_image;


            }
        }

//        dd($images);

        return $resuts;

    }

}
