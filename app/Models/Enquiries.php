<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiries extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'enquiries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'enquiry_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['enquiry_name', 'enquiry_email', 'enquiry_subject','enquiry_message','enquiry_user_id','enquiry_phone'];
}
