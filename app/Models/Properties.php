<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Properties extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'properties';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'property_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['property_sub_type', 'property_userid', 'property_for', 'property_type', 'property_by', 'property_status'];

    public function propertyOptions()
    {
        return $this->hasMany(PropertyOptions::Class, 'po_property_id', 'property_id');
    }
    public function productImages()
    {
        return $this->hasMany(PropertyImages::Class, 'pi_property_id', 'property_id');
    }
}
