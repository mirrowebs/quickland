<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyOptions extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'property_options';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'po_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'po_property_id',
        'po_construction_status',
        'po_construction_month',
        'po_construction_year',
        'po_bedrooms',
        'po_bathrooms',
        'po_balconies',
        'po_facing',
        'po_furnishing',
        'po_other_rooms',
        'po_age_of_property',
        'po_total_floors',
        'po_floor_no',
        'po_parking',
        'po_plot_area',
        'po_plot_area_type',
        'po_built_up_area',
        'po_built_up_area_type',
        'po_carpet_area',
        'po_carpet_area_type',
        'po_longitude',
        'po_latitude',
        'po_location_name',
        'po_landmark',
        'po_city',
        'po_locality',
        'po_expected_price',
        'po_all_inclusive_price',
        'po_price_per_sqft',
        'po_maintenance_amount',
        'po_maintenance_type',
        'po_booking_amount',
        'po_description',
        'po_landscape',
        'po_indoor_games',
        'po_fitness',
        'po_outdoor_games',
        'po_network_and_connectivity',
        'po_green_living',
        'po_recreation',
        'po_convenience',
        'po_health_facilities',
        'po_safety_and_security',
        'po_infrastructure',
        'po_entertainment',
        'po_retail',
        'po_transport'
    ];

    public function propertyOptions()
    {
        return $this->belongsTo(Properties::Class, 'property_id', 'po_property_id');
    }

}
