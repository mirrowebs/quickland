<?php


function getCountries()
{
    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
    return $countries;
}
function getPropertyImages($propertyid)
{
    $image = \App\Models\PropertyImages::where('pi_property_id', $propertyid)->first();
    return $image;
}


