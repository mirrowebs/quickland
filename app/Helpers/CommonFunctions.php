<?php


function strCapitalSort($string)
{
//    $string = 'MaryGoesToSchool';
    $nStr = preg_replace_callback('/[A-Z]/', function ($matches) {
        return $matches[0] = '_' . ucfirst($matches[0]);
    }, $string);


    return strtolower($nStr);
}


function listingOptions($section = null)
{
    $returnArray = array();

    $options = array(
        'userInfo' => array(
            'name' => 'Name',
            'email' => 'email',
            'mobile' => 'mobile',
        ),

        'location' => array(

            'pro_latitude' => 'latitude',
            'pro_longitude' => 'longitude',
            'pro_location_name' => 'location_name', // village area like
            'pro_land_mark' => 'landmark',
            'pro_city' => 'city',
            'pro_locality' => 'locality', // state name
//            'pro_country' => 'country', // country name
        ),

        'youAre' => array(
            'owner' => 'owner',
            'dealer' => 'dealer'
        ),
        'propertyFor' => array(
            'sell' => array(
                'propertyType' => array(
                    'residential' => array(
                        'apartment' => array(
                            'construction-status' => array(
                                'readyToMove' => 'Ready to Move',

                                'underConstruction' => array('Month', 'Year') // expected date dropdown list
                            ),
                            'features' => array(
                                'spaces' => array(
                                    'bedrooms' => range(0, 12),
                                    'bathrooms' => range(0, 12),
                                    'balconies' => array(1, 2, 3, 'more then 3'),
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'otherRooms' => array('Pooja Room', 'Study Room', 'Store Room', 'Fire Room', 'Other'),
                                    'ageOfProperty' => range(0, 12),
                                    'floorsNo' => range(0, 60),
                                    'totalFloors' => range(0, 60),

                                    'parking' => array('None', 'Car Covered', 'Bike Covered', 'Open'),
                                ),
                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'builtUpArea' => array('sq.ft', 'sq.yard'),
                                    'carpet-area' => array('sq.ft', 'sq.yard'),
                                ),

                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),

                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'independentHouseOrVilla' => array(
                            'construction-status' => array(
                                'readyToMove' => 'Ready to Move',
                                'underConstruction' => array('Month', 'Year') // expected date dropdown list
                            ),
                            'features' => array(
                                'spaces' => array(
                                    'bedrooms' => range(0, 6),
                                    'bathrooms' => range(0, 6),
                                    'balconies' => array(1, 2, 3, 'more then 3'),
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'otherRooms' => array('Pooja Room', 'Study Room', 'Store Room', 'Fire Room', 'Other'),
                                    'ageOfProperty' => range(0, 12),
                                    'lift' => array('yes', 'no'),  // checkbox
                                    'totalFloors' => range(0, 60),
                                    'parking' => array('None', 'Car Covered', 'Bike Covered', 'Open'),
                                ),
                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'builtUpArea' => array('sq.ft', 'sq.yard'),
                                    'carpetArea' => array('sq.ft', 'sq.yard'),
                                ),
                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'independentBuilderFloor' => array(
                            'constructionStatus' => array(
                                'readyToMove' => 'Ready to Move',
                                'underConstruction' => array('Month', 'Year') // expected date dropdown list
                            ),
                            'features' => array(
                                'spaces' => array(
                                    'bedrooms' => range(0, 6),
                                    'bathrooms' => range(0, 12),
                                    'balconies' => array(1, 2, 3, 'more then 3'),
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'otherRooms' => array('Pooja Room', 'Study Room', 'Store Room', 'Fire Room', 'Other'),
                                    'ageOfProperty' => range(0, 12),
                                    'floorsNo' => array(1 - 60),
                                    'totalFloors' => range(0, 60),
                                    'parking' => array('None', 'Car Covered', 'Bike Covered', 'Open'),
                                ),
                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'builtUpArea' => array('sq.ft', 'sq.yard'),
                                    'carpetArea' => array('sq.ft', 'sq.yard'),
                                ),

                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'Residential Land' => array(

                            'features' => array(
                                'InFuture' => 'In Future',// datepicker
                                'dimensions' => array(
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'length' => array('sq.ft', 'sq.yard'),
                                    'width' => array('sq.ft', 'sq.yard'),
                                    'Floors Allowed for Construction' => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'more then 9'),
                                ),
                                'price' => array(
                                    'Expected Price' => 'Expected Price Rs.',
                                    'AllInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'MaintananceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'BookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                    ),
                    'commercial' => array(
                        'CommercialOfficeOrSpace' => array(
                            'construction-status' => array(
                                'new' => 'New',
                                'reSale' => 'Re-Sale',
                                'under-construction' => array('Month', 'Year') // expected date dropdown list
                            ),

                            'features' => array(
                                'spaces' => array(
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'ageOfProperty' => range(0, 12),
                                    'floorsNo' => range(1, 60),
                                    'totalFloors' => range(1, 60),
                                    'parking' => array('None', 'Car Covered', 'Bike Covered', 'Open'),
                                ),
                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'builtUpArea' => array('sq.ft', 'sq.yard'),
                                    'carpetArea' => array('sq.ft', 'sq.yard'),
                                ),
                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'commercialShops' => array(
                            'construction-status' => array(
                                'new' => 'New',
                                'reSale' => 'Re-Sale',
                                'underConstruction' => array('Month', 'Year') // expected date dropdown list
                            ),

                            'features' => array(
                                'spaces' => array(
                                    'bathrooms' => range(0, 12),
                                    'balconies' => array(1, 2, 3, 'more then 3'),
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'ageOfProperty' => range(0, 12),
                                    'floorsNo' => range(1, 60),
                                    'totalFloors' => range(1, 60),
                                    'parking' => array('None', 'Car Covered', 'Bike Covered', 'Open'),
                                ),
                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'builtUpArea' => array('sq.ft', 'sq.yard'),
                                    'carpetArea' => array('sq.ft', 'sq.yard'),
                                ),
                                'AgeOfPropertyFiled' => array(1 - 80),
                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'commercialLands' => array(
                            'features' => array(
                                'spaces' => array(
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                ),

                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),

                                ),

                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'agriculturalFarmLands' => array(

                            'features' => array(

                                'spaces' => array(
                                    'gardenType' => array('rice', 'mango', 'Sweet potato'),// datepicker
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'waterPumpSource' => array('yes', 'no'),
                                ),

                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),

                                ),

                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                    ),
                ),
            ),
            'rent' => array(
                'propertyType' => array(
                    'residential' => array(
                        'apartment' => array(

                            'features' => array(

                                'spaces' => array(

                                    'bedrooms' => range(1, 6),
                                    'bathrooms' => range(1, 6),
                                    'floorNo' => range(1, 6),
                                    'totalFloors' => range(1, 6),
                                    'willingToRentOutTo' => array('Family', 'Bachelors (Men/Women)', 'Company'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'ageOfProperty' => range(0, 12),
                                ),

                                'dimensions' => array(

                                    'builtUpArea' => array('sq.ft'), //with text box
                                    'carpetArea' => array('sq.ft'),  //with text box
                                    'availableFrom' => array('bydate', 'immediately'),  //with text box
                                ),

                                'price' => array(
                                    'monthly-rent',
                                    'securityDeposit',
                                    'waterCharges',
                                    'other', // free text
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),

                        ),
                        'independentHouseOrVilla' => array(

                            'features' => array(
                                'spaces' => array(
                                    'bedrooms' => range(1, 6),
                                    'bathrooms' => range(1, 6),
                                    'willingToRentOutTo' => array('Family', 'Bachelors (Men/Women)', 'Company'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'ageOfProperty' => range(0, 12),
                                ),

                                'dimensions' => array(
                                    'builtUpArea' => array('sq.ft', 'sq.yard'), //with text box
                                    'carpetArea' => array('sq.ft', 'sq.yard'), //with text box
                                    'availableFrom' => array('bydate', 'immediately'),  //with text box
                                ),


                                'price' => array(
                                    'monthlyRent',
                                    'securityDeposit',
                                    'waterCharges',
                                    'other', // free text
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'independentBuilderFloor' => array(


                            'features' => array(

                                'spaces' => array(
                                    'bedrooms' => range(0, 12),
                                    'bathrooms' => range(0, 12),
                                    'floorNo' => range(0, 12),
                                    'totalFloors' => range(0, 12),
                                    'willingToRentOutTo' => array('Family', 'Bachelors (Men/Women)', 'Company'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'ageOfProperty' => range(0, 12),
                                ),
                                'dimensions' => array(
                                    'builtUpArea' => array('sq.ft', 'sq.yard'), //with text box
                                    'carpetArea' => array('sq.ft', 'sq.yard'),  //with text box
                                    'availableFrom' => array('bydate', 'immediately'),  //with text box
                                ),


                                'price' => array(
                                    'monthly-rent',
                                    'securityDeposit',
                                    'waterCharges',
                                    'other', // free text
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),


                        ),
                    ),
                    'commercial' => array(
                        'commercialOfficeOrSpace' => array(
                            'constructionStatus' => array(
                                'new' => 'New',
                                'reSale' => 'Re-Sale',
                                'underConstruction' => array('Month', 'Year') // expected date dropdown list
                            ),

                            'features' => array(
                                'spaces' => array(
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'ageOfProperty' => range(0, 12),
                                    'floorsNo' => range(1, 60),
                                    'totalFloors' => range(1, 60),
                                    'parking' => array('None', 'Car Covered', 'Bike Covered', 'Open'),
                                ),
                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'builtUpArea' => array('sq.ft', 'sq.yard'),
                                    'carpetArea' => array('sq.ft', 'sq.yard'),
                                ),
                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'commercialShops' => array(
                            'constructionStatus' => array(
                                'new' => 'New',
                                'reSale' => 'Re-Sale',
                                'underConstruction' => array('Month', 'Year') // expected date dropdown list
                            ),

                            'features' => array(
                                'spaces' => array(
                                    'bathrooms' => range(0, 12),
                                    'balconies' => array(1, 2, 3, 'more then 3'),
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'furnishing' => array('semi furnishing', 'fully furnishing', 'unfurnishing'),
                                    'ageOfProperty' => range(0, 12),
                                    'floorsNo' => range(1, 60),
                                    'totalFloors' => range(1, 60),
                                    'parking' => array('None', 'Car Covered', 'Bike Covered', 'Open'),
                                ),
                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),
                                    'builtUpArea' => array('sq.ft', 'sq.yard'),
                                    'carpetArea' => array('sq.ft', 'sq.yard'),
                                ),
                                'AgeOfPropertyFiled' => array(1 - 80),
                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'commercialLands' => array(
                            'features' => array(
                                'spaces' => array(
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                ),

                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),

                                ),

                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                        'agricultural-farm-lands' => array(

                            'features' => array(

                                'spaces' => array(
                                    'gardenType' => array('rice', 'mango', 'Sweet potato'),// datepicker
                                    'facing' => array('North', 'North-East', 'South', 'South-East', 'East', 'East-West'),
                                    'waterPumpSource' => array('yes', 'no'),
                                ),

                                'dimensions' => array(
                                    'plotArea' => array('sq.ft', 'sq.yard'),

                                ),

                                'price' => array(
                                    'expectedPrice' => 'Expected Price Rs.',
                                    'allInclusivePrice' => 'All Inclusive Price Rs.',
                                    'pricePer' => 'Price per Sq.ft',
                                    'maintenanceAmount' => array(
                                        'monthy',
                                        'annually',
                                        'one time',
                                        'other',
                                    ),
                                    'bookingAmount' => 'Booking Amount',
                                )

                            ),
                            'amenities' => array(
                                'landscape' => array('Landscaped Garden'),
                                'indoorGames' => array('Tennis', 'Badminton', 'Play Area'),
                                'fitness' => array('Swimming Pool', 'Gymnasium'),
                                'outdoorGames' => array('Play Area', 'Tennis Court', 'Badminton Court', 'Basket Ball Court'),
                                'networkAndConnectivity' => array('Intercom', 'Wifi'),
                                'greenLiving' => array('Rain Water Harvesting'),
                                'recreation' => array('Club House'),
                                'convenience' => array('Library', 'Maintenance Staff', '24hr Backup Electricity'),
                                'healthFacilities' => array('Health Facilities', 'Jacuzzi Steam', 'Spa', 'Yoga'),
                                'safetyAndSecurity' => array('Gated Community', 'Security', 'CC Cameras'),
                                'infrastructure' => array('Pucca Road'),
                                'entertainment' => array('Projector', 'Dance Club'),
                                'retail' => array('Grocery Store', 'Shopping Malls'),
                                'transport' => array('Shuttle Busses'),

                            ),
                            'propertyDescription' => 'PropertyFiled Description',
                            'images' => array('Living Room', 'Bedrooms', 'Bathrooms', 'Kitchen', 'Building', 'Floor Plan', 'Route Map', 'Other', 'Contact'),
                        ),
                    ),
                ),
            ),

        ),


    );

    if (!empty($section)) {
        $returnArray = $options[$section];
    } else {
        $returnArray = $options;
    }

    return $returnArray;
}

function textResort($srt, $find = null, $replace = null)
{

    $srt = preg_replace('/(?<!\ )[A-Z]/', ' $0', $srt);

    if ($find != '' && $replace != '') {
        $return = str_replace($find, $replace, $srt);
    } else {

        $return = str_replace('_', ' ', $srt);
    }

    return ucwords($return);

}


function sortText($srt, $alias = null)
{
    $return = '';
    if ($alias != '') {
        $return = str_replace('-', ' ', $srt);
    } else {
        $return = $srt;

    }


    $return = ucwords($return);

    return $return;
}


function allOptions($module, $option = null)
{
    $return_data = "";

    $array50 = range(0, 49);
    $array50['50 +'] = '50 +';
    unset($array50[0]);

    $array10 = range(0, 9);
    $array10['10 +'] = '10 +';
    unset($array10[0]);

    $array4 = range(0, 3);
    $array4['4 +'] = '4 +';
    unset($array4[0]);

    $displayName =
        array(
            "statues" => array('active' => 'Active', 'inactive' => 'Inactive'),
            "yesno" => array('Yes' => 'Yes', 'No' => 'No'),
            "water_source" => array('Municipal corporation' => 'Municipal corporation', 'Borewell' => 'Borewell'),
            "property_facing" => array('East Facing' => 'East Facing', 'North East' => 'North East', 'West' => 'West'),
            "sell" => array('Sell' => 'Sell', 'Rent' => 'Rent'),
            "property_type" => array('Residential' => 'Residential', 'Commercial' => 'Commercial', 'Open Lands' => 'Open Lands'),
            "Residential" => array('Apartment' => 'Apartment', 'Independent House/ Villa' => 'Independent House/ Villa', 'Independent Floor' => 'Independent Floor'),
            "Open Lands" => array(),
            "Commercial" => array('Office Space' => 'Office Space', 'Shops' => 'Shops', 'Commercial Land' => 'Commercial Land', 'Agricultural Farm Land' => 'Agricultural Farm Land', 'Showrooms' => 'Showrooms', 'Warehouse' => 'Warehouse', 'Other' => 'Other'),
            "transaction_type" => array('New' => 'New', 'Resale' => 'Resale'),
            "area_unit" => array('Sq. Feet' => 'Sq. Feet', 'Sq. Yard' => 'Sq. Yard', 'Sq. Meter' => 'Sq. Meter'),
            "bedrooms" => $array10,
            "total_floors" => $array50,
            "bathrooms" => $array4,
            "balcony" => $array4,
            "property_on_floor" => array('Basement' => 'Basement', 'Lower Ground' => 'Lower Ground', 'Ground' => 'Ground', '1-50+' => '1-50+'),
            "resered_parking" => array('None' => 'None', 'Covered' => 'Covered', 'Open' => 'Open'),
            "owner_ship" => array('FreeHold' => 'FreeHold', 'Leasehold' => 'Leasehold', 'Co-operative Society' => 'Co-operative Society', 'Power of Attorney' => 'Power of Attorney'),
            "availability" => array('Ready to Move' => 'Ready to Move', 'Under Construction' => 'Under Construction'),
            "possession_by" => array('Immediately' => 'Immediately', 'With in 1-5 Months' => 'With in 1-5 Months', '2019-2025' => '2019-2025'),
            "other_rooms" => array('Pooja Room' => 'Pooja Room', 'Study Room' => 'Study Room', 'Maid Room' => 'Maid Room', 'Others' => 'Others'),
            "maintenance" => array('Monthly' => 'Monthly', 'Annually' => 'Annually', 'One time' => 'One time', 'Per Unit Monthly' => 'Per Unit Monthly'),
            "furniture" => array(' Full Furniture' => ' Full Furniture', 'Partially Furniture' => 'Partially Furniture', 'Unfurniture' => 'Unfurniture'),
            "amenties" => array('Lift' => 'Lift', 'Park' => 'Park', 'Maintenance Staf' => 'Maintenance Staf', 'Visitor Parking' => 'Visitor Parking', 'Water rage' => 'Water rage', 'Intercom Facility' => 'Intercom Facility', 'Security/Alaram' => 'Security/Alaram'),
        );

    if ($module && $option) {
        $return_data = $displayName[$module][$option];
    } else {
        $return_data = $displayName[$module];
    }
    return $return_data;

}

function getBreadcrumbs($parameters, $title, $url = null)
{
    if ($url) {
        $img = $url;
    } else {
        $img = '/plugins/images/heading-title-bg.jpg';
    }
    $html = '<div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1> ' . $title . '</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">';
    foreach ($parameters as $key => $value) {
        if (is_array($value)) {

            $route = $key;
            $name = '';
            $options = array();

            foreach ($value as $akey => $avalue) {
                if ($akey == 'name') {
                    $name = $avalue;
                } else {
                    $options = $avalue;
                }
            }

            $url = route($route, $options);

            if (!next($parameters)) {
                $html .= '<li class="active">' . $name . '</li>';

            } else {
                $html .= '<li><a href=' . $url . '>' . $name . '</a></li>';
            }

        } else {
            if (!next($parameters)) {
                $html .= '<li class="active">' . $value . '</li>';
            } else {
                $url = route($key);
                $html .= '<li><a href=' . $url . '>' . $value . '</a></li>';
            }
        }
    }
    $html .= '</ol>
                </div>
            </div>
        </div>
    </div>';
    return $html;
}


function g_print($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

