<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Properties;
use App\Models\PropertyImages;
use App\Models\PropertyOptions;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use File;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        define('PAGE_LIMIT', 30);
    }

    public function index($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $tableInfo = new User();
            $this->validate(request(), [
                'name' => 'required',
                'email' => ['required', Rule::unique($tableInfo->getTable())->ignore(Auth::id(), 'id')],
                'mobile' => 'required',
                'mobile_prefix' => 'required',
                'gender' => 'required',
                'dob' => 'required',
            ], [
                'name.required' => 'Please enter name',
                'email.required' => 'Please enter email',
                'mobile.required' => 'Enter mobile',
                'mobile_prefix.required' => 'Enter mobile prefix',
                'gender.required' => 'Select Gender',
                'dob.required' => 'Enter DOB',
            ]);
            $fileName = '';
            if ($request->hasFile('image')) {
                $uploadPath = public_path('/uploads/users/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/users/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['image']->move($uploadPath, $fileName);
                $requestData['image'] = $fileName;
            }

            $requestData['dob'] = Carbon::createFromFormat('d-m-Y', $requestData['dob'])->toDateString();


            $users = User::findOrFail(Auth::id());
            $users->update($requestData);

            $mes = 'User updated successfully!';
            return redirect()->route('userprofile')->with('flash_message', $mes);
        } else {
            $data = array();
            $data['active_menu'] = 'myaccount';
            $data['sub_active_menu'] = 'profile';
            $data['title'] = 'User Profile';
            $data['user'] = User::where('id', Auth::id())->first();
            return view('frontend.user.profile', $data);
        }
    }


    public function ListingAdd(Request $request, $step = null)
    {

        if ($step != '') {
            switch ($step) {
                case '2':
                    $basicdata = Session::get('formData')['basic'];
                    $options_form_hidden = array(
                        'propertyFor' => $basicdata['propertyFor'],
                        'youAreOtp' => $basicdata['propertyType'],
                        'optionsSelected' => $basicdata['propertyTypeOtp'],
                    );
                    $data = array();
                    $data['title'] = 'Add Listing Step 2';
                    $data['active_menu'] = 'home';
                    $data['sub_active_menu'] = 'home';
                    $data['options_form_hidden'] = $options_form_hidden;
                    $data['sub_active_menu'] = 'add_step_2';
                    return view('frontend.listing.add.listingAdd_2', $data);

                    break;
                case '3':

                    if ($request->isMethod('post')) {
                        $options_form_hidden = $request->post('hidden');
                        $oldFormData = Session::get('formData');
                        $oldFormData = array_merge($oldFormData, $request->post());
                        Session::put('formData', $oldFormData);
                    }

                    $data = array();
                    $data['title'] = 'Add Listing Step 3';
                    $data['active_menu'] = 'home';
                    $data['sub_active_menu'] = 'add_step_3';
                    $data['options_form_hidden'] = $options_form_hidden;
                    return view('frontend.listing.add.listingAdd_3', $data);


                    break;
                case '4':

                    if ($request->isMethod('post')) {
                        $options_form_hidden = $request->post('hidden');
                        $oldFormData = Session::get('formData');
                        $oldFormData = array_merge($oldFormData, $request->post());
                        Session::put('formData', $oldFormData);
                        $requestData = $request->all();
                    }

                    $data = array();
                    $data['title'] = 'Add Listing Step 4';
                    $data['active_menu'] = 'home';
                    $data['sub_active_menu'] = 'add_step_4';
                    $data['options_form_hidden'] = $options_form_hidden;
                    return view('frontend.listing.add.listingAdd_4', $data);
                    break;
                case '5':

                    if ($request->isMethod('post')) {
                        $options_form_hidden = $request->post('hidden');
                        $oldFormData = Session::get('formData');
                        $oldFormData = array_merge($oldFormData, $request->post());
                        Session::put('formData', $oldFormData);
                        $requestData = $request->all();
                    }

                    $data = array();
                    $data['title'] = 'Add Listing Step 4';
                    $data['active_menu'] = 'home';
                    $data['sub_active_menu'] = 'add_step_5';
                    $data['options_form_hidden'] = $options_form_hidden;
                    return view('frontend.listing.add.listingAdd_5', $data);
                    break;
                default:

                    $requestData = $request->all();
                    $formdata = Session::get('formData');

//                    dd($formdata);


//                dd(Session::get('propertyid'));


                    $inputpo = array();
                    $inputpo['po_property_id'] = Session::get('propertyid');


                    if (isset($formdata['PropertyDetails']['constructionStatus']['value'])) {
                        $inputpo['po_construction_status'] = $formdata['PropertyDetails']['constructionStatus']['value'];
                    }
                    if (isset($formdata['PropertyDetails']['constructionStatus']['month'])) {
                        $inputpo['po_construction_month'] = $formdata['PropertyDetails']['constructionStatus']['month'];
                    }
                    if (isset($formdata['PropertyDetails']['constructionStatus']['year'])) {
                        $inputpo['po_construction_year'] = $formdata['PropertyDetails']['constructionStatus']['year'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['bedrooms'])) {
                        $inputpo['po_bedrooms'] = $formdata['PropertyDetails']['spaces']['bedrooms'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['bathrooms'])) {

                        $inputpo['po_bathrooms'] = $formdata['PropertyDetails']['spaces']['bathrooms'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['balconies'])) {

                        $inputpo['po_balconies'] = $formdata['PropertyDetails']['spaces']['balconies'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['facing'])) {

                        $inputpo['po_facing'] = $formdata['PropertyDetails']['spaces']['facing'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['furnishing'])) {

                        $inputpo['po_furnishing'] = $formdata['PropertyDetails']['spaces']['furnishing'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['otherRooms'])) {

                        $inputpo['po_other_rooms'] = $formdata['PropertyDetails']['spaces']['otherRooms'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['ageOfProperty'])) {
                        $inputpo['po_age_of_property'] = $formdata['PropertyDetails']['spaces']['ageOfProperty'];

                    }
                    if (isset($formdata['PropertyDetails']['spaces']['floorsNo'])) {

                        $inputpo['po_total_floors'] = $formdata['PropertyDetails']['spaces']['floorsNo'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['totalFloors'])) {
                        $inputpo['po_floor_no'] = $formdata['PropertyDetails']['spaces']['totalFloors'];

                    }
                    if (isset($formdata['PropertyDetails']['spaces']['parking'])) {

                        $inputpo['po_parking'] = $formdata['PropertyDetails']['spaces']['parking'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['plotArea']['value'])) {

                        $inputpo['po_plot_area'] = $formdata['PropertyDetails']['dimensions']['plotArea']['value'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['plotArea']['type'])) {

                        $inputpo['po_plot_area_type'] = $formdata['PropertyDetails']['dimensions']['plotArea']['type'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['builtUpArea']['value'])) {

                        $inputpo['po_built_up_area'] = $formdata['PropertyDetails']['dimensions']['builtUpArea']['value'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['builtUpArea']['type'])) {
                        $inputpo['po_built_up_area_type'] = $formdata['PropertyDetails']['dimensions']['builtUpArea']['type'];

                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['carpetArea']['value'])) {
                        $inputpo['po_carpet_area'] = $formdata['PropertyDetails']['dimensions']['carpetArea']['value'];

                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['carpetArea']['type'])) {
                        $inputpo['po_carpet_area_type'] = $formdata['PropertyDetails']['dimensions']['carpetArea']['type'];

                    }
                    if (isset($formdata['location']['pro_longitude'])) {
                        $inputpo['po_longitude'] = $formdata['location']['pro_longitude'];

                    }
                    if (isset($formdata['location']['pro_latitude'])) {

                        $inputpo['po_latitude'] = $formdata['location']['pro_latitude'];
                    }
                    if (isset($formdata['location']['pro_location_name'])) {
                        $inputpo['po_location_name'] = $formdata['location']['pro_location_name'];

                    }
                    if (isset($formdata['location']['pro_land_mark'])) {

                        $inputpo['po_landmark'] = $formdata['location']['pro_land_mark'];
                    }
                    if (isset($formdata['location']['pro_city'])) {
                        $inputpo['po_city'] = $formdata['location']['pro_city'];

                    }
                    if (isset($formdata['location']['pro_locality'])) {

                        $inputpo['po_locality'] = $formdata['location']['pro_locality'];
                    }
                    if (isset($formdata['price']['expectedPrice'])) {

                        $inputpo['po_expected_price'] = $formdata['price']['expectedPrice'];
                    }
                    if (isset($formdata['price']['allInclusivePrice'])) {
                        $inputpo['po_all_inclusive_price'] = $formdata['price']['allInclusivePrice'];

                    }
                    if (isset($formdata['price']['pricePer'])) {

                        $inputpo['po_price_per'] = $formdata['price']['pricePer'];
                    }
                    if (isset($formdata['price']['maintananceAmount']['value'])) {

                        $inputpo['po_maintenance_amount'] = $formdata['price']['maintananceAmount']['value'];
                    }
                    if (isset($formdata['price']['maintananceAmount']['type'])) {
                        $inputpo['po_maintenance_amount_type'] = $formdata['price']['maintananceAmount']['type'];

                    }
                    if (isset($formdata['price']['bookingAmount'])) {

                        $inputpo['po_booking_amount'] = $formdata['price']['bookingAmount'];
                    }
                    if (isset($requestData['features']['description'])) {
                        $inputpo['po_description'] = $requestData['features']['description'];

                    }
                    if (isset($requestData['features']['amenities']['landscape'])) {
                        $inputpo['po_landscape'] = implode(',', $requestData['features']['amenities']['landscape']);
                    }
                    if (isset($requestData['features']['amenities']['indoorGames'])) {
                        $inputpo['po_indoor_games'] = implode(',', $requestData['features']['amenities']['indoorGames']);
                    }
                    if (isset($requestData['features']['amenities']['fitness'])) {
                        $inputpo['po_fitness'] = implode(',', $requestData['features']['amenities']['fitness']);
                    }
                    if (isset($requestData['features']['amenities']['outdoorGames'])) {
                        $inputpo['po_outdoor_games'] = implode(',', $requestData['features']['amenities']['outdoorGames']);
                    }
                    if (isset($requestData['features']['amenities']['metworkAndConnectivity'])) {
                        $inputpo['po_network_and_connectivity'] = implode(',', $requestData['features']['amenities']['metworkAndConnectivity']);
                    }
                    if (isset($requestData['features']['amenities']['greenLiving'])) {
                        $inputpo['po_green_living'] = implode(',', $requestData['features']['amenities']['greenLiving']);
                    }
                    if (isset($requestData['features']['amenities']['recreation'])) {
                        $inputpo['po_recreation'] = implode(',', $requestData['features']['amenities']['recreation']);
                    }
                    if (isset($requestData['features']['amenities']['convenience'])) {
                        $inputpo['po_convenience'] = implode(',', $requestData['features']['amenities']['convenience']);
                    }
                    if (isset($requestData['features']['amenities']['healthFacilities'])) {
                        $inputpo['po_health_facilities'] = implode(',', $requestData['features']['amenities']['healthFacilities']);
                    }
                    if (isset($requestData['features']['amenities']['safetyAndSecurity'])) {
                        $inputpo['po_safety_and_security'] = implode(',', $requestData['features']['amenities']['safetyAndSecurity']);
                    }
                    if (isset($requestData['features']['amenities']['infrastructure'])) {
                        $inputpo['po_infrastructure'] = implode(',', $requestData['features']['amenities']['infrastructure']);
                    }
                    if (isset($requestData['features']['amenities']['entertainment'])) {
                        $inputpo['po_entertainment'] = implode(',', $requestData['features']['amenities']['entertainment']);
                    }
                    if (isset($requestData['features']['amenities']['retail'])) {
                        $inputpo['po_retail'] = implode(',', $requestData['features']['amenities']['retail']);
                    }
                    if (isset($requestData['features']['amenities']['transport'])) {
                        $inputpo['po_transport'] = implode(',', $requestData['features']['amenities']['transport']);
                    }


//                    dd(Session::all());
//                    dd($inputpo);

//                    $inputpo['po_property_id'] = Session::get('propertyid');
                    PropertyOptions::create($inputpo);


//                    echo "<pre>";
//                    print_r($request->hasFile('propertyimages'));
//                    exit();


                    if ($request->hasFile('propertyimages')) {
                        foreach ($request['propertyimages'] as $file) {
                            $uploadPath = public_path('/uploads/properties/');

                            if (!file_exists($uploadPath)) {
                                mkdir($uploadPath, 0777, true);
                            }
                            $extension = $file->getClientOriginalName();

                            $fileName = time() . $extension;

                            $thumbPath = public_path('/uploads/properties/thumbs/');

                            if (!file_exists($thumbPath)) {
                                mkdir($thumbPath, 0777, true);
                            }

                            $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                            $thumb_img->save($thumbPath . '/' . $fileName, 60);

                            $file->move($uploadPath, $fileName);

                            $imgdata = array();
                            $imgdata['pi_image'] = $fileName;
                            $imgdata['pi_property_id'] = Session::get('propertyid');
                            PropertyImages::create($imgdata);
                        }
                    }

//                    dd($inputpo);
                    return redirect()->route('ListingAdd')->with('flash_message', 'Property added successfully!');

                    break;
            }

        } else {

            Session::forget('propertyid');

            if ($request->isMethod('post')) {
                $requestData = $request->all();

                $basicData = $request->post('basic');
                Session::put('formData', $request->post());
                $requestData['property_by'] = $basicData['youAre'];
                $requestData['property_for'] = $basicData['propertyFor'];
                $requestData['property_type'] = $basicData['propertyType'];
                $requestData['property_sub_type'] = $basicData['propertyTypeOtp'];
                $requestData['property_userid'] = Auth::id();
                $property_id = Properties::create($requestData);

                Session::put('propertyid', $property_id->property_id);


                return redirect()->route('ListingAdd', ['step' => 2])->with('flash_message', 'PropertyFiled added successfully!');

            }

            $data = array();
            $data['title'] = 'Add Listing Step 1';
            $data['active_menu'] = 'add_list';
            $data['sub_active_menu'] = 'add_step_1';
            return view('frontend.listing.add.listingAdd_1', $data);


        }


    }


    public function properties(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $propertyimages = PropertyImages::where('pi_property_id', $requestData['property_id'])->get();

            foreach ($propertyimages as $image) {

                File::delete('uploads/properties/' . $image->pi_image);
                File::delete('uploads/properties/thumbs/' . $image->pi_image);
                PropertyImages::destroy($image->pi_id);
            }

            PropertyOptions::where('po_property_id', $requestData['property_id'])->delete();

            Properties::destroy($requestData['property_id']);

            return redirect()->route('userproperties')->with('flash_message', 'PropertyFiled deleted successfully!');
        } else {
            $properties = Properties::with('propertyOptions')->where('property_userid', Auth::id())->orderBy('property_id', 'desc')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'myaccount';
        $data['sub_active_menu'] = 'properties';
        $data['title'] = 'User Properties';
        $data['properties'] = $properties;
        return view('frontend.user.properties.list', $data);
    }

    public function updateProperties($type, $propertyid, Request $request)
    {

        $properties = Properties::with('propertyOptions')->where('property_id', $propertyid)->first();


        $data = array();
        $data['active_menu'] = 'updateproperties';
        $data['sub_active_menu'] = 'updateproperties';
        $data['title'] = 'Update Properties';
        $data['properties'] = $properties;

        $ajaxData = new AjaxController();
        switch ($type) {
            case "basic":
                if ($request->isMethod('post')) {

                    $requestData = $request->all();
                    $basicData = $request->post('basic');
                    $requestData['property_by'] = $basicData['youAre'];
                    $requestData['property_for'] = $basicData['propertyFor'];
                    $requestData['property_type'] = $basicData['propertyType'];
                    $requestData['property_sub_type'] = $basicData['propertyTypeOtp'];

                    $property = Properties::findOrFail($propertyid);
                    $property->update($requestData);

                    $mes = 'Property Details updated successfully!';

                    return redirect()->route('userpropertyDetails', ['type' => $type, 'propertyid' => $propertyid])->with('flash_message', $mes);
                }

                return view('frontend.user.properties.basic', $data);
                break;
            case "propertyDetails":

                if ($request->isMethod('post')) {

                    $formdata = $request->all();

                    $inputpo = array();
                    if (isset($formdata['PropertyDetails']['constructionStatus']['value'])) {
                        $inputpo['po_construction_status'] = $formdata['PropertyDetails']['constructionStatus']['value'];
                    }
                    if (isset($formdata['PropertyDetails']['constructionStatus']['month'])) {
                        $inputpo['po_construction_month'] = $formdata['PropertyDetails']['constructionStatus']['month'];
                    }
                    if (isset($formdata['PropertyDetails']['constructionStatus']['year'])) {
                        $inputpo['po_construction_year'] = $formdata['PropertyDetails']['constructionStatus']['year'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['bedrooms'])) {
                        $inputpo['po_bedrooms'] = $formdata['PropertyDetails']['spaces']['bedrooms'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['bathrooms'])) {

                        $inputpo['po_bathrooms'] = $formdata['PropertyDetails']['spaces']['bathrooms'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['balconies'])) {

                        $inputpo['po_balconies'] = $formdata['PropertyDetails']['spaces']['balconies'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['facing'])) {

                        $inputpo['po_facing'] = $formdata['PropertyDetails']['spaces']['facing'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['furnishing'])) {

                        $inputpo['po_furnishing'] = $formdata['PropertyDetails']['spaces']['furnishing'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['otherRooms'])) {

                        $inputpo['po_other_rooms'] = $formdata['PropertyDetails']['spaces']['otherRooms'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['ageOfProperty'])) {
                        $inputpo['po_age_of_property'] = $formdata['PropertyDetails']['spaces']['ageOfProperty'];

                    }
                    if (isset($formdata['PropertyDetails']['spaces']['floorsNo'])) {

                        $inputpo['po_total_floors'] = $formdata['PropertyDetails']['spaces']['floorsNo'];
                    }
                    if (isset($formdata['PropertyDetails']['spaces']['totalFloors'])) {
                        $inputpo['po_floor_no'] = $formdata['PropertyDetails']['spaces']['totalFloors'];

                    }
                    if (isset($formdata['PropertyDetails']['spaces']['parking'])) {

                        $inputpo['po_parking'] = $formdata['PropertyDetails']['spaces']['parking'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['plotArea']['value'])) {

                        $inputpo['po_plot_area'] = $formdata['PropertyDetails']['dimensions']['plotArea']['value'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['plotArea']['type'])) {

                        $inputpo['po_plot_area_type'] = $formdata['PropertyDetails']['dimensions']['plotArea']['type'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['builtUpArea']['value'])) {

                        $inputpo['po_built_up_area'] = $formdata['PropertyDetails']['dimensions']['builtUpArea']['value'];
                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['builtUpArea']['type'])) {
                        $inputpo['po_built_up_area_type'] = $formdata['PropertyDetails']['dimensions']['builtUpArea']['type'];

                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['carpetArea']['value'])) {
                        $inputpo['po_carpet_area'] = $formdata['PropertyDetails']['dimensions']['carpetArea']['value'];

                    }
                    if (isset($formdata['PropertyDetails']['dimensions']['carpetArea']['type'])) {
                        $inputpo['po_carpet_area_type'] = $formdata['PropertyDetails']['dimensions']['carpetArea']['type'];

                    }


                    PropertyOptions::where('po_property_id', $propertyid)
                        ->update($inputpo);
                    $mes = 'Property Details updated successfully!';

                    return redirect()->route('userpropertyDetails', ['type' => $type, 'propertyid' => $propertyid])->with('flash_message', $mes);

                }


                $options_form_hidden = array(
                    'propertyFor' => $properties['property_for'],
                    'youAreOtp' => $properties['property_type'],
                    'optionsSelected' => $properties['property_sub_type'],
                );

                $data['options_form_hidden'] = $options_form_hidden;


                return view('frontend.user.properties.property', $data);
                break;
            case "location":
                if ($request->isMethod('post')) {

                    $formdata = $request->all();

                    $inputpo = array();

                    if (isset($formdata['location']['pro_longitude'])) {
                        $inputpo['po_longitude'] = $formdata['location']['pro_longitude'];

                    }
                    if (isset($formdata['location']['pro_latitude'])) {

                        $inputpo['po_latitude'] = $formdata['location']['pro_latitude'];
                    }
                    if (isset($formdata['location']['pro_location_name'])) {
                        $inputpo['po_location_name'] = $formdata['location']['pro_location_name'];

                    }
                    if (isset($formdata['location']['pro_land_mark'])) {

                        $inputpo['po_landmark'] = $formdata['location']['pro_land_mark'];
                    }
                    if (isset($formdata['location']['pro_city'])) {
                        $inputpo['po_city'] = $formdata['location']['pro_city'];

                    }
                    if (isset($formdata['location']['pro_locality'])) {

                        $inputpo['po_locality'] = $formdata['location']['pro_locality'];
                    }


                    PropertyOptions::where('po_property_id', $propertyid)
                        ->update($inputpo);
                    $mes = 'Property Details updated successfully!';

                    return redirect()->route('userpropertyDetails', ['type' => $type, 'propertyid' => $propertyid])->with('flash_message', $mes);

                }
                $options_form_hidden = array(
                    'propertyFor' => $properties['property_for'],
                    'youAreOtp' => $properties['property_type'],
                    'optionsSelected' => $properties['property_sub_type'],
                );

                $data['options_form_hidden'] = $options_form_hidden;

                return view('frontend.user.properties.location', $data);
                break;
            case "pricing":

                if ($request->isMethod('post')) {

                    $formdata = $request->all();

                    $inputpo = array();
                    if (isset($formdata['price']['expectedPrice'])) {

                        $inputpo['po_expected_price'] = $formdata['price']['expectedPrice'];
                    }
                    if (isset($formdata['price']['allInclusivePrice'])) {
                        $inputpo['po_all_inclusive_price'] = $formdata['price']['allInclusivePrice'];

                    }
                    if (isset($formdata['price']['pricePer'])) {

                        $inputpo['po_price_per'] = $formdata['price']['pricePer'];
                    }
                    if (isset($formdata['price']['maintenanceAmount']['value'])) {

                        $inputpo['po_maintenance_amount'] = $formdata['price']['maintenanceAmount']['value'];
                    }
                    if (isset($formdata['price']['maintenanceAmount']['type'])) {
                        $inputpo['po_maintenance_amount_type'] = $formdata['price']['maintenanceAmount']['type'];

                    }
                    if (isset($formdata['price']['bookingAmount'])) {

                        $inputpo['po_booking_amount'] = $formdata['price']['bookingAmount'];
                    }


                    PropertyOptions::where('po_property_id', $propertyid)
                        ->update($inputpo);
                    $mes = 'Property Details updated successfully!';

                    return redirect()->route('userpropertyDetails', ['type' => $type, 'propertyid' => $propertyid])->with('flash_message', $mes);
                }
                $options_form_hidden = array(
                    'propertyFor' => $properties['property_for'],
                    'youAreOtp' => $properties['property_type'],
                    'optionsSelected' => $properties['property_sub_type'],
                );

                $data['options_form_hidden'] = $options_form_hidden;

                return view('frontend.user.properties.pricing', $data);
                break;
            case "features":
                if ($request->isMethod('post')) {

                    $requestData = $request->all();

                    $inputpo = array();
                    if (isset($requestData['features']['description'])) {
                        $inputpo['po_description'] = $requestData['features']['description'];

                    }
                    if (isset($requestData['features']['amenities']['landscape'])) {
                        $inputpo['po_landscape'] = implode(',', $requestData['features']['amenities']['landscape']);
                    }
                    if (isset($requestData['features']['amenities']['indoorGames'])) {
                        $inputpo['po_indoor_games'] = implode(',', $requestData['features']['amenities']['indoorGames']);
                    }
                    if (isset($requestData['features']['amenities']['fitness'])) {
                        $inputpo['po_fitness'] = implode(',', $requestData['features']['amenities']['fitness']);
                    }
                    if (isset($requestData['features']['amenities']['outdoorGames'])) {
                        $inputpo['po_outdoor_games'] = implode(',', $requestData['features']['amenities']['outdoorGames']);
                    }
                    if (isset($requestData['features']['amenities']['networkAndConnectivity'])) {
                        $inputpo['po_network_and_connectivity'] = implode(',', $requestData['features']['amenities']['networkAndConnectivity']);
                    }
                    if (isset($requestData['features']['amenities']['greenLiving'])) {
                        $inputpo['po_green_living'] = implode(',', $requestData['features']['amenities']['greenLiving']);
                    }
                    if (isset($requestData['features']['amenities']['recreation'])) {
                        $inputpo['po_recreation'] = implode(',', $requestData['features']['amenities']['recreation']);
                    }
                    if (isset($requestData['features']['amenities']['convenience'])) {
                        $inputpo['po_convenience'] = implode(',', $requestData['features']['amenities']['convenience']);
                    }
                    if (isset($requestData['features']['amenities']['healthFacilities'])) {
                        $inputpo['po_health_facilities'] = implode(',', $requestData['features']['amenities']['healthFacilities']);
                    }
                    if (isset($requestData['features']['amenities']['safetyAndSecurity'])) {
                        $inputpo['po_safety_and_security'] = implode(',', $requestData['features']['amenities']['safetyAndSecurity']);
                    }
                    if (isset($requestData['features']['amenities']['infrastructure'])) {
                        $inputpo['po_infrastructure'] = implode(',', $requestData['features']['amenities']['infrastructure']);
                    }
                    if (isset($requestData['features']['amenities']['entertainment'])) {
                        $inputpo['po_entertainment'] = implode(',', $requestData['features']['amenities']['entertainment']);
                    }
                    if (isset($requestData['features']['amenities']['retail'])) {
                        $inputpo['po_retail'] = implode(',', $requestData['features']['amenities']['retail']);
                    }
                    if (isset($requestData['features']['amenities']['transport'])) {
                        $inputpo['po_transport'] = implode(',', $requestData['features']['amenities']['transport']);
                    }

//                    dd($inputpo);

                    PropertyOptions::where('po_property_id', $propertyid)
                        ->update($inputpo);


                    if ($request->hasFile('propertyimages')) {

                        PropertyImages::fileUpload('propertyimages', $propertyid);


                    }


                    $mes = 'Property Details updated successfully!';

                    return redirect()->route('userpropertyDetails', ['type' => $type, 'propertyid' => $propertyid])->with('flash_message', $mes);

                }

                $options_form_hidden = array(
                    'propertyFor' => $properties['property_for'],
                    'youAreOtp' => $properties['property_type'],
                    'optionsSelected' => $properties['property_sub_type'],
                );

                $data['options_form_hidden'] = $options_form_hidden;
                $data['property_images'] = PropertyImages::where('pi_property_id', $propertyid)->get();

                return view('frontend.user.properties.features', $data);
                break;
            default:
                return view('frontend.user.properties.basic', $data);
        }
    }

    public function deletepropertyimage(Request $request)
    {

        PropertyImages::removeImage($request['image']);

//        $image = PropertyImages::findOrFail($request['image']);
//
//        File::delete('uploads/properties/' . $image->pi_image);
//        File::delete('uploads/properties/thumbs/' . $image->pi_image);
//
//        PropertyImages::destroy($request['image']);
        exit();
    }
}
