<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Quickland-Home';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.home', $data);
    }


    public function getPropertyType(Request $request)
    {

        $returndata = array();

        $propertyFor = $request->get('propertyFor');
        $propertyFor_Value = $request->get('propertyFor_Value');


        if (isset(listingOptions('propertyFor')[$propertyFor]) && !empty(listingOptions('propertyFor')[$propertyFor])) {

            $PropertyTypes = array_keys(listingOptions('propertyFor')[$propertyFor]['propertyType']);

            ?>
            <div class="box">
                <div class="box-inner">
                    <div class="box-title">
                        <h2>Property Type</h2>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group propertyTypeOptionslist">
                                <?php
                                foreach ($PropertyTypes AS $PropertyType) {
                                    ?>

                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <div class="radio-wrapper">
                                                <input type="radio" <?php echo (!empty($propertyFor_Value) && $propertyFor_Value == $PropertyType) ? 'checked' : '' ?>
                                                       class="form-check-input"
                                                       name="basic[propertyType]"
                                                       value="<?= $PropertyType ?>"/>
                                                <span class=" indicator"></span>
                                            </div>
                                            <?= textResort($PropertyType) ?>
                                        </label>
                                    </div>
                                    <?php

                                }

                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php

        }


//        return json_encode($returndata);


//        dd($request->get('for'));
    }


    public function getPropertyTypeOptions(Request $request)
    {

        $propertyFor = $request->get('propertyFor');
        $youAreOtp = $request->get('youAreOtp');
        $property_sub_type = $request->get('property_sub_type');


        if (isset(listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp]) && count(listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp]) > 0) {

            $propertyTypeOptions = array_keys(listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp]);

//            dd($propertyTypeOptions)

            ?>
            <div class="box">
                <div class="box-inner">
                    <div class="box-title">
                        <h2>selected options</h2>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group propertyTypeOptionSelected">
                                <?php
                                foreach ($propertyTypeOptions AS $propertyTypeOption) {
                                    ?>

                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <div class="radio-wrapper">
                                                <input type="radio"
                                                       class="form-check-input" <?php echo ($property_sub_type==$propertyTypeOption)?'checked':'' ?>
                                                       name="basic[propertyTypeOtp]"
                                                       value="<?= $propertyTypeOption ?>"/>
                                                <span class="indicator"></span>
                                            </div>
                                            <?= textResort($propertyTypeOption) ?>
                                        </label>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }

    public
    function getPropertyTypeOptionSelected($options = null, Request $request = null)
    {


        if ($options != null) {
            $propertyFor = $options['propertyFor'];
            $youAreOtp = $options['youAreOtp'];
            $optionsSelected = $options['optionsSelected'];
        } else {
            $propertyFor = $request->get('propertyFor');
            $youAreOtp = $request->get('youAreOtp');
            $optionsSelected = $request->get('optionsSelected');
        }


//        dd($options);


        $optionsSelectedData = listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp][$optionsSelected];


//        $constructionStatus = $optionsSelectedData['constructionStatus'];
//        $data_features = $optionsSelectedData['features'];
//        $data_amenities = $optionsSelectedData['Amenities'];
//        $data_images = $optionsSelectedData['images'];
//        $data_property_description = $optionsSelectedData['property_description'];


        if (is_array($optionsSelectedData)) {

            ?>

            <?php
//            dump($optionsSelectedData);

            ?>

            <div class="row">
                <?php
                //            dump($data_features);
                ?>

                <div class="col-md-12">

                    <div class="box">
                        <div class="box-inner">
                            <div class="box-title">

                                <h2> <?= textResort($optionsSelected) ?> features </h2>
                            </div>


                            <?php

                            if (!empty($optionsSelectedData)) {
                                foreach ($optionsSelectedData AS $optionsSelectedItemKey => $optionsSelectedItem) {
//                                dump($optionsSelectedItemKey);

                                    switch ($optionsSelectedItemKey) {
                                        case 'construction-status':
                                            if (isset($optionsSelectedData['construction-status'])) {
                                                ?>
                                                <div class="row">

                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <label class=""> constructionStatus </label>
                                                            <select class="form-control" name="construction-status">
                                                                <?php
                                                                foreach ($optionsSelectedData['construction-status'] AS $constructionStatusKey => $constructionStatusData) {
                                                                    ?>
                                                                    <option value="<?= $constructionStatusKey ?>">
                                                                        <?= $constructionStatusKey ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">

                                                        <!--                                Expecting-->

                                                        <?php
                                                        $months = array(
                                                            'January',
                                                            'February',
                                                            'March',
                                                            'April',
                                                            'May',
                                                            'June',
                                                            'July ',
                                                            'August',
                                                            'September',
                                                            'October',
                                                            'November',
                                                            'December',
                                                        );

                                                        ?>

                                                        <div class="form-group">
                                                            <label class=""> month </label>
                                                            <select class="form-control"
                                                                    name="construction-status[month]">
                                                                <?php
                                                                foreach ($months AS $month) {
                                                                    ?>
                                                                    <option value="<?= $month ?>">
                                                                        <?= $month ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>


                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php
                                                        $years = array(
                                                            'January',
                                                            'February',
                                                            'March',
                                                            'April',
                                                            'May',
                                                            'June',
                                                            'July ',
                                                            'August',
                                                            'September',
                                                            'October',
                                                            'November',
                                                            'December',
                                                        );
                                                        ?>

                                                        <div class="form-group">
                                                            <label class=""> Year </label>
                                                            <select class="form-control"
                                                                    name="construction-status[year]">
                                                                <?php
                                                                foreach ($years AS $year) {
                                                                    ?>
                                                                    <option value="<?= $year ?>">
                                                                        <?= $year ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>
                                                <?php
                                            }

                                            break;
                                        case 'features':


                                            foreach ($optionsSelectedItem AS $option_features_key => $options_features_Data) {
                                                switch ($option_features_key) {
                                                    case 'spaces':

//                                                    dd($option_features_key);


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2> <?= textResort($option_features_key) ?></h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $spacesKey => $spacesData) {

                                                                    ?>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class=""> <?= $spacesKey ?> </label>
                                                                            <select class="form-control"
                                                                                    name="spaces[<?= $spacesKey ?>]">
                                                                                <?php
                                                                                foreach ($spacesData AS $spacesOpt) {
                                                                                    ?>
                                                                                    <option value="<?= $spacesOpt ?>">
                                                                                        <?= $spacesOpt ?>
                                                                                    </option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }

                                                        break;
                                                    case 'dimensions':


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2>dimensions</h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $dimensionsKey => $dimensionsData) {

                                                                    ?>
                                                                    <div class="col-md-3">

                                                                        <div class="input-group mb-3">
                                                                            <input type="text" class="form-control"
                                                                                   name="dimensions[<?= $dimensionsKey ?>][value]"
                                                                                   placeholder="<?= $dimensionsKey ?>"/>
                                                                            <div class="input-group-append">

                                                                                <select class="form-control"
                                                                                        name="spaces[<?= $dimensionsKey ?>][type]">
                                                                                    <?php
                                                                                    foreach ($dimensionsData AS $dimensionOpt) {
                                                                                        ?>
                                                                                        <option value="<?= $dimensionOpt ?>">
                                                                                            <?= $dimensionOpt ?>
                                                                                        </option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>

                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }


                                                        break;
                                                    case 'price':


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2>price</h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $priceKey => $priceData) {


                                                                    if (is_array($priceData)) {

                                                                        ?>

                                                                        <div class="col-md-3">

                                                                            <div class="input-group mb-3">
                                                                                <input type="text" class="form-control"
                                                                                       name="dimensions[<?= $priceKey ?>][value]"
                                                                                       placeholder="<?= $priceKey ?>"/>
                                                                                <div class="input-group-append">

                                                                                    <select class="form-control"
                                                                                            name="spaces[<?= $priceKey ?>][type]">
                                                                                        <?php
                                                                                        foreach ($priceData AS $priceOpt) {
                                                                                            ?>
                                                                                            <option value="<?= $priceOpt ?>">
                                                                                                <?= $priceOpt ?>
                                                                                            </option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>

                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <?php

                                                                    } else {

                                                                        ?>
                                                                        <div class="col-md-3">

                                                                            <div class="input-group mb-3">
                                                                                <input type="text" class="form-control"
                                                                                       name="dimensions[<?= $priceKey ?>][value]"
                                                                                       placeholder="<?= $priceData ?>"/>

                                                                            </div>


                                                                        </div>
                                                                        <?php

                                                                    }


                                                                    ?>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }

                                                        break;


                                                    default:


                                                        break;
                                                }
                                            }


                                            break;
                                        case 'amenities':

//                                        dd($optionsSelectedItem);

                                            ?>
                                            <div class="box-title">
                                                <h2>Amenities</h2>
                                            </div>

                                            <div class="row">


                                                <!--                                            <div class="col-md-12">-->
                                                <?php
                                                foreach ($optionsSelectedItem AS $AmenitiesKey => $AmenitiesData) {

//                                                dump($AmenitiesKey);

                                                    ?>


                                                    <div class="col-md-3">
                                                        <div class="card" style="margin: 0 0 10px 0;">
                                                            <!-- Default panel contents -->
                                                            <div class="card-header">
                                                                <?= $AmenitiesKey ?>
                                                            </div>

                                                            <ul class="list-group list-group-flush">


                                                                <?php
                                                                foreach ($AmenitiesData AS $spacesOpt) {
                                                                    ?>

                                                                    <li class="list-group-item" style="padding: 8px">
                                                                        <?= $spacesOpt ?>
                                                                        <label class="switch ">
                                                                            <input type="checkbox"
                                                                                   name="amenities[<?= $AmenitiesKey ?>]"
                                                                                   class="default">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </li>


                                                                    <?php
                                                                }
                                                                ?>


                                                            </ul>
                                                        </div>
                                                    </div>


                                                    <?php
                                                }
                                                ?>

                                                <!--                                            </div>-->
                                                <!--                                            <div class="col-md-8">-->
                                                <!---->
                                                <!--                                            </div>-->
                                            </div>
                                            <?php

                                            break;
                                        case 'images':
                                            break;
                                        case 'property_description':

                                            ?>

                                            <div class="box-title">
                                                <h2> <?= $optionsSelectedItem ?></h2>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">

                                                    <textarea class="form-control editor"
                                                              name="<?= $optionsSelectedItemKey ?>"></textarea>
                                                    </div>
                                                </div>
                                            </div>


                                            <?php


                                            break;
//                                    case '':
//                                        break;
                                    }

                                }
                            }

                            ?>


                        </div>
                    </div>

                </div>
                <div class="col-md-6">


                </div>
            </div>


            <?php
        } else {
            echo 'no options ';
        }


    }


}
