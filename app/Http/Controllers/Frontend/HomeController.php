<?php


namespace App\Http\Controllers\Frontend;


use App\Models\Enquiries;

use App\Models\Properties;

use App\Models\PropertyImages;

use App\Models\PropertyOptions;

use App\User;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Session;

use Illuminate\Validation\Validator;


class HomeController extends Controller

{

    public function __construct()

    {

        define('PAGE_LIMIT', 30);

    }


    public function index()

    {

        $data = array();

        $data['title'] = 'Quickland-Home';

        $data['active_menu'] = 'home';

        $data['sub_active_menu'] = 'home';

        return view('frontend.home', $data);

    }


    public function propertyList($id = null)

    {

        if ($id) {

            $data = array();

            $data['title'] = 'Quickland-Property view';

            $data['active_menu'] = 'propertyList';

            $data['sub_active_menu'] = 'propertyList';

            $data['property'] = Properties::with('propertyOptions')->where('property_id', $id)
                ->first();

            return view('frontend.listing.propertyview', $data);

        } else {

            $data = array();

            $data['title'] = 'Quickland-Property List';

            $data['active_menu'] = 'propertyList';

            $data['sub_active_menu'] = 'propertyList';

            $data['properties'] = Properties::with('propertyOptions', 'productImages')
                ->orderBy('property_id', 'desc')
                ->paginate(PAGE_LIMIT);

            return view('frontend.listing.propertyList', $data);

        }


    }


    public function agentsList($id = null)

    {

        if ($id) {

            $data = array();

            $data['title'] = 'Quickland-Agents List';

            $data['active_menu'] = 'agentsList';

            $data['sub_active_menu'] = 'agentsList';

            $data['properties'] = Properties::with('propertyOptions')->where('property_userid', $id)->orderBy('property_id', 'desc')
                ->paginate(3);

            $data['agent'] = User::where('id', $id)->orderBy('id', 'desc')
                ->first();

            return view('frontend.agents.view', $data);

        } else {

            $data = array();

            $data['title'] = 'Quickland-Agents List';

            $data['active_menu'] = 'agentsList';

            $data['sub_active_menu'] = 'agentsList';

            $data['agents'] = User::orderBy('id', 'desc')
                ->paginate(PAGE_LIMIT);

            return view('frontend.agents.list', $data);

        }


    }


    public function agentDetails($id)

    {

        $data = array();

        $data['title'] = 'Quickland-Agents List';

        $data['active_menu'] = 'agentsList';

        $data['sub_active_menu'] = 'agentsList';

        $data['agent'] = User::where('id', $id)->first();

        return view('frontend.agentsDetails', $data);

    }

    public function enquiries(Request $request)
    {

        if ($request->isMethod('post')) {

            $return = array();

            $requestData = $request->all();

            $validator = \Illuminate\Support\Facades\Validator::make($requestData,

                [

                    'enquiry_name' => 'required',

                    'enquiry_subject' => 'required',

                    'enquiry_email' => 'required|email'

                ],

                [

                    'enquiry_name.required' => 'Please enter email',

                    'enquiry_subject.required' => 'Please enter subject',

                    'enquiry_email.required' => 'Please enter email',

                    'enquiry_email.email' => 'Please enter valid email'

                ]

            );

            if ($validator->fails()) {

                $return['status'] = 2;

                $test = array();

                foreach ($validator->messages()->getMessages() as $field_name => $messages) {

                    array_push($test, $messages[0]);// messages are retrieved (publicly)

                }

                $return['errors'] = implode('<br>', $test);

            } else {

                $cdetails = Enquiries::create($requestData);

                if ($cdetails->exists) {

                    $return['status'] = 1;

                } else {

                    $return['status'] = 2;

                    $return['errors'] = "Please enter valid data";

                }

            }

            return $return;

        }

    }

}

