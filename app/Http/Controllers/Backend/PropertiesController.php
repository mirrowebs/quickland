<?php

namespace App\Http\Controllers\Backend;

use App\Models\Properties;
use App\Models\PropertyImages;
use App\Models\PropertyOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $propertyimages = PropertyImages::where('pi_property_id', $requestData['property_id'])->get();

            foreach ($propertyimages as $image) {

                File::delete('uploads/properties/' . $image->pi_image);
                File::delete('uploads/properties/thumbs/' . $image->pi_image);
                PropertyImages::destroy($image->pi_id);
            }

            PropertyOptions::where('po_property_id', $requestData['property_id'])->delete();

            Properties::destroy($requestData['property_id']);

            return redirect()->route('properties')->with('flash_message', 'PropertyFiled deleted successfully!');
        } else {
            $properties = Properties::with('propertyOptions')->orderBy('property_id', 'desc')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'properties';
        $data['sub_active_menu'] = 'properties-list';
        $data['title'] = 'Properties';
        $data['properties'] = $properties;
        return view('backend.properties.list', $data);
    }

    public function addNewProperties(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $this->validate(request(), [
                'property.property_sell' => 'required',
                'property.property_type' => 'required',
                'property.property_transaction_type' => 'required',
                'property.property_location' => 'required',
                'property.property_built_up_area' => 'required',
                'property.property_area_unit' => 'required',
                'property.property_status' => 'required'
            ], [
                'property.property_sell.required' => 'Please enter type',
                'property.property_type.required' => 'Please enter type',
                'property.property_transaction_type.required' => 'Please enter transaction type',
                'property.property_location.required' => 'Please enter location',
                'property.property_built_up_area.required' => 'Please enter built up area',
                'property.property_area_unit.required' => 'Please enter area unit',
                'property.property_status.required' => 'Please select status'
            ]);

            if (isset($requestData['options']['po_other_rooms'])) {
                $requestData['options']['po_other_rooms'] = implode(',', $requestData['options']['po_other_rooms']);
            } else {
                $requestData['options']['po_other_rooms'] = '';
            }

            if (isset($requestData['options']['po_reserved_on_parking'])) {
                $requestData['options']['po_reserved_on_parking'] = implode(',', $requestData['options']['po_reserved_on_parking']);
            } else {
                $requestData['options']['po_reserved_on_parking'] = '';
            }

            if (isset($requestData['options']['po_amenties'])) {
                $requestData['options']['po_amenties'] = implode(',', $requestData['options']['po_amenties']);
            } else {
                $requestData['options']['po_amenties'] = '';
            }


            if (isset($requestData['options']['po_inclusive_price'])) {
                $requestData['options']['po_inclusive_price'] = 1;
            } else {
                $requestData['options']['po_inclusive_price'] = 0;
            }

            if (isset($requestData['options']['po_price_negotiable'])) {
                $requestData['options']['po_price_negotiable'] = 1;
            } else {
                $requestData['options']['po_price_negotiable'] = 0;
            }


            if ($requestData['property_id'] == '') {


                $propertyid = Properties::create($requestData['property'])->property_id;

                if ($requestData['property']['property_type'] == 'Residential') {
                    $options = $requestData['options'];
                    $options['po_property_id'] = $propertyid;
                    PropertyOptions::create($options);
                }

                $mes = 'PropertyFiled added successfully!';
            } else {

                $property = Properties::findOrFail($requestData['property_id']);

                PropertyOptions::where('po_property_id', $property->property_id)->delete();

                if ($requestData['property']['property_type'] == 'Residential') {

                    $options = $requestData['options'];
                    $options['po_property_id'] = $property->property_id;
                    PropertyOptions::create($options);
                }

                $property->update($requestData['property']);


                $mes = 'PropertyFiled updated successfully!';
            }
            return redirect()->route('properties')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['property_id'] = '';
            $data['property'] = '';
            if ($id) {
                $data['property_id'] = $id;
                $data['property'] = Properties::with('propertyOptions')->where('property_id', $id)->first();
            }

//            dd($data['property']->propertyOptions[0]);

            $data['active_menu'] = 'properties';
            $data['sub_active_menu'] = 'manage-properties';
            $data['title'] = 'Manage properties';
            return view('backend.properties.add', $data);
        }
    }

    public function getPropertySubTypes(Request $request)
    {
        $property = $request['property'];
        $return = array();
        $return['propertytypes'] = allOptions($property);
        $return['length'] = count(allOptions($property));
        echo json_encode($return);
        exit();
    }

    public function addPropertyImages(Request $request, $id)
    {
        if ($request->isMethod('post')) {

            if ($request->hasFile('propertyimages')) {
                foreach ($request['propertyimages'] as $file) {
                    $uploadPath = public_path('/uploads/properties/');

                    if (!file_exists($uploadPath)) {
                        mkdir($uploadPath, 0777, true);
                    }
                    $extension = $file->getClientOriginalName();

                    $fileName = time() . $extension;

                    $thumbPath = public_path('/uploads/properties/thumbs/');

                    if (!file_exists($thumbPath)) {
                        mkdir($thumbPath, 0777, true);
                    }

                    $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumb_img->save($thumbPath . '/' . $fileName, 60);

                    $file->move($uploadPath, $fileName);

                    $requestdata = array();
                    $requestdata['pi_image'] = $fileName;
                    $requestdata['pi_property_id'] = $id;
                    PropertyImages::create($requestdata);
                }
            }

            return redirect()->route('addPropertyImages', ['id' => $id])->with('flash_message', 'Product Images added successfully');

        } else {
            $data = array();

            $data['property_id'] = $id;
            $data['property_images'] = PropertyImages::where('pi_property_id', $id)->get();
            $data['active_menu'] = 'properties';
            $data['sub_active_menu'] = 'properties-list';
            $data['title'] = 'PropertyFiled Images';
            return view('backend.properties.images', $data);
        }
    }

    public function deletepropertyimage(Request $request)
    {
        $image = PropertyImages::findOrFail($request['image']);

        File::delete('uploads/properties/' . $image->pi_image);
        File::delete('uploads/properties/thumbs/' . $image->pi_image);

        PropertyImages::destroy($request['image']);
        exit();
    }


}
