<?php

namespace App\Http\Controllers\Backend;

use App\Models\Enquiries;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        define('PAGE_LIMIT', 30);
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Quickland-Dashboard';
        $data['active_menu'] = 'dashboard';
        $data['sub_active_menu'] = 'dashboard';
        return view('backend.dashboard', $data);
    }

    public function agentsList()
    {
        $data = array();
        $data['title'] = 'Quickland-Agents List';
        $data['active_menu'] = 'agentsList';
        $data['sub_active_menu'] = 'agentsList';
        $data['agents'] = User::where('role', '!=', 404)->orderBy('id', 'desc')
            ->paginate(PAGE_LIMIT);;
        return view('backend.agents.list', $data);
    }

   public function agentEnquiriesList($id)
    {
        $data = array();
        $data['title'] = 'Quickland-Agent Enquiries List';
        $data['active_menu'] = 'agentsList';
        $data['sub_active_menu'] = 'agentsList';
        $data['enquiries'] = Enquiries::where('enquiry_user_id',$id)->orderBy('enquiry_id', 'desc')
            ->paginate(PAGE_LIMIT);;
        return view('backend.agents.enquiries', $data);
    }


    public function profile(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required'
            ], [
                'name.required' => 'Please enter name',
                'email.required' => 'Please enter email'
            ]);
            $id = Auth::id();
            $user = User::findOrFail($id);
            $user->update($requestData);
            $mes = 'profile updated successfully!';
            return redirect()->route('profile')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['active_menu'] = 'profile';
            $data['sub_active_menu'] = 'profile';
            $data['title'] = 'Quickland - Update profile';
            return view('backend.updateProfile', $data);
        }
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $this->validate(request(), [
                'password' => 'required',
                'new_password' => 'required|min:6|max:12',
                'confirm_password' => 'required|min:6|max:12|same:new_password'
            ], [
                'password.required' => 'Please enter old password',
                'new_password.required' => 'Please enter new password',
                'confirm_password.required' => 'Please confirm password'
            ]);


            $id = Auth::id();
            $user = User::findOrFail($id);
            if (Hash::check($requestData['password'], $user->password)) {
                $requestData['password'] = Hash::make($requestData['new_password']);
                $user->update($requestData);
                $mes = 'Password updated successfully!';
                return redirect()->route('changePassword')->with('flash_message', $mes);
            } else {
                $mes = 'Old password is wrong ...try again';
                return redirect()->route('changePassword')->with('flash_message_error', $mes);
            }

        } else {
            $data = array();
            $data['active_menu'] = 'changepassword';
            $data['sub_active_menu'] = 'changepassword';
            $data['title'] = 'Quickland - Change Password';
            return view('backend.changePassword', $data);
        }
    }

}
