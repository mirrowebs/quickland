<?php

namespace App\Http\Controllers\Options;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyFiled extends Controller
{
    public function __construct()
    {

    }

    public function hiddenFiledsRender($fileds)
    {
        if (count($fileds) > 0) {
            foreach ($fileds AS $filed => $filedValue) {
                ?>
                <input name="hidden[<?= $filed ?>]" value="<?= $filedValue ?>" type="hidden"/>
                <?php
            }
        }
    }


    public function Features($options, $values = null, $type = null)
    {

        if ($values != '' && count($values) > 0) {
            $currentData = $values[0];
//            dump($currentData);
        }

        if (!empty($options)) {
            $propertyFor = $options['propertyFor'];
            $youAreOtp = $options['youAreOtp'];
            $optionsSelected = $options['optionsSelected'];
        }
        $optionsSelectedData = listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp][$optionsSelected];


        if (is_array($optionsSelectedData)) {
            ?>

            <div class="row">


                <div class="col-md-12">


                    <?php

                    if (!empty($optionsSelectedData)) {
                        foreach ($optionsSelectedData AS $optionsSelectedItemKey => $optionsSelectedItem) {
//                                dump($optionsSelectedItemKey);

                            switch ($optionsSelectedItemKey) {


                                case 'amenities':

//                                        dd($optionsSelectedItem);

                                    ?>
                                    <div class="box">
                                        <div class="box-inner">

                                            <div class="box-title">
                                                <h2>Amenities</h2>
                                            </div>

                                            <div class="row">


                                                <!--                                            <div class="col-md-12">-->
                                                <?php
                                                foreach ($optionsSelectedItem AS $AmenitiesKey => $AmenitiesData) {

//                                                dump($AmenitiesKey);
//                                                dump($currentData);


                                                    $curent_key = 'po_' . strCapitalSort($AmenitiesKey);


//                                                    echo $curent_key;
//                                                    dump($currentData->$curent_key);

                                                    if (isset($currentData->$curent_key)) {

                                                        $fut_opt = explode(',', $currentData->$curent_key);
                                                        $fut_opt = array_filter($fut_opt);
                                                    } else {
                                                        $fut_opt = '';
                                                    }

//                                                    dump($fut_opt);
                                                    ?>


                                                    <div class="col-md-3">
                                                        <div class="card" style="margin: 0 0 10px 0;">
                                                            <!-- Default panel contents -->
                                                            <div class="card-header">
                                                                <?= textResort($AmenitiesKey, '1') ?>
                                                            </div>

                                                            <ul class="<?= ($type != '') ? 'amenities_s' : 'list-group list-group-flush ' ?> ">


                                                                <?php
                                                                foreach ($AmenitiesData AS $spacesOpt) {

//                                                                    dump($fut_opt);
//                                                                    dump($spacesOpt);


                                                                    ?>



                                                                    <li class="<?= ($type != '') ? ' ' : 'list-group-item ' ?> " style="padding: 8px">
                                                                    <?= $spacesOpt ?>
                                                                    <label class=" ">

                                                                    <?php
                                                                    if ($type != '') {
                                                                        ?>

                                                                        <li class="<?= (is_array($fut_opt) && in_array($spacesOpt, $fut_opt)) ? 'yes' : 'no' ?>">
                                                                            <?= $spacesOpt; ?>
                                                                        </li>


                                                                        <?php

                                                                    } else {
                                                                        ?>
                                                                        <input type="checkbox"
                                                                               name="features[amenities][<?= $AmenitiesKey ?>][]" <?= (is_array($fut_opt) && in_array($spacesOpt, $fut_opt)) ? 'checked' : '' ?>
                                                                               value="<?= $spacesOpt ?>"
                                                                               class="default">
                                                                        <?php
                                                                    }


                                                                    ?>


                                                                    </label>
                                                                    </li>


                                                                    <?php
                                                                }
                                                                ?>


                                                            </ul>
                                                        </div>
                                                    </div>


                                                    <?php
                                                }
                                                ?>

                                                <!--                                            </div>-->
                                                <!--                                            <div class="col-md-8">-->
                                                <!---->
                                                <!--                                            </div>-->
                                            </div>


                                        </div>
                                    </div>
                                    <?php

                                    break;

//                                    case '':
//                                        break;
                            }

                        }
                    }

                    ?>


                </div>
                <div class="col-md-6">


                </div>
            </div>


            <?php
        }


        ?>

        <?php
    }

    public function Pricing($options, $values = null)
    {


        //        dump($location);
        if ($values != '' && count($values) > 0) {
            $currentData = $values[0];
//            dump($currentData);
        }

        if (!empty($options)) {
            $propertyFor = $options['propertyFor'];
            $youAreOtp = $options['youAreOtp'];
            $optionsSelected = $options['optionsSelected'];
        }
        $optionsSelectedData = listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp][$optionsSelected];


        if (is_array($optionsSelectedData)) {
            ?>
            <div class="row">

                <div class="col-md-12">

                    <div class="box">
                        <div class="box-inner">
                            <div class="box-title">
                                <h2> <?= textResort($optionsSelected, '1') ?>  </h2>
                            </div>


                            <?php

                            if (!empty($optionsSelectedData)) {
                                foreach ($optionsSelectedData AS $optionsSelectedItemKey => $optionsSelectedItem) {
//                                dump($optionsSelectedItemKey);

                                    switch ($optionsSelectedItemKey) {

                                        case 'features':


                                            foreach ($optionsSelectedItem AS $option_features_key => $options_features_Data) {
                                                switch ($option_features_key) {
                                                    case 'price':


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2>price</h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $priceKey => $priceData) {

                                                                    $curent_key = 'po_' . strCapitalSort($priceKey);
                                                                    $curent_type = 'po_' . strCapitalSort($priceKey) . '_type';


                                                                    if (is_array($priceData)) {

//                                                                        dump($currentData);

                                                                        ?>

                                                                        <div class="col-md-6">

                                                                            <div class="input-form mb-3">
                                                                                <label><?= textResort($curent_key) ?></label>
                                                                                <div class="input-group">
                                                                                    <input type="text"
                                                                                           class="form-control <?= $curent_key ?>"
                                                                                           name="price[<?= $priceKey ?>][value]"
                                                                                           id="<?= $curent_key ?>"
                                                                                           value="<?= (isset($currentData) && $currentData->$curent_key != '') ? $currentData->$curent_key : '' ?>"
                                                                                           placeholder="<?= $priceKey ?>"/>
                                                                                    <div class="input-group-append">

                                                                                        <select class="form-control"
                                                                                                name="price[<?= $priceKey ?>][type]">
                                                                                            <?php
                                                                                            foreach ($priceData AS $priceOpt) {
                                                                                                ?>
                                                                                                <option value="<?= $priceOpt ?>" <?= (isset($currentData) && $currentData->$curent_type == $priceOpt) ? 'selected' : '' ?>>
                                                                                                    <?= $priceOpt ?>
                                                                                                </option>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                        </select>

                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <?php

                                                                    } else {

                                                                        ?>
                                                                        <div class="col-md-6">

                                                                            <div class="input-form mb-3">
                                                                                <label><?= textResort($curent_key) ?></label>
                                                                                <div class="input-group">
                                                                                    <input type="text"
                                                                                           class="form-control <?= $curent_key ?>"
                                                                                           id="<?= $curent_key ?>"
                                                                                           name="price[<?= $priceKey ?>]"
                                                                                           value="<?= (isset($currentData) && $currentData->$curent_key != '') ? $currentData->$curent_key : '' ?>"
                                                                                           placeholder="<?= $priceData ?>"/>
                                                                                </div>

                                                                            </div>


                                                                        </div>
                                                                        <?php

                                                                    }


                                                                    ?>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }

                                                        break;


                                                }

                                            }
                                    }

                                    ?>


                                    <?
                                }

//                                dd($optionsSelectedData);

                            }

                            ?>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">


                </div>
            </div>
            <?php
        }


        ?>

        <?php
    }

    public function locationOpt($hidden, $values = null)
    {

        $location = listingOptions('location');

//        dump($location);
        if ($values != '' && count($values) > 0) {
            $currentData = $values[0];
//            dump($currentData);
        }

        ?>
        <div class="box">
            <div class="box-inner">
                <div class="box-title">
                    <h2>Location</h2>
                </div>
                <div class="row">

                    <?php
                    if (count($location) > 0) {
                        foreach ($location AS $fieds => $fiedsName) {
                            $curent_key = 'po_' . strCapitalSort($fiedsName);
                            switch ($fieds) {


//                                    case 'pro_city':
//                                        break;


                                case 'pro_locality':


                                    ?>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class=""> <?= textResort($fiedsName) ?> </label>
                                            <select name="location[<?= $fieds ?>]" class="form-control">
                                                <option>Select</option>
                                                <option <?php echo (isset($currentData->$curent_key) && $currentData->$curent_key == 'AP') ? 'selected' : '' ?> >
                                                    AP
                                                </option>
                                                <option <?php echo (isset($currentData->$curent_key) && $currentData->$curent_key == 'TS') ? 'selected' : '' ?> >
                                                    TS
                                                </option>
                                            </select>
                                        </div>
                                    </div>


                                    <?php


//                                    echo $fiedsName;
//                                    echo $curent_key;
//                                    echo $currentData->$curent_key;

                                    break;


                                default:

                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-group ">


                                            <label class=""> <?= textResort($fiedsName) ?> </label>
                                            <input name="location[<?= $fieds ?>]" id="<?= $fiedsName ?>"
                                                   value="<?= (isset($currentData->$curent_key) && $currentData->$curent_key !== '') ? $currentData->$curent_key : '' ?>"
                                                   class="form-control <?= $fiedsName ?>"/>


                                        </div>
                                    </div>


                                    <?php


                                    break;
                            }
//                            echo $fiedsName;
//                            echo $curent_key;
//                            echo $currentData->$curent_key;
                            ?>

                            <?php
                        }
                    }
                    ?>


                </div>
            </div>
        </div>
        <?php

//        location
    }

    public function propertyDetails($options, $values = null)
    {
        if (!empty($options)) {
            $propertyFor = $options['propertyFor'];
            $youAreOtp = $options['youAreOtp'];
            $optionsSelected = $options['optionsSelected'];
        }
        $optionsSelectedData = listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp][$optionsSelected];


//        dump($values);

        if ($values != '' && count($values) > 0) {
            $currentData = $values[0];
//            dump($currentData);
        }

        if (is_array($optionsSelectedData)) {
            ?>
            <div class="row">
                <?php
                //            dump($data_features);
                ?>

                <div class="col-md-12">

                    <h2> Features </h2>
                    <div class="box">
                        <div class="box-inner">
                            <div class="box-title">
                                <h2> <?= textResort($optionsSelected, '1') ?>  </h2>
                            </div>


                            <?php

                            if (!empty($optionsSelectedData)) {
                                foreach ($optionsSelectedData AS $optionsSelectedItemKey => $optionsSelectedItem) {
//                                dump($optionsSelectedItemKey);

                                    switch ($optionsSelectedItemKey) {
                                        case 'construction-status':
                                            if (isset($optionsSelectedData['construction-status'])) {
                                                ?>
                                                <div class="row">

                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <label class=""> Construction Status </label>
                                                            <select class="form-control"
                                                                    name="PropertyDetails[constructionStatus][value]">
                                                                <?php
                                                                foreach ($optionsSelectedData['construction-status'] AS $constructionStatusKey => $constructionStatusData) {
                                                                    ?>
                                                                    <option value="<?= $constructionStatusKey ?>" <?= (isset($currentData) && $currentData->po_construction_status == $constructionStatusKey) ? 'selected' : '' ?> >
                                                                        <?= $constructionStatusKey ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">

                                                        <!--                                Expecting-->

                                                        <?php
                                                        $months = array(
                                                            'January',
                                                            'February',
                                                            'March',
                                                            'April',
                                                            'May',
                                                            'June',
                                                            'July ',
                                                            'August',
                                                            'September',
                                                            'October',
                                                            'November',
                                                            'December',
                                                        );

                                                        ?>

                                                        <div class="form-group">
                                                            <label class=""> Month </label>
                                                            <select class="form-control"
                                                                    name="PropertyDetails[constructionStatus][month]">
                                                                <?php
                                                                foreach ($months AS $month) {
                                                                    ?>
                                                                    <option value="<?= $month ?>" <?= (isset($currentData) && $currentData->po_construction_month == $month) ? 'selected' : '' ?> >
                                                                        <?= $month ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>


                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php
                                                        $starting_year = date('Y', strtotime('-10 year'));
                                                        $ending_year = date('Y', strtotime('+10 year'));
                                                        $current_year = date('Y');
                                                        ?>

                                                        <div class="form-group">
                                                            <label class=""> Year </label>
                                                            <select class="form-control"
                                                                    name="PropertyDetails[constructionStatus][year]">
                                                                <?php
                                                                for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                                                    ?>
                                                                    <option value="<?= $starting_year ?>" <?= (isset($currentData) && $starting_year == $currentData->po_construction_year) ? 'selected' : '' ?> >
                                                                        <?= $starting_year; ?>
                                                                    </option>
                                                                    <?php
//                                                                    echo '<option value="' . $starting_year . '"';
//                                                                    if ($starting_year == $current_year) {
//                                                                        echo ' selected="selected"';
//                                                                    }
//                                                                    echo ' >' . $starting_year . '</option>';
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>
                                                <?php
                                            }

                                            break;
                                        case
                                        'features':


                                            foreach ($optionsSelectedItem AS $option_features_key => $options_features_Data) {
                                                switch ($option_features_key) {
                                                    case 'spaces':

//                                                    dd($option_features_key);


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2> <?= textResort($option_features_key) ?></h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $spacesKey => $spacesData) {

                                                                    ?>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class="">

                                                                                <?php
                                                                                echo textResort($spacesKey);


                                                                                switch ($spacesKey) {
                                                                                    case 'ageOfProperty':
                                                                                        echo ' (years)';
                                                                                        break;
                                                                                    default:
                                                                                        break;


                                                                                }
                                                                                ?>

                                                                            </label>

                                                                            <?php

                                                                            //                                                                            isset($currentData) &&

                                                                            switch ($spacesKey) {
                                                                                case 'otherRooms':


                                                                                    $curent_key = 'po_' . strCapitalSort($spacesKey);

//                                                                                   echo $currentData->$other_room;

//                                                                                echo strCapitalSort($spacesKey);

                                                                                    ?>


                                                                                    <select multiple
                                                                                            class="form-control select2"
                                                                                            name="PropertyDetails[spaces][<?= $spacesKey ?>]">
                                                                                        <?php
                                                                                        foreach ($spacesData AS $spacesOpt) {
                                                                                            ?>
                                                                                            <option value="<?= $spacesOpt ?>" <?= (isset($currentData) && $currentData->$curent_key == $spacesOpt) ? 'selected' : '' ?> >
                                                                                                <?= $spacesOpt ?>
                                                                                            </option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                    <?php
                                                                                    break;
                                                                                default:
                                                                                    $curent_key = 'po_' . strCapitalSort($spacesKey);

                                                                                    ?>
                                                                                    <select class="form-control"
                                                                                            name="PropertyDetails[spaces][<?= $spacesKey ?>]">
                                                                                        <?php
                                                                                        foreach ($spacesData AS $spacesOpt) {
                                                                                            ?>
                                                                                            <option value="<?= $spacesOpt ?>" <?= (isset($currentData) && $currentData->$curent_key == $spacesOpt) ? 'selected' : '' ?>>
                                                                                                <?= $spacesOpt ?>
                                                                                            </option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                    <?php
//                                                                                    echo $spacesKey.'=>'.$curent_key;

                                                                                    break;


                                                                            }
                                                                            ?>

                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }

                                                        break;
                                                    case 'dimensions':


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2>Dimensions</h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $dimensionsKey => $dimensionsData) {

                                                                    $curent_key = 'po_' . strCapitalSort($dimensionsKey);
                                                                    $curent_type = 'po_' . strCapitalSort($dimensionsKey) . '_type';

                                                                    ?>
                                                                    <div class="col-md-3">
                                                                        <label> <?= textResort($dimensionsKey, '1') ?> </label>
                                                                        <div class="input-group mb-3">
                                                                            <input type="text" class="form-control"
                                                                                   name="PropertyDetails[dimensions][<?= $dimensionsKey ?>][value]"
                                                                                   value="<?= (isset($currentData) && $currentData->$curent_key != '') ? $currentData->$curent_key : '' ?>"
                                                                                   placeholder="<?= $dimensionsKey ?>"/>
                                                                            <div class="input-group-append">

                                                                                <select class="form-control"
                                                                                        name="PropertyDetails[dimensions][<?= $dimensionsKey ?>][type]">
                                                                                    <?php
                                                                                    foreach ($dimensionsData AS $dimensionOpt) {
                                                                                        ?>
                                                                                        <option value="<?= $dimensionOpt ?>" <?= (isset($currentData) && $currentData->$curent_type == $dimensionOpt) ? 'selected' : '' ?> >
                                                                                            <?= $dimensionOpt ?>
                                                                                        </option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>


                                                                            </div>
                                                                        </div>

                                                                        <?php

                                                                        //                                                                        echo $dimensionsKey.'=>'.$curent_key;
                                                                        ?>


                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }
                                                        break;
//                                    case '':
//                                        break;
                                                }

                                            }
                                    }

                                    ?>


                                    <?
                                }

//                                dd($optionsSelectedData);

                            }

                            ?>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">


                </div>
            </div>
            <?php
        }
    }


    public
    function getPropertyTypeOptionSelected($options = null, Request $request = null)
    {


        if ($options != null) {
            $propertyFor = $options['propertyFor'];
            $youAreOtp = $options['youAreOtp'];
            $optionsSelected = $options['optionsSelected'];
        } else {
            $propertyFor = $request->get('propertyFor');
            $youAreOtp = $request->get('youAreOtp');
            $optionsSelected = $request->get('optionsSelected');
        }


//        dd($options);


        $optionsSelectedData = listingOptions('propertyFor')[$propertyFor]['propertyType'][$youAreOtp][$optionsSelected];


//        $constructionStatus = $optionsSelectedData['constructionStatus'];
//        $data_features = $optionsSelectedData['features'];
//        $data_amenities = $optionsSelectedData['Amenities'];
//        $data_images = $optionsSelectedData['images'];
//        $data_property_description = $optionsSelectedData['property_description'];


        if (is_array($optionsSelectedData)) {

            ?>

            <?php
//            dump($optionsSelectedData);

            ?>

            <div class="row">
                <?php
                //            dump($data_features);
                ?>

                <div class="col-md-12">

                    <div class="box">
                        <div class="box-inner">
                            <div class="box-title">
                                <h2> <?= $optionsSelected ?> features </h2>
                            </div>


                            <?php

                            if (!empty($optionsSelectedData)) {
                                foreach ($optionsSelectedData AS $optionsSelectedItemKey => $optionsSelectedItem) {
//                                dump($optionsSelectedItemKey);

                                    switch ($optionsSelectedItemKey) {
                                        case 'construction-status':
                                            if (isset($optionsSelectedData['construction-status'])) {
                                                ?>
                                                <div class="row">

                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <label class=""> constructionStatus </label>
                                                            <select class="form-control"
                                                                    name="construction-status">
                                                                <?php
                                                                foreach ($optionsSelectedData['construction-status'] AS $constructionStatusKey => $constructionStatusData) {
                                                                    ?>
                                                                    <option value="<?= $constructionStatusKey ?>">
                                                                        <?= $constructionStatusKey ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">

                                                        <!--                                Expecting-->

                                                        <?php
                                                        $months = array(
                                                            'January',
                                                            'February',
                                                            'March',
                                                            'April',
                                                            'May',
                                                            'June',
                                                            'July ',
                                                            'August',
                                                            'September',
                                                            'October',
                                                            'November',
                                                            'December',
                                                        );

                                                        ?>

                                                        <div class="form-group">
                                                            <label class=""> month </label>
                                                            <select class="form-control"
                                                                    name="construction-status[month]">
                                                                <?php
                                                                foreach ($months AS $month) {
                                                                    ?>
                                                                    <option value="<?= $month ?>">
                                                                        <?= $month ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>


                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php
                                                        $years = array(
                                                            'January',
                                                            'February',
                                                            'March',
                                                            'April',
                                                            'May',
                                                            'June',
                                                            'July ',
                                                            'August',
                                                            'September',
                                                            'October',
                                                            'November',
                                                            'December',
                                                        );
                                                        ?>

                                                        <div class="form-group">
                                                            <label class=""> Year </label>
                                                            <select class="form-control"
                                                                    name="construction-status[year]">
                                                                <?php
                                                                foreach ($years AS $year) {
                                                                    ?>
                                                                    <option value="<?= $year ?>">
                                                                        <?= $year ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>
                                                <?php
                                            }

                                            break;
                                        case 'features':


                                            foreach ($optionsSelectedItem AS $option_features_key => $options_features_Data) {
                                                switch ($option_features_key) {
                                                    case 'spaces':

//                                                    dd($option_features_key);


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2> <?= $option_features_key ?></h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $spacesKey => $spacesData) {

                                                                    ?>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class=""> <?= textResort($spacesKey) ?> </label>
                                                                            <select class="form-control"
                                                                                    name="spaces[<?= $spacesKey ?>]">
                                                                                <?php
                                                                                foreach ($spacesData AS $spacesOpt) {
                                                                                    ?>
                                                                                    <option value="<?= $spacesOpt ?>">
                                                                                        <?= $spacesOpt ?>
                                                                                    </option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }

                                                        break;
                                                    case 'dimensions':


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2>dimensions</h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $dimensionsKey => $dimensionsData) {

                                                                    ?>
                                                                    <div class="col-md-3">

                                                                        <div class="input-group mb-3">
                                                                            <input type="text"
                                                                                   class="form-control"
                                                                                   name="dimensions[<?= $dimensionsKey ?>][value]"
                                                                                   placeholder="<?= $dimensionsKey ?>"/>
                                                                            <div class="input-group-append">

                                                                                <select class="form-control"
                                                                                        name="spaces[<?= $dimensionsKey ?>][type]">
                                                                                    <?php
                                                                                    foreach ($dimensionsData AS $dimensionOpt) {
                                                                                        ?>
                                                                                        <option value="<?= $dimensionOpt ?>">
                                                                                            <?= $dimensionOpt ?>
                                                                                        </option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>

                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }


                                                        break;
                                                    case 'price':


                                                        if (isset($options_features_Data)) {
                                                            ?>
                                                            <div class="box-title">
                                                                <h2>price</h2>
                                                            </div>

                                                            <div class="row">


                                                                <?php
                                                                foreach ($options_features_Data AS $priceKey => $priceData) {


                                                                    if (is_array($priceData)) {

                                                                        ?>

                                                                        <div class="col-md-3">

                                                                            <div class="input-group mb-3">
                                                                                <input type="text"
                                                                                       class="form-control"
                                                                                       name="dimensions[<?= $priceKey ?>][value]"
                                                                                       placeholder="<?= $priceKey ?>"/>
                                                                                <div class="input-group-append">

                                                                                    <select class="form-control"
                                                                                            name="spaces[<?= $priceKey ?>][type]">
                                                                                        <?php
                                                                                        foreach ($priceData AS $priceOpt) {
                                                                                            ?>
                                                                                            <option value="<?= $priceOpt ?>">
                                                                                                <?= $priceOpt ?>
                                                                                            </option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>

                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <?php

                                                                    } else {

                                                                        ?>
                                                                        <div class="col-md-3">

                                                                            <div class="input-group mb-3">
                                                                                <input type="text"
                                                                                       class="form-control"
                                                                                       name="dimensions[<?= $priceKey ?>][value]"
                                                                                       placeholder="<?= $priceData ?>"/>

                                                                            </div>


                                                                        </div>
                                                                        <?php

                                                                    }


                                                                    ?>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }

                                                        break;


                                                    default:


                                                        break;
                                                }
                                            }


                                            break;
                                        case 'amenities':

//                                        dd($optionsSelectedItem);

                                            ?>
                                            <div class="box-title">
                                                <h2>Amenities</h2>
                                            </div>

                                            <div class="row">


                                                <!--                                            <div class="col-md-12">-->
                                                <?php
                                                foreach ($optionsSelectedItem AS $AmenitiesKey => $AmenitiesData) {

//                                                dump($AmenitiesKey);

                                                    ?>


                                                    <div class="col-md-3">
                                                        <div class="card" style="margin: 0 0 10px 0;">
                                                            <!-- Default panel contents -->
                                                            <div class="card-header">
                                                                <?= textResort($AmenitiesKey) ?>
                                                            </div>

                                                            <ul class="list-group list-group-flush">


                                                                <?php
                                                                foreach ($AmenitiesData AS $spacesOpt) {
                                                                    ?>

                                                                    <li class="list-group-item"
                                                                        style="padding: 8px">
                                                                        <?= $spacesOpt ?>
                                                                        <label class="switch ">
                                                                            <input type="checkbox"
                                                                                   name="amenities[<?= $AmenitiesKey ?>]"
                                                                                   class="default">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </li>


                                                                    <?php
                                                                }
                                                                ?>


                                                            </ul>
                                                        </div>
                                                    </div>


                                                    <?php
                                                }
                                                ?>

                                                <!--                                            </div>-->
                                                <!--                                            <div class="col-md-8">-->
                                                <!---->
                                                <!--                                            </div>-->
                                            </div>
                                            <?php

                                            break;
                                        case 'images':
                                            break;
                                        case 'property_description':

                                            ?>

                                            <div class="box-title">
                                                <h2> <?= textResort($optionsSelectedItem) ?></h2>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">

                                                    <textarea class="form-control editor"
                                                              name="<?= $optionsSelectedItemKey ?>"></textarea>
                                                    </div>
                                                </div>
                                            </div>


                                            <?php


                                            break;
//                                    case '':
//                                        break;
                                    }

                                }
                            }

                            ?>


                        </div>
                    </div>

                </div>
                <div class="col-md-6">


                </div>
            </div>


            <?php
        } else {
            echo 'no options ';
        }


    }


}
